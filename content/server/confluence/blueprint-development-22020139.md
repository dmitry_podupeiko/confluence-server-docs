---
title: Blueprint Development 22020139
aliases:
    - /server/confluence/blueprint-development-22020139.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=22020139
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=22020139
confluence_id: 22020139
platform:
product:
category:
subcategory:
---
# Confluence Server Development : Blueprint Development

{{% warning %}}

This page is a container for some old material. I'm keeping it around for reference. I'll delete it within a month.

{{% /warning %}}
