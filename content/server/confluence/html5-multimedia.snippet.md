---
aliases:
- /server/confluence/html5-multimedia-39368913.html
- /server/confluence/html5-multimedia-39368913.md
category: devguide
confluence_id: 39368913
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368913
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368913
date: '2017-12-08'
legacy_title: HTML5 Multimedia
platform: server
product: confluence
subcategory: other
title: HTML5 Multimedia
---
# HTML5 Multimedia

## Introduction

This tutorial will cover creating a plugin that creates a macro that will use html 5 elements to display video or audio elements. The key learning is the basics of macro creation and using velocity templates.

The macro has several parameters - such as the location of the multimedia file (url or attachment).

|                 |                                                                                                                               |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------|
| Applicable To   | Confluence 4.x or higher                                                                                                      |
| Prerequisites   | This tutorial assumes that you completed the "[HTML5 Multimedia](/server/confluence/html5-multimedia.snippet)" tutorial |
| Time to Compete | 1.0 hour                                                                                                                      |

## Outline

## Create the plugin "html5-multimedia"

{{% warning %}}

In my instance, I used an include-page-with-replacement macro with the following substitution rule: /hello-world/html5-multimedia/

{{% /warning %}}

## Conventions

### Working Directory

For all of the tutorials, the working directory is called *atlas-development *and is located in the users home directory.

### Development Deployment Paradigm

The tutorials use QuickReload and expect to produce transformerless plugins.

### POM.xml

In the tutorials, there is a parent POM.xml file in the working directory.  The POM.xml file in the plugin directory has been configured to inherit from the POM.xml and all redundant entries have been removed.  Typically, only the artifact version, name, description and build instructions are included. 

### Development Environment

In order to keep things simple, the development has been done in a plain text editor, gvim.  Many find the [Eclipse IDE](/server/confluence/conventions.snippet) a more productive environment. 

## Open a Command Shell

The Atlassian SDK provides several tools that are designed to be launched from a command shell.  Using the table below which shows where in the application launcher for the various operating systems the command shell launcher is, launch the command shell

|         |                                                                 |
|---------|-----------------------------------------------------------------|
| Windows | Start -&gt; All Programs -&gt; Accessories -&gt; Command Prompt |
| OS X    | Applications -&gt; Utilities -&gt; Terminal                     |
| Linux   | Applications Menu -&gt; Accessories -&gt; Terminal              |

## Move to the Working Directory

 

1.  Open a terminal window and perform the following steps:
2.  Create the working directory for plugin development unless it already exists

    {{% note %}}

    While a working directory is not strictly necessary, it's a very good practice for reasons described later

    {{% /note %}}

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td>Windows</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:&gt;</span>mkdir <span class="st">&quot;%HOMEDRIVE%%HOMEPATH%</span><span class="sc">\a</span><span class="st">tlas-development&quot;</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    <tr class="even">
    <td>Linux/Mac</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>$ mkdir <span class="op">~</span><span class="ss">/atlas-development</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    </tbody>
    </table>

    \* *

3.  Change into the Directory

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td>Windows</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:&gt;</span>cd <span class="st">&quot;%HOMEDRIVE%%HOMEPATH%</span><span class="sc">\a</span><span class="st">tlas-development&quot;</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    <tr class="even">
    <td>Linux/Mac </td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>$ cd <span class="op">~</span><span class="ss">/atlas-development</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    </tbody>
    </table>

## Create a New Confluence Plugin

Enter the command **`atlas-create-confluence-plugin`**.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Windows</td>
<td><div class="pdl code panel" style="border-width: 1px;">
<div class="panelContent pdl codeContent">
<pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:</span>...<span class="op">&gt;</span> atlas<span class="op">-</span>create<span class="op">-</span>confluence<span class="op">-</span>plugin</div></code></pre>
</div>
</div></td>
</tr>
<tr class="even">
<td>Linux/Mac</td>
<td><div class="pdl code panel" style="border-width: 1px;">
<div class="panelContent pdl codeContent">
<pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>...<span class="at">$</span> <span class="at">atlas</span><span class="op">-</span>create<span class="op">-</span>confluence<span class="op">-</span>plugin</div></code></pre>
</div>
</div></td>
</tr>
</tbody>
</table>

The **`atlas-create-confluence-plugin`** tool will prompt you for various parameters:

| Parameter  | Description                                                                                                                                                | Example     |
|------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|
| groupId    | A dot delimited string to create a namespace for plugin so that all of the modules can be uniquely identified even if another plugin users the same names. | com.example |
| artifactId | A string to uniquely identify the plugin with the **groupId** namespace                                                                                    | hello-world |
| version    | An arbitrary string to represent the version.                                                                                                              | 1.0.0       |
| package    | The package to use for the Java code. Note, dashes are not allowed in package names and the "." seperated strings will translate into a directory path.    | com.example |

 

The  tool will ask you to confirm your settings, press `y` to continue.

The tool then creates a **`helloworld`** folder with a basic plugin skeleton.

``` javascript
C:\Users\selberg\atlas-development>atlas-create-confluence-plugin
Executing: "C:\Applications\Atlassian\atlassian-plugin-sdk-6.2.2\apache-maven-3.2.1\bin\mvn.bat" com.atlassian.maven.plugins:maven-confluence-plugin:"6.2.1":create -gs C:\Applications\Atlassian\atlassian-plugin-sdk-6.2.2\apache-maven-3.2.1/conf/settings.xml
Java HotSpot(TM) 64-Bit Server VM warning: ignoring option MaxPermSize=256M; support was removed in 8.0
[INFO] Scanning for projects...
[INFO]
[INFO] Using the builder org.apache.maven.lifecycle.internal.builder.singlethreaded.SingleThreadedBuilder with a thread count of 1
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] ------------------------------------------------------------------------
[INFO]
[INFO] --- maven-confluence-plugin:6.2.1:create (default-cli) @ standalone-pom ---
[INFO] Google Analytics Tracking is enabled to collect AMPS usage statistics.
[INFO] Although no personal information is sent, you may disable tracking by adding <allowGoogleTracking>false</allowGoogleTracking> to the amps plugin configuration in your pom.xml
[INFO] Sending event to Google Analytics: AMPS:confluence - Create Plugin
[INFO] determining latest stable product version...
[INFO] using latest stable product version: 5.9.3
[INFO] determining latest stable data version...
[INFO] using latest stable data version: 5.6.6
Define value for groupId: : com.example
Define value for artifactId: : hello-world
Define value for version:  1.0.0-SNAPSHOT: : 1.0.0
Define value for package:  com.example: : com.example
Confirm properties configuration:
groupId: com.example
artifactId: hello-world
version: 1.0.0
package: com.example
 Y: :
[INFO] Setting property: classpath.resource.loader.class => 'org.codehaus.plexus.velocity.ContextClassLoaderResourceLoader'.
[INFO] Setting property: velocimacro.messages.on => 'false'.
[INFO] Setting property: resource.loader => 'classpath'.
[INFO] Setting property: resource.manager.logwhenfound => 'false'.
[INFO] Generating project in Batch mode
[INFO] Archetype repository missing. Using the one from [com.atlassian.maven.archetypes:confluence-plugin-archetype:RELEASE 
-> https://maven.atlassian.com/public] found in catalog internal
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 39,column 22] : ${atlassian.spring.scanner.version} is not a valid reference.
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 46,column 22] : ${atlassian.spring.scanner.version} is not a valid reference.
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 90,column 47] : ${atlassian.plugin.key} is not a valid reference.
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 142,column 31] : ${project.groupId} is not a valid reference.
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 142,column 50] : ${project.artifactId} is not a valid reference.
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 32.420 s
[INFO] Finished at: 2016-01-06T16:49:25-08:00
[INFO] Final Memory: 21M/328M
[INFO] ------------------------------------------------------------------------
```

{{% note %}}

The warning statements are a known issue and not a problem. See <a href="https://ecosystem.atlassian.net/browse/AMPS-57" class="uri external-link">https://ecosystem.atlassian.net/browse/AMPS-57</a> for more information.

{{% /note %}}

## (Optional) Remove the Java Code From the Plugin Scaffolding

The command, atlas-create-confluence-plugin, will provide hooks for testing and exporting a java api from the plugin. For simple plugins, such as one designed to deliver some javascript, these hooks are unnecessary complexity and can be removed.

### Removing Unnecessary Java Files

By default there will be a directory created for a plugin api at .../src/main/java/.../api and .../src/main/java/.../impl.  The api and impl directories can be deleted

{{% note %}}

If the default java files are removed, the test directories need to be removed as well as the tests will try and use the java code.

{{% /note %}}

### Removing the Test directories

Be default, there will be directories for test code under .../src/test/java and .../src/test/resources.  The java and resources directories can be deleted.

## (Optional) Link the plugin's POM.xml file to the Parent POM.xml

Unable to render {include} The included page could not be found.

## Launch the Development Instance of Confluence

{{% note %}}

Confluence may be launched from the plugin's folder or from a sibling plugin's folder if there is a parent POM.xml file. The recommended approach is to have a 'dummy' plugin named by the version of Confluece which gets instantiated.

{{% /note %}}

1.  Open a new command shell (so you can still execute commands in your existing shell)
2.  Change to the root directory of the plugin from which the development instance of Confluence will be launched.  
    *Note, the root directory of the plugin will contain the file "pom.xml" *

    |         |                                                          |
    |---------|----------------------------------------------------------|
    | Windows | c:&gt;cd "%HOMEDIR%%HOMEPATH%\\atlas-development\\5-9-4" |
    | Mac     | $ cd ~/atlas-development/5-9-4                           |

3.  Launch confluence

    ``` javascript
    $ atlas-run
    ```

    By default, the SDK instance of Confluence will be launched with the Developer Mode turned on. While this adds some extra debugging features, it slows Confluence down. If not needed, it can be turned off using the command: `atlas-run -Datlassian.dev.mode=false`

    [Read more about the Confluence Developer Mode](https://developer.atlassian.com/confdev/development-resources/confluence-developer-faq/enabling-developer-mode)

    {{% note %}}

    It can take quite some time for Confluence to get up and running. The very first time Confluence is launched through the SDK, it will download all of the necessary components. The second time, it will reuse what it downloaded early - but it still takes a while for Confluence to get up and running.

    When Confluence is ready, you will see the text:

    ``` javascript
    [INFO] confluence started successfully in XXXs at http://<myserver>:1990/confluence
    [INFO] Type Ctrl-D to shutdown gracefully
    [INFO] Type Ctrl-C to exit
    ```

    You may then open the url printed above (http://&lt;myserveer&gt;:1990/confluence) and login. The default administrator credentials are admin/admin.

    {{% /note %}}

## Log Into Confluence

To log into Confluence, visit <a href="http://localhost:1990/confluence" class="uri external-link">http://localhost:1990/confluence</a> in a web browser.  Login as the administrator (username = admin, password = admin)

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22282494/image2015-9-28%2015%3A49%3A45.png?version=1&amp;modificationDate=1455032501047&amp;api=v2" class="confluence-external-resource" height="250" />

## Package the Plugin

{{% warning %}}

The instructions below expect QuickReload to be used. QuickReload has replaced FastDEV and the atlas-cli methods for building and pushing changes to Confluence. When Confluence launches, it will scan for plugin directories to watch for changes.  Thus, to update the plugin all that is required is to re-package it. If the plugin directory does not exist when Confluence is launched, it will not be watched.

If the development plugin requires transformation, an equivalent approach is to use the **pi** command from withing the command line interface (**atlas-cli**)

{{% /warning %}}

The command below, **atlas-package**, will compile all of the java code write the appropriate manifest files and package the plugin into an OSGi bundle suitable for uploading to Confluence

``` javascript
$ altas-package
```

If the package is successfully built, a message similar to the one below will be displayed.

``` javascript
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 9.611 s
[INFO] Finished at: 2016-02-01T15:21:40-08:00
[INFO] Final Memory: 51M/389M
[INFO] ------------------------------------------------------------------------
```

## (Optional) Validate the Install

In a web browser with Confluence displayed, select the "Add-ons" from the gear menu dropdown at the top of the browser window. 

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22282685/image2015-9-28%2015%3A49%3A25.png?version=1&amp;modificationDate=1455047009147&amp;api=v2" class="confluence-external-resource" height="250" />

 

 From the "Manage add-ons" page, you should see your macro installed in the **User-installed add-ons** section.  If the plugin is in solid black text, it is installed and enabled.  If it is greyed out, it is installed by not enabled. 

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22282685/image2015-9-28%2015%3A51%3A4.png?version=1&amp;modificationDate=1455046995053&amp;api=v2" class="confluence-external-resource" height="250" />

## Add/update various module to the **`atlassian-plugin.xml`** file

 The first thing the plugin will need to to let confluence have access to it's resources in the macro-browser and preview contexts

``` javascript
    <!-- add our web resources -->
    <web-resource key="html5-multimedia-resources" name="html5-multimedia Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        
        <resource type="download" name="html5-multimedia.css" location="/css/html5-multimedia.css"/>
        <resource type="download" name="html5-multimedia.js" location="/js/html5-multimedia.js"/>
        <resource type="download" name="images/" location="/images"/>
        <context>html5-multimedia</context>
        <context>macro-browser</context>
        <context>preview</context>
    </web-resource>
```

Then a module is needed to activate some javascript that will be used to auto-populate the attachment dropdown list

``` javascript
    <!-- javascript utility for auto-filling in the attachment dropdown list -->
    <web-resource key="macro-browser-smart-fields" name="Macro Browser Smart Fields">
       <resource type="download" name="attachment-dropdown-filters.js" location="js/attachment-dropdown-filters.js" />
       <dependency>confluence.editor.actions:editor-macro-browser</dependency>
       <context>macro-browser</context>
    </web-resource>
```

Lastly, the macro itself needs to be defined.  In the parameters, there is a hidden value for **page.**  This is required for the javascript to automatically detect attachments of the current page.  The macro is designed to only show multimedia files attached to the current page, so the value is hidden to prevent users from changing it.

``` javascript
    <!-- publish our macros -->
    <xhtml-macro name="html5-multimedia" key="html5-multimedia" class="com.example.HTML5Multimedia"
       <parameters>
           <parameter name="page"              type="confluence-content" hidden="true">
              <option key="includeDatePath" value="true"/>>
           </parameter>
           <parameter name="name"              type="attachment" />
           <parameter name="url"               type="string" />
           <parameter name="preset"            type="enum">
               <value name="640x480 (4:3 VGA)" />
               <value name="800x600 (4:3 SVGA)" />
               <value name="1024x768 (4:3 XGA)" />
               <value name="1600x1200 (4:3 SXGA+)" />
               <value name="1280x1024 (5:4 SXGA)" />
               <value name="640x360 (16:9)" />
               <value name="960x540 (16:9 qHD)" />
               <value name="1280x720 (16:9 HD)" />
               <value name="1920x1080 (16:9 Full HD)" />
          </parameter>
          <parameter name="width"              type="int" />
          <parameter name="height"             type="int" />
          <parameter name="poster"             type="string"  />
          <parameter name="autoplay"           type="boolean" />
          <parameter name="loop"               type="boolean" />
          <parameter name="is-audio"           type="boolean" />
          <parameter name="hide-download-link" type="boolean" />
       </parameters>
    </xhtml-macro>
```

For this use case, the macro is being designed to host a multimedia file attached to the page (**name**)** **or one that is hosted externally to Confluence (**url**).  One of the two is required, however, the parameter syntax does not support the "this or that is required" logic.  An alternate approach is to declare two macros that use the same source code but one requires the url and the other requires the attachment.  If a parameter is required ( via the attribute *required="true"* ), then the macro browser will automatically open and not allow the macro to be inserted unless a value is provided. 

{{% note %}}

The xhtml macro describes the macro.  For a full list of the types and options available for the xhtml macro, see the document in the Confluence plugin guide on "[Including Information in your Macro for the Macro Browser](https://developer.atlassian.com/confdev/confluence-plugin-guide/confluence-plugin-module-types/macro-module/including-information-in-your-macro-for-the-macro-browser)".

{{% /note %}}

**Complete atlassian-plugin.xml**  Expand source

``` xml
<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>
    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="html5-multimedia"/>
    
    <!-- add our web resources -->
    <web-resource key="html5-multimedia-resources" name="html5-multimedia Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        
        <resource type="download" name="html5-multimedia.css" location="/css/html5-multimedia.css"/>
        <resource type="download" name="html5-multimedia.js" location="/js/html5-multimedia.js"/>
        <resource type="download" name="images/" location="/images"/>
        <context>html5-multimedia</context>
        <context>macro-browser</context>
        <context>preview</context>
    </web-resource>
    <!-- javascript utility for auto-filling in the attachment dropdown list -->
    <web-resource key="macro-browser-smart-fields" name="Macro Browser Smart Fields">
       <resource type="download" name="attachment-dropdown-filters.js" location="js/attachment-dropdown-filters.js" />
       <dependency>confluence.editor.actions:editor-macro-browser</dependency>
       <context>macro-browser</context>
    </web-resource>
    <!-- publish our macros -->
    <xhtml-macro name="html5-multimedia" key="html5-multimedia" class="com.example.HTML5Multimedia"
       <parameters>
           <parameter name="page"             type="confluence-content" hidden="true">
              <option key="includeDatePath" value="true"/>>
           </parameter>
           <parameter name="name"              type="attachment" />
           <parameter name="url"               type="string" />
           <parameter name="preset"            type="enum">
               <value name="640x480 (4:3 VGA)" />
               <value name="800x600 (4:3 SVGA)" />
               <value name="1024x768 (4:3 XGA)" />
               <value name="1600x1200 (4:3 SXGA+)" />
               <value name="1280x1024 (5:4 SXGA)" />
               <value name="640x360 (16:9)" />
               <value name="960x540 (16:9 qHD)" />
               <value name="1280x720 (16:9 HD)" />
               <value name="1920x1080 (16:9 Full HD)" />
          </parameter>
          <parameter name="width"              type="int" />
          <parameter name="height"             type="int" />
          <parameter name="poster"             type="string"  />
          <parameter name="autoplay"           type="boolean" />
          <parameter name="loop"               type="boolean" />
          <parameter name="is-audio"           type="boolean" />
          <parameter name="hide-download-link" type="boolean" />
       </parameters>
    </xhtml-macro>
    
</atlassian-plugin>
```

## Add Javascript to Activate the Attachment Dropdown Auto-population Method

The javascript to automatically populate the attachment dropdown is provided by Confluence - however, it needs to be told which attachments may be include according to their file extension.

Create the file .../html5-multimedia/src/main/resources/js/attachment-dropdown-filters.js with the following content

``` javascript
(function($) {
  var audioFileTypes = ["mp3", "ogg", "wav"];
  var videoFileTypes = ["mp4", "ogg", "webm"];
  var multimediaFileTypes = videoFileTypes.concat( audioFileTypes );
 
  AJS.MacroBrowser.activateSmartFieldsAttachmentsOnPage("html5-multimedia", multimediaFileTypes );
})(AJS.$);
```

This will tell the macrobrowser, that for the html5-multimedia plugin the attachments dropdown should be autopopulated with attachments of the type mp3, ogg, wav, mp4 and webm from the selected page.  By default, the page will be the current page.  It can be convienent to set the attribute *hidden="true"* on the page parameter so that only attachments from the current page are included.

{{% note %}}

The parameter names **page** and **name** are referenced in the javascript, so they cannot be changed without breaking the auto-population functionality.

{{% /note %}}

## The Java Code

### The Macro Skeleton

The most basic macro is going to look like the following

**HTML5Multimedia.java**

``` java
package com.example;

import java.util.Map;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.content.render.xhtml.ConversionContext


public class HTML5Multimedia implements Macro
{
    // object constructor
    public HTML5Multimedia()
    {
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException
    {
        return "";
    }
    
    @Override
    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }
    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }
}
```

The BodyType can take the values of NONE, PLAIN\_TEXT and RICH\_TEXT.  If the BodyType is none, the macro will be displayed as a solid rectangle in the Confluence editor.  If the BodyType is PLAIN\_TEXT, the macro will be a hollow rectangle and the user will be able to enter plain text.  For last value, RICH\_TEXT, the author can insert add content into the macro body using all of the editor's features including macros.

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22839472/image2016-2-17%2010%3A14%3A33.png?version=1&amp;modificationDate=1455732873910&amp;api=v2" class="confluence-external-resource" />

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22839472/image2016-2-17%2010%3A15%3A50.png?version=1&amp;modificationDate=1455732950927&amp;api=v2" class="confluence-external-resource" />

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22839472/image2016-2-17%2010%3A16%3A49.png?version=1&amp;modificationDate=1455733009307&amp;api=v2" class="confluence-external-resource" />

The OutputType can take the values of BLOCK and INLINE.  It determines if the widgit is contained block tag which gets a new paragraph or just put inline with the html.

When the page is loaded, the execute function will be called and the output of the **execute** method will be inserted into the page.

### Including Confluence Services

There are a number of exceptionally useful objects provided by Confluence.  For example a SettingsManager which can provide the current url for Confluence and the VelocityHelperService that can transform velocity templates into HTML. There are three patterns for pulling these components into your java class: setter based injection, constructor based injection, annotation based injection.

The setter based injection is mostly deprecated and is only referenced here for completeness.  The constructor based injection is use for plugins that require transformation.  Annotation based injection is used for transformerless plugins.  The syntax for the constructor and annotation is identical with the exception of needing to add annotations such as "@Component-Import" in front of the class to be imported.

{{% note %}}

The list of OSGi components that may be imported can be viewed in your confluence instance at: **&lt;your\_confluence\_url&gt;/admin/pluginexports.action**

{{% /note %}}

The HTML5-Multimedia needs three services

-   AttachmentManager - This is used to get the full download path to the attachment by it's name.
-   SettingsManager - This is used to get the confluence base url.
-   VelocityHelperService - This is used to transform velocity templates into HTML.

#### Constructor Injection

Below is the code necessary for pulling in the three services into the macro using constructor injection.

**HTML5Multimedia.java**

``` java
package com.example;
...
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.setup.settings.SettingsManager;
...
public class HTML5Multimedia implements Macro
{
    private final AttachmentManager     attachmentManager;
    private final SettingsManager       settingsManager;
    private final VelocityHelperService velocityHelperService;
    
    // object constructor
    public HTML5Multimedia( final AttachmentManager attachmentManager,
                            final SettingsManager settingsManager,
                            final VelocityHelperService velocityHelperService)
    {
        this.attachmentManager     = attachmentManager;
        this.settingsManager       = settingsManager;
        this.velocityHelperService = velocityHelperService;
    }

    ...
}
```

#### Annotation Injection

With annotation injection, two things need to be done.  The packages containing the components to be imported need to be specified in the &lt;import-package&gt; element of the POM.xml file and at least one declaration for each of the objects needs to be annotated with @ComponentImport.  Because only one annotation per imported class is required, there are two strategies that can be employed.  The first is to add the annotation inline with the plugin code.  The second is to create a dummy class where the annotations are declared.  The second approach has the disadvantage of having another file to create and manage, but the advantage of centralizing annotations and keeping the macro code portable between transformerless and transformation required builds.  For this tutorial, the syntax for inline annotation is shown however, the dummy class approach is used.

#### POM.xml Import-Package Declarations

**POM.xml (excerpt only)**

``` xml
... 
                        <!-- Add package import here -->
                        <Import-Package>
                            org.springframework.osgi.*;resolution:="optional",
                            org.eclipse.gemini.blueprint.*;resolution:="optional",
                            com.atlassian.confluence.pages,
                            com.atlassian.confluence.plugin.services,
                            com.atlassian.confluence.setup.settings,
                            *
                        </Import-Package>
...
```

**POM.xml (full)**  Expand source

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
   
    <artifactId>html5-multimedia</artifactId>
    <version>1.0.0</version>
    <name>html5-multimedia</name>
    <description>This is the com.example:html5-multimedia plugin for Atlassian Confluence.</description>
   
    <properties>       
        <!-- This key is used to keep the consistency between the key in atlassian-plugin.xml and the key to generate bundle. -->
        <atlassian.plugin.key>${project.groupId}.${project.artifactId}</atlassian.plugin.key>
    </properties>
    <modelVersion>4.0.0</modelVersion>
    <packaging>atlassian-plugin</packaging>
    <parent>
        <groupId>com.example</groupId>
        <artifactId>parent</artifactId>
        <version>1.0.0</version>
    </parent>
     <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-confluence-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <productVersion>${confluence.version}</productVersion>
                    <productDataVersion>${confluence.data.version}</productDataVersion>
                    <enableQuickReload>true</enableQuickReload>
                    <enableFastdev>false</enableFastdev>
                    <encoding>${encoding}</encoding>
                    <instructions-gone>
                        <Atlassian-Plugin-Key-disabled>${atlassian.plugin.key}</Atlassian-Plugin-Key-disabled>
  
                        <!-- Add package to export here -->
                        <Export-Package>
                        </Export-Package>
  
                        <!-- Add package import here -->
                        <Import-Package>
                            org.springframework.osgi.*;resolution:="optional",
                            org.eclipse.gemini.blueprint.*;resolution:="optional",
                            com.atlassian.confluence.pages,
                            com.atlassian.confluence.plugin.services,
                            com.atlassian.confluence.setup.settings,
                            *
                        </Import-Package>
  
                        <!-- Ensure plugin is spring powered - see https://extranet.atlassian.com/x/xBS9hQ  -->
                        <Spring-Context>*</Spring-Context>
                    </instructions-gone>
                </configuration>
            </plugin>
        </plugins>
    </build>
  
</project>
```

 

#### Inline Code Annotation (Example only)

{{% note %}}

Note, the inline code example is only shown for example only.

{{% /note %}}

**HTML5Multimedia.java**

``` java
package com.example;
...
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
...
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.setup.settings.SettingsManager;
...
@Scanned
public class HTML5Multimedia implements Macro
{
    @ComponentImport private final AttachmentManager     attachmentManager;
    @ComponentImport private final SettingsManager       settingsManager;
    @ComponentImport private final VelocityHelperService velocityHelperService;
    
    // object constructor
    public HTML5Multimedia( final AttachmentManager attachmentManager,
                            final SettingsManager settingsManager,
                            final VelocityHelperService velocityHelperService)
    {
        this.attachmentManager     = attachmentManager;
        this.settingsManager       = settingsManager;
        this.velocityHelperService = velocityHelperService;
    }

    ...
}
```

#### Dummy Class Annotation

**ComponentImports.java**

``` java
package com.example;
 
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
 
/**
 * This class is used to replace <component-import /> declarations in the atlassian-plugin.xml.
 * This class will be scanned by the atlassian spring scanner at compile time.
 * There are no situations this class needs to be instantiated, it's sole purpose 
 * is to collect the component-imports in the one place.
 *
 * The list of components that may be imported can be found at: <your-confluence-url>/admin/pluginexports.action
 */
@SuppressWarnings("UnusedDeclaration")
@Scanned
public class ComponentImports
{
    @ComponentImport com.atlassian.confluence.plugin.services.VelocityHelperService velocityHelperService;
    @ComponentImport com.atlassian.confluence.setup.settings.SettingsManager        settingsManager;
    @ComponentImport com.atlassian.confluence.pages.AttachmentManager               attachmentManager;
  
    private ComponentImports()
    {
        throw new Error("This class should not be instantiated");
    }
}
```

### The execute() method

For this particular macro, there is not much to do in the source code.  The job is to take the parameters passed in, perhaps do a bit of massaging of the values and stick it all into an html5 &lt;video&gt; or &lt;audio&gt; tag.  In order to avoid the tediousness of constructing html via string concatenation, a velocity template will be used.

Below is a simplified version of the HTML5Multimedia execute method.  The macro definition has quite a few parameters and for the sake of introducing the execute function in this tutorial, the execute function has been trimmed down to only parsing two of the parameters (loop and is-audio).

**execute method (simplified)**

``` java
    //Macro Browser Keys
    public static final String MACRO_EDITOR_LOOP_KEY = "loop";
    public static final String MACRO_EDITOR_IS_AUDIO_KEY = "is-audio"
    ...
 
    //Velocity Template Keys
    public static final String LOOP_KEY = MACRO_EDITOR_LOOP_KEY;
    ...
 
    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException
    {
        Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();
        
        String videoTemplate = "/com/example/templates/html5-video.vm";
        String audioTemplate = "/com/example/templates/html5-audio.vm";
        String template      = "";
             
        if( parameters.containsKey(MACRO_EDITOR_LOOP_KEY) ){
           velocityContext.put(LOOP_KEY, "true");
        }
           
        if( parameters.containsKey(MACRO_EDITOR_IS_AUDIO_KEY) ){
           template = audioTemplate;
        } else {
           template = videoTemplate;
        }
        return velocityHelperService.getRenderedTemplate(template, velocityContext);
    }
```

**execute method (full)**  Expand source

``` javascript
    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException
    {
        Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();
        
        String videoTemplate = "/com/example/templates/html5-video.vm";
        String audioTemplate = "/com/example/templates/html5-audio.vm";
        String template = "";
        String fileExtension = "";
        String url           = "";
        String fileName;
        String baseUrl  = settingsManager.getGlobalSettings().getBaseUrl();
    
        if( parameters.containsKey( MACRO_EDITOR_URL_KEY ) ){
           url = parameters.get( MACRO_EDITOR_URL_KEY );
           if( parameters.containsKey( MACRO_EDITOR_URL_MEDIA_TYPE_KEY ) && !StringUtils.isEmpty( parameters.get( MACRO_EDITOR_URL_MEDIA_TYPE_KEY ) ) ){
              fileExtension = parameters.get( MACRO_EDITOR_URL_MEDIA_TYPE_KEY );
           } else if( url.length() > 3 ) {
              fileExtension = url.substring( url.length() - 3 );
           }
           velocityContext.put(INCLUDE_MEDIA_KEY, "true" );
        } else {
           fileName = (String) parameters.get( MACRO_EDITOR_ATTACHMENT_KEY );
           Attachment attachment  = attachmentManager.getAttachment( context.getEntity(), fileName );
        
           url           = baseUrl + (String) attachment.getDownloadPath();
           fileExtension = (String) attachment.getFileExtension();   
           velocityContext.put(FILE_NAME_KEY, fileName );
        }
        if( url != null ){
           // force the controls on.
           velocityContext.put(ADD_CONTROLS_KEY, "controls" );
           
           velocityContext.put(URL_KEY, url );
           velocityContext.put(FILE_EXTENSION_KEY, fileExtension );        
        
           if( parameters.containsKey(MACRO_EDITOR_POSTER_KEY) ){
              velocityContext.put(POSTER_KEY, (String) parameters.get(MACRO_EDITOR_POSTER_KEY));
           }
           
           if( parameters.containsKey(MACRO_EDITOR_LOOP_KEY) ){
              velocityContext.put(LOOP_KEY, "true");
           }
           
           if( parameters.containsKey(MACRO_EDITOR_AUTOPLAY_KEY) ){
              velocityContext.put(AUTOPLAY_KEY, "true");
           } else if( parameters.containsKey(MACRO_EDITOR_AUTOSTART_KEY) ){
              velocityContext.put(AUTOPLAY_KEY, "true");
           }
           
           if( !parameters.containsKey(MACRO_EDITOR_HIDE_DOWNLOAD_LINK_KEY) ){
              velocityContext.put(ADD_DOWNLOAD_LINK_KEY, "true");
           }
           if( parameters.containsKey(MACRO_EDITOR_PRESET_KEY) ){
              String preset = (String) parameters.get(MACRO_EDITOR_PRESET_KEY);
              if( preset.equals( "640x480 (4:3 VGA)" ) ){
                 velocityContext.put(WIDTH_KEY, "640" );
                 velocityContext.put(HEIGHT_KEY, "480" );
              } else if( preset.equals( "800x600 (4:3 SVGA)" ) ){
                 velocityContext.put(WIDTH_KEY, "800" );
                 velocityContext.put(HEIGHT_KEY, "600" );
              } else if( preset.equals( "1024x768 (4:3 XGA)" ) ){
                 velocityContext.put(WIDTH_KEY, "1024" );
                 velocityContext.put(HEIGHT_KEY, "768" );
              } else if( preset.equals( "1600x1200 (4:3 SXGA+)" ) ){
                 velocityContext.put(WIDTH_KEY, "1600" );
                 velocityContext.put(HEIGHT_KEY, "1200" );
              } else if( preset.equals( "1280x1024 (5:4 SXGA)" ) ){
                 velocityContext.put(WIDTH_KEY, "1280" );
                 velocityContext.put(HEIGHT_KEY, "1024" );
              } else if( preset.equals( "640x360 (16:9)" ) ){
                 velocityContext.put(WIDTH_KEY, "640" );
                 velocityContext.put(HEIGHT_KEY, "360" );
              } else if( preset.equals( "960x540 (16:9 qHD)" ) ){
                 velocityContext.put(WIDTH_KEY, "960" );
                 velocityContext.put(HEIGHT_KEY, "540" );
              } else if( preset.equals( "1280x720 (16:9 HD)" ) ){
                 velocityContext.put(WIDTH_KEY, "1280" );
                 velocityContext.put(HEIGHT_KEY, "720" );
              } else if( preset.equals( "1920x1080 (16:9 Full HD)" ) ){
                 velocityContext.put(WIDTH_KEY, "1920" );
                 velocityContext.put(HEIGHT_KEY, "1080" );
              }
           } else {
              if( parameters.containsKey(MACRO_EDITOR_WIDTH_KEY) ){
                 velocityContext.put(WIDTH_KEY, (String) parameters.get(MACRO_EDITOR_WIDTH_KEY));
              }
              if( parameters.containsKey(MACRO_EDITOR_HEIGHT_KEY) ){
                 velocityContext.put(HEIGHT_KEY, (String) parameters.get(MACRO_EDITOR_HEIGHT_KEY));
              }
           }
        }
        if( parameters.containsKey(MACRO_EDITOR_IS_AUDIO_KEY) ){
           template = audioTemplate;
        } else if( fileExtension.equals( "mp3" ) || fileExtension.equals( "wav" ) ) {
           template = audioTemplate;
        } else {
           template = videoTemplate;
        }
        return velocityHelperService.getRenderedTemplate(template, velocityContext);
    }
```

 

The prototype of execute is: String execute( Map&lt;String, String&gt; parameters, String body, ConversionContext context  )

**Parameters** is a map of the keys declared in the &lt;xhtml-macro&gt; element of the atlassian-plugin.xml file and the values set in the macro browser by the author.  The body is the content entered into the body of the macro by the author.  As the html5-multimedia macro has no body, this will be an empty string. lastly is the **context**.  This last object contains information such as the current page (*context.getEntity()*).

The test if a value exists for a particuar parameter is *parameters.containsKey( "key-name"* ); This function will return true if the key exists and false otherwise.  If no value was set in the macro browser, the key is not passed in even though it was defined in the &lt;xhtml-macro&gt; element.

The return of the execute() method is expected to be the HTML to be inserted where the macro was placed.

#### Using Velocity

 To use a velocity template, three things are required: The velocity rendering engine, a velocity context and a velocity template.

The velocity rendering engine is provided by the VelocityHelperServer which was imported earlier

The VelocityHelperService can generate a default context that contains variables and objects that the Confluence velocity environment expects to be available:

``` javascript
Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();
```

To insert additional variables into the context, use the "put" method.

``` javascript
velocityContext.put( key, value );
```

The velocity template is a file either in the plugin's resources directory or one of it's sub folders.  The path to the plugin needs to be provided to the velocity rendering engine.  If a velocity template is placed at**.../src/main/resources/com/example/templates/html5-video.vm**, the velocity rendering engine will look for it at "/com/example/templates/html5-video.vm".

{{% warning %}}

When the velocity engine is looking for a template, it can see all of the templates for all of the loaded plugins but can't tell which template belongs to which plugin. So, if two plugins have distinct templates that happen to share a common name and path, the velocity rendering engine will use one for both plugins. It is therefore a best practice to create a "name space" folder tree under the resources folder. For example, .../src/main/resources/com/example/templates/html5-video.vm

{{% /warning %}}

A velocity template is really an html file with some special syntax that the velocity engine knows how to work with.  The system has an entire <a href="https://velocity.apache.org/engine/releases/velocity-1.5/user-guide.html" class="external-link">website</a> dedicated to it's usage. For this example, only the directives below are used:

Comments start with \#\#

*\#\# is a comment*

Context Variables/Objects are accessed with $variableName

*$url is the value for the key "url" inserted into the velocity context.*

If statement

*\#if( )*  
*\#else*  
*\#end *

**.../resources/com/example/templates/html5-video.vm**

``` javascript
## Developed by: Scott Selberg
## Leveraging code from : David Simpson
## Date created: 02/02/2015
 
#if( $url )
<video
    #if ($addControls) controls="controls"  #end
    #if ($autoplay)    autoplay="autoplay"  #end
    #if ($loop)        loop="loop"          #end
    #if ($width)       width="$width"       #end
    #if ($height)      height="$height"     #end
    #if ($poster)      poster="$poster"     #end
>
    <source type="video/$fileextension" src="$url" />
    <object type="application/x-shockwave-flash" data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf"
        #if ($width)  width="$width"   #else width="640"  #end
        #if ($height) height="$height" #else height="360" #end
        >
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowFullScreen" value="true" />
        <param name="wmode" value="transparent" />
        <param name="flashVars" value="config={'playlist':[{'url':'$generalUtil.urlEncode("$action.getGlobalSettings().getBaseUrl()$url")','autoPlay':false}]}" />
        <span title="No video playback capabilities, please download the video from the page attachments"></span>
    </object>
</video>
#if( $addDownloadLink && $filename ) <p class="html5-multimedia html5-video"><a href="$url">Download $filename</a></p> #end
#else
#if( $includeMedia )
<p>No url specified</p>
#else
<p>No attachment specified</p>
#end
#end
```

**.../resources/com/example/templates/html5-audio.vm**  Expand source

``` javascript
## Developed by: Scott Selberg
## Leveraging code from : David Simpson
## Date created: 02/02/2015
 
#if( $url )
<audio
    #if ($addControls) controls="controls" #end
    #if ($autoplay)    autoplay="autoplay" #end
    #if ($loop)        loop="loop"         #end
>
    <source type="audio/$fileextension" src="$url" />
</audio>
#if( $filename && $addDownloadLink ) <p class="html5-multimedia html5-audio"><a href="$url">Download $filename</a></p> #end
#else

#if( $includeMedia )
<p>No url specified</p>
#else
<p>No attachment specified</p>
#end

#end
```

## Re-package to Add in the New Changes

{{% warning %}}

The instructions below expect QuickReload to be used. QuickReload has replaced FastDEV and the atlas-cli methods for building and pushing changes to Confluence. When Confluence launches, it will scan for plugin directories to watch for changes.  Thus, to update the plugin all that is required is to re-package it. If the plugin directory does not exist when Confluence is launched, it will not be watched.

If the development plugin requires transformation, an equivalent approach is to use the **pi** command from withing the command line interface (**atlas-cli**)

{{% /warning %}}

The command below, **atlas-package**, will compile all of the java code write the appropriate manifest files and package the plugin into an OSGi bundle suitable for uploading to Confluence

``` javascript
$ altas-package
```

If the package is successfully built, a message similar to the one below will be displayed.

``` javascript
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 9.611 s
[INFO] Finished at: 2016-02-01T15:21:40-08:00
[INFO] Final Memory: 51M/389M
[INFO] ------------------------------------------------------------------------
```

## Fist View

The macro should be fully functional at this point in time.  Log into Confluence.  Create a new page.  Attach an mp4 file, then insert the html5-multimedia macro.  When you open the html5-multimedia macro in the macro browser, you should see the mp4 file in the list of available attachments.  Save the macro and the page and the video should be visible with a browser that supports html5. 

## Providing Descriptive Parameters

By default, confluence will look for particular keys in the i18n properties file (**html5-multimedia.properties**) to supply the displayed text and description for a parameter value. There are 4 types of values for every macro which are listed below with the accompanying i18n syntax:

|                             |                                                                               |
|-----------------------------|-------------------------------------------------------------------------------|
| Macro Name                  | &lt;*atlassian-plugin-key*&gt;.&lt;*macro-key*&gt;.label                      |
| Macro Description           | &lt;*atlassian-plugin-key*&gt;.&lt;*macro-key*&gt;.desc                       |
| Macro Parameter Name        | &lt;*atlassian-plugin-key*&gt;.&lt;*macro-key*&gt;.&lt;*param-name*&gt;.label |
| Macro Parameter Description | &lt;*atlassian-plugin-key*&gt;.&lt;*macro-key*&gt;.&lt;*param-name*&gt;.desc  |

The &lt;*atlassian.plugin.key*&gt; needs to match the value of what is defined in the ${atlassian.plugin.key} variable of the plugin's POM.xml file.  For this example, it is **com.example.html5-multimedia**.

The &lt;*macro-key*&gt; is whatever the key for the &lt;xhtml&gt; macro is.  For this case, it is **html5-multimedia**.

The &lt;*param-name*&gt; needs to match the value for the name attribute of the &lt;parameter&gt; element.  The values for the html5-multimedia plugin are provided below.

**html5-multimedia.properties**  Expand source

``` javascript
#input any key/value pairs here
com.example.html5-multimedia.html5-multimedia.label=HTML5 Multimedia
com.example.html5-multimedia.html5-multimedia.desc=Insert an audio or video file using HTML5.
com.example.html5-multimedia.html5-multimedia.param.name.label=Attachment
com.example.html5-multimedia.html5-multimedia.param.name.desc=Supported file formats: mp4, ogg, webm, mp3 or wav
com.example.html5-multimedia.html5-multimedia.param.preset.label=Video dimension presets
com.example.html5-multimedia.html5-multimedia.param.preset.desc=
com.example.html5-multimedia.html5-multimedia.param.width.label=Width
com.example.html5-multimedia.html5-multimedia.param.width.desc=Width of the file to be displayed, in pixels or a percentage of the page.  Example 500, 100%
com.example.html5-multimedia.html5-multimedia.param.height.label=Height
com.example.html5-multimedia.html5-multimedia.param.height.desc=Height of the file to be displayed, in pixels or a percentage of the page.  Example: 200, 20%
com.example.html5-multimedia.html5-multimedia.param.poster.label=Poster
com.example.html5-multimedia.html5-multimedia.param.poster.desc=The url for a static picture to show before the video starts.
com.example.html5-multimedia.html5-multimedia.param.autoplay.label=Autoplay 
com.example.html5-multimedia.html5-multimedia.param.autoplay.desc=If set, playback will start automatically.
com.example.html5-multimedia.html5-multimedia.param.loop.label=Loop playback
com.example.html5-multimedia.html5-multimedia.param.loop.desc=
com.example.html5-multimedia.html5-multimedia.param.is-audio.label=File contains audio only 
com.example.html5-multimedia.html5-multimedia.param.is-audio.desc=For formats that supports both audio and video (mp4 and ogg), but only audio is supplied.
com.example.html5-multimedia.html5-multimedia.param.add-download-link.label=Add a link to download the attachment
com.example.html5-multimedia.html5-multimedia.param.add-download-link.desc=
```

[For the real go-getter, a REST interface can be added to create a link to provide documentation on how to use the macro](/server/confluence/html5-multimedia.snippet).

## Icons

Icons can be added for the plugin and the macro.  The actual image files should be placed in the .../src/main/resources/images folder.  There are 6 images which could be useful:

|               |                  |
|---------------|------------------|
| Plugin Icon   | 16x16 png file   |
| Plugin Logo   | 144x144 png file |
| Plugin Banner | 560.274 png file |
| Vendor Icon   | 16x16 png file   |
| Vendor Logo   | 72x72 png file   |
| Macro Icon    | 80x80 png file   |

 

The macro icon is specified as an attribute of the &lt;xhtml-macro&gt; element

``` javascript
<xhtml-macro name="html5-multimedia" key="html5-multimedia" class="com.example.HTML5Multimedia"
             icon="/download/resources/${atlassian.plugin.key}:html5-multimedia-resources/images/html5-multimedia-80x80.png">
...
</xhtml-macro>
```

The plugin and vendor images are specified in the &lt;plugin-info&gt; element

``` javascript
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/plugin-icon-16x16.png</param>
        <param name="plugin-logo">images/plugin-logo-144x144.png</param>
        <param name="plugin-banner">images/plugin-banner-560x274.png</param>
        <param name="vendor-icon">images/vendor-icon-16x16.png</param>
        <param name="vendor-logo">images/vendor-logo-72x72.png</param>
    </plugin-info>
```

## Placing the Macro into Browser Categories

The macro browser has several pre defined categories of macros to assist users in locating a macro.  A macro is placed into one or more of the categores by the presence of one or more &lt;category name="*category-name*"/&gt; child element of the &lt;xhtml-macro&gt; parent.

The valid values for *category-name* are

-   formatting
-   confluence-content
-   media
-   visuals
-   navigation
-   external-content
-   communication
-   reporting
-   admin
-   development

## (Optional) Replacing the built in Multimedia Macro

By default, when a video or audio file is added to a page, Confluence will automatically insert a multimedia macro to display the element.  The Confluence multimedia macro does not use html5 tags which can cause problems with modern browsers.  To replace the default multimedia macro, create a second disabled &lt;xhtml-macro&gt; element with an identical key and parameters to the Atlassian multimedia macro.  (The key happens to be **multimedia**.)  The html5 macro can then be uploaded into Confluence.  An administrator can then disable the Atlassian multimedia macro and enable the HTML5 one.  Now, every declaration of the multimedia macro across Confluence will use the new plugin to render the html.

``` javascript
    <!-- This is an alias for the html5-video macro so that it can override the default confluence multimedia macro for drag&drop -->
    <!-- This macro has the same parameters as the regular confluence macro -->
    <xhtml-macro name="multimedia" key="multimedia" class="com.example.HTML5Multimedia" state="disabled"
                 icon="/download/resources/${atlassian.plugin.key}:html5-multimedia-resources/images/html5-multimedia-80x80.png">
        <category name="media"/>
        <category name="confluence-content"/>
        <parameters>
            <parameter name="page" type="confluence-content" hidden="true">
                <option key="includeDatePath" value="true"/>
            </parameter>
            <parameter name="name" type="attachment" required="true">
                <alias name=""/>
            </parameter>
            <parameter name="width"     type="string"  required="false" />
            <parameter name="height"    type="string"  required="false" />
            <parameter name="autostart" type="boolean" required="false" />
        </parameters>
    </xhtml-macro>
```

**The final atlassian-plugin.xml file**  Expand source

``` xml
 <atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/plugin-icon-16x16.png</param>
        <param name="plugin-logo">images/plugin-logo-144x144.png</param>
        <param name="plugin-banner">images/plugin-banner-560x274.png</param>
        <param name="vendor-icon">images/vendor-icon-16x16.png</param>
        <param name="vendor-logo">images/vendor-logo-72x72.png</param>
    </plugin-info>

    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="html5-multimedia"/>
    
    <!-- add our web resources -->
    <web-resource key="html5-multimedia-resources" name="html5-multimedia Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        
        <resource type="download" name="html5-multimedia.css" location="/css/html5-multimedia.css"/>
        <resource type="download" name="html5-multimedia.js" location="/js/html5-multimedia.js"/>
        <resource type="download" name="images/" location="/images"/>

        <context>html5-multimedia</context>
        <context>macro-browser</context>
        <context>preview</context>
    </web-resource>

    <!-- javascript utility for auto-filling in the attachment dropdown list -->
    <web-resource key="macro-browser-smart-fields" name="Macro Browser Smart Fields">
       <resource type="download" name="attachment-dropdown-filters.js" location="js/attachment-dropdown-filters.js" />
       <dependency>confluence.editor.actions:editor-macro-browser</dependency>
       <context>macro-browser</context>
    </web-resource>

    <!-- publish our macros -->
    <xhtml-macro name="html5-multimedia" key="html5-multimedia" 
                 class="com.example.HTML5Multimedia"
                 icon="/download/resources/${atlassian.plugin.key}:html5-multimedia-resources/images/html5-multimedia-80x80.png">
       <category name="media"/>
       <category name="confluence-content"/>
       <parameters>
           <parameter name="page"             type="confluence-content" hidden="true">
              <option key="includeDatePath" value="true"/>>
           </parameter>
           <parameter name="name"             type="attachment" />
           <parameter name="url"              type="string" />
           <parameter name="preset"           type="enum">
               <value name="640x480 (4:3 VGA)" />
               <value name="800x600 (4:3 SVGA)" />
               <value name="1024x768 (4:3 XGA)" />
               <value name="1600x1200 (4:3 SXGA+)" />
               <value name="1280x1024 (5:4 SXGA)" />
               <value name="640x360 (16:9)" />
               <value name="960x540 (16:9 qHD)" />
               <value name="1280x720 (16:9 HD)" />
               <value name="1920x1080 (16:9 Full HD)" />
           </parameter>
           <parameter name="width"              type="int" />
           <parameter name="height"             type="int" />
           <parameter name="poster"             type="string"  />
           <parameter name="autoplay"           type="boolean" />
           <parameter name="loop"               type="boolean" />
           <parameter name="is-audio"           type="boolean" />
           <parameter name="hide-download-link" type="boolean" />
       </parameters>
    </xhtml-macro>

    <!-- This is an alias for the html5-video macro so that it can override the default confluence multimedia macro for drag&drop -->
    <!-- This macro has the same parameters as the regular confluence macro -->
    <xhtml-macro name="multimedia" key="multimedia" class="com.example.HTML5Multimedia" state="disabled"
                 icon="/download/resources/${atlassian.plugin.key}:html5-multimedia-resources/images/html5-multimedia-80x80.png">
        <category name="media"/>
        <category name="confluence-content"/>
        <parameters>
            <parameter name="page" type="confluence-content" hidden="true">
                <option key="includeDatePath" value="true"/>
            </parameter>
            <parameter name="name" type="attachment" required="true">
                <alias name=""/>
            </parameter>
            <parameter name="width"     type="string"  required="false" />
            <parameter name="height"    type="string"  required="false" />
            <parameter name="autostart" type="boolean" required="false" />
        </parameters>
    </xhtml-macro>
    
</atlassian-plugin>
```

## Re-package to Add in the New Changes

{{% warning %}}

The instructions below expect QuickReload to be used. QuickReload has replaced FastDEV and the atlas-cli methods for building and pushing changes to Confluence. When Confluence launches, it will scan for plugin directories to watch for changes.  Thus, to update the plugin all that is required is to re-package it. If the plugin directory does not exist when Confluence is launched, it will not be watched.

If the development plugin requires transformation, an equivalent approach is to use the **pi** command from withing the command line interface (**atlas-cli**)

{{% /warning %}}

The command below, **atlas-package**, will compile all of the java code write the appropriate manifest files and package the plugin into an OSGi bundle suitable for uploading to Confluence

``` javascript
$ altas-package
```

If the package is successfully built, a message similar to the one below will be displayed.

``` javascript
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 9.611 s
[INFO] Finished at: 2016-02-01T15:21:40-08:00
[INFO] Final Memory: 51M/389M
[INFO] ------------------------------------------------------------------------
```

[&lt; Giants Color Theme](/server/confluence/giants-color-theme.snippet)
