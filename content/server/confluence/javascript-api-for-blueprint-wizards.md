---
aliases:
- /server/confluence/javascript-api-for-blueprint-wizards-22020359.html
- /server/confluence/javascript-api-for-blueprint-wizards-22020359.md
category: devguide
confluence_id: 22020359
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=22020359
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=22020359
date: '2018-03-28'
guides: guides
legacy_title: Javascript API for blueprint wizards
platform: server
product: confluence
subcategory: learning
title: Javascript API for blueprint wizards
---
# JavaScript API for blueprint wizards

This page gives details about the JavaScript API that supports blueprint wizards. This API was released with Confluence 5.1.
For more information, see [Preparing for Confluence 5.1](/server/confluence/preparing-for-confluence-5-1/).

## Overview of the API

The Confluence blueprint features include a JavaScript `setWizard()` API. You can use this API to access one of the three hooks.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Hook</p></th>
<th><p>Usage</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>pre-render</p></td>
<td><p>Called before the system renders the Soy template. Use to add data to the Soy rendering context.</p></td>
</tr>
<tr class="even">
<td><p>post-render</p></td>
<td><p>Called after the system renders the Soy template. Used to add JavaScript behavior to the wizard page.</p></td>
</tr>
<tr class="odd">
<td><p>submit</p></td>
<td><p>Called when the dialog is submitted. Use this when you want to:</p>
<ul>
<li>Validate the wizard form.</li>
<li>Set the next wizard page.</li>
<li>Override the default submission behavior (for example, to go to a custom page).</li>
</ul></td>
</tr>
</tbody>
</table>

The `callback` function for each hook gets passed the jQuery event object and the `state` object with several properties.
Not all properties are available to all hooks. 

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p><br />
Property</p></th>
<th><p><br />
Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>wizardData</p></td>
<td><p>Contains all data gathered by the wizard pages. The data is contained in the <code>pages</code> property of <code>wizardData</code>. The <code>pages</code> property is a map where the key is the id of the dialog page (e.g. page1Id) and the value is the <code>pageData</code> collected from that page.</p>
<p><strong>pre-render:</strong> YES</p>
<p><strong>post-render:</strong> YES</p>
<p><strong>submit:</strong> YES</p></td>
</tr>
<tr class="even">
<td><p>$container</p></td>
<td><p>The jQuery object wrapping of the page rendered Soy template.</p>
<p><strong>pre-render:</strong> NO</p>
<p><strong>post-render:</strong> YES</p>
<p><strong>submit:</strong> YES</p></td>
</tr>
<tr class="odd">
<td><p>pageData</p></td>
<td><p>Filled with the page form values. May have further data added to it.</p>
<p><strong>pre-render:</strong> NO</p>
<p><strong>post-render:</strong> NO</p>
<p><strong>submit:</strong> YES</p></td>
</tr>
<tr class="even">
<td><p>nextPageId</p></td>
<td><p>Blank when the submit callback is called. You can set this value to specify which Wizard page to show next. If blank, the next page defined in <code>&lt;dialog-wizard&gt;</code> is shown.</p>
<p><strong>pre-render:</strong> NO</p>
<p><strong>post-render:</strong> NO</p>
<p><strong>submit:</strong> YES</p></td>
</tr>
<tr class="odd">
<td><p>finalUrl</p></td>
<td><p>Blank when the function is called. You can set this value on the last page of the Wizard it specifies where the Wizard will go on completion. If blank, the Wizard displays the <strong>editor</strong> page or <strong>View</strong> page based on the <code>create-result</code> configured in your <code>blueprint</code> module. See &quot;Skipping the editor&quot; for details.</p>
<p><strong>pre-render:</strong> NO</p>
<p><strong>post-render:</strong> NO</p>
<p><strong>submit:</strong> YES</p></td>
</tr>
<tr class="even">
<td><p>soyRenderContext</p></td>
<td><p>Contains values for use in the Soy render. The object that is used to pass values inside Soy render <code>context</code>. The context is empty by default.</p>
<p><strong>pre-render:</strong> YES</p>
<p><strong>post-render:</strong> NO</p>
<p><strong>submit:</strong> NO</p></td>
</tr>
</tbody>
</table>

## Validation through the submit hook

To add validation to the form in your wizard, you can use the `submit` hook provided by the API.
In your blueprint JavaScript, add the following hook:

``` javascript
Confluence.Blueprint.setWizard('com.atlassian.confluence.plugins.myplugin:blueprint-item', function(wizard) {
    wizard.on('submit.page1Id', function(e, state) {
        var myName = state.pageData.myName;
        if (myName == 'abc') {
            alert('That is not a real name!');
            return false;
        }
    });
});
```

As you can see from the above code, values from the wizard form are present as properties of the `state.pageData` object.
See the [Write an intermediate blueprint plugin](/server/confluence/write-an-intermediate-blueprint-plugin) tutorial for a complete example.

## Skipping the editor

Some templates might get enough data from the wizard. They can skip the editor and create the page directly.
To do this, update your blueprint module to include a `create-result` attribute:

``` xml
<blueprint key="myplugin-blueprint"
    ...
    create-result="view"/>
```

By default,upon completion of the wizard, the user is taken to the page in the Confluence editor. To specify the view result, add a `create-result` attribute to your `blueprint` module.
When view is set, the wizard creates the page immediately and takes the user to the completed page view.
If you choose to set `create-result` to view, you must pass a title for the new page.
You can pass this value from your `ContextProvider` class or from the wizard's JavaScript.
Using the two previous methods allows you to collect a user-specified title using a form, or you can generate a page title for the user.

### User-specified page titles

The easiest way to pass a page title via the JavaScript wizard is to have a `title` form field in your wizard.
This form value is passed to the server on the submit. The alternative is to add the `title` property to the `wizardData` 
state object manually somewhere in your JavaScript Wizard hooks. An example of a `title` field in the Wizard is the File List Blueprint that is bundled with Confluence.

### Generated page titles

You can generate a page title from back-end logic. For example, you can generate it from a call to a remote site,
or by adding a prefix/suffix to the blueprint name. To achieve this, you can use a `ContextProvider` class
(see [the intermediate tutorial for an example](/server/confluence/write-an-intermediate-blueprint-plugin) for an example).
In your code, add a property with the key `ContentPageTitle` to the context map. For example:

``` java
public Map<String, Object> getContextMap(Map<String, Object> context)
{
    context.put("myName", "Sherlock");
    context.put("ContentPageTitle", yourPageTitleProvider.makeTitle());
    return context;
}
```

An example of the `ContentPageTitle` being set by a `ContextProvider` is the Meeting Notes Blueprint that is bundled with Confluence.  

## Custom JavaScript Wizards and Callbacks

There may be certain behaviors that our API doesn't allow. In that case, you might need more control of the create experience.
To get this control, register a direct callback when the user selects your blueprint and then clicks the **Create** button.
To register a direct callback, add the following code to your blueprint's JavaScript: 

``` javascript
Confluence.Blueprint.setDirectCallback('com.atlassian.confluence.plugins.myplugin:blueprint-item', function(e, state) {
    state.finalUrl = Confluence.getContextPath() + "/pages/createpage.action?spaceKey=" + encodeURIComponent(state.spaceKey);
});
```

When you use this callback, users that create your blueprint are taken to the URL specified previously. In this example,
it is the Confluence editor with a blank page.
The state object which is passed inside direct callback function is similar to default state object mentioned in overview section.
The two important properties for direct callbacks are:

*   Use `state.finalUrl` instead of calling `window.location` or `window.open` directly. 
*   `state.spaceKey` contains the space key of the space that the user selected in the **Create** dialog.

If your blueprint includes a *Let's get started* page, the system displays it before redirecting the user to the `finalURL` page.
It ignores all other wizard pages, even if you have JavaScript calls to `setWizard` and `<dialog-wizard`&gt; is defined in your Blueprint.

Once you send the user to a different browser location, you'll need to wire up the required `xwork` actions in your plugin XML, add a custom action, and so on. This advanced behavior is common with the Confluence plugins. See the [XWork-WebWork module](/server/confluence/xwork-webwork-module#writingan-action) for more information.

{{% note %}}

To provide users with a consistent experience and simplify the development experience for you, we recommend using the Blueprint API *as much as possible*. If you need to use `setDirectCallback` because of a deficiency in the API, *<a href="https://jira.atlassian.com/secure/CreateIssue!default.jspa?selectedProjectId=10470" class="external-link">please let us know</a>*!

{{% /note %}}
