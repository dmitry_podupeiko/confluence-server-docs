---
aliases:
- /server/confluence/custom-user-directories-in-confluence-2031680.html
- /server/confluence/custom-user-directories-in-confluence-2031680.md
category: devguide
confluence_id: 2031680
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031680
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031680
date: '2017-12-08'
guides: guides
legacy_title: Custom User Directories in Confluence
platform: server
product: confluence
subcategory: learning
title: Custom user directories in Confluence
---
# Custom user directories in Confluence

{{% note %}}

Atlassian does not support code-level customisations of our products. We will support your instance for problems other than user-management-related issues if you are using custom user management code.

{{% /note %}}

This documentation applies to Confluence 3.5 and later.

### Introduction

Writing a custom user directory should be the last resort of customers who cannot use any of our supported user management configurations, and cannot use the database or remote API to manually synchronise their users with Confluence.

These instructions are written for customers who are able to write and debug Java code in a web application like Confluence. If you do not have experience doing this, custom user directories are probably not the best approach for your situation.

### Writing a custom user directory

To use a custom user directory in Confluence, you need to write a custom subclass of `com.atlassian.crowd.directory.RemoteDirectory`. You may wish to subclass one of the existing implementations in Crowd: `MicrosoftActiveDirectory`.

If you extend an existing implementation, an instance of your directory will be wrapped in a database-caching layer (`DbCachingRemoteDirectory`) like the default implementations are. If you don't extend an existing implementation, you will not get any caching. This should be considered when evaluating the performance of your custom user directory.

### Installing a custom user directory

To use a custom directory, you need to configure a directory in Confluence through the UI of the appropriate type, then modify the database to set the implementation class to the type you've created.

1.  Install the compiled custom user directory in Confluence's classpath - either as a JAR in `confluence/WEB-INF/lib/` or as a class file in the appropriate directory under `confluence/WEB-INF/classes/`
2.  Using the Confluence web UI, set up one of the built-in directory types with the configuration options you need.
3.  Update the database to set 'impl\_class' (and 'lower\_impl\_class') in the `cwd_directory` table in the database to the fully qualified name (and lowercase fully-qualified name) of your RemoteDirectory implementation class.
4.  Add any additional attributes required by your implementation to `cwd_directory_attribute` table.

Note: code customisations are not supported by Atlassian. If you need help with implementing this, you can try the <a href="http://answers.atlassian.com" class="external-link">forums</a>.

### Related pages

-   <a href="#configuring-user-directories" class="unresolved">Configuring User Directories</a>
-   [Creating a Custom Directory Connector](https://developer.atlassian.com/display/CROWDDEV/Creating+a+Custom+Directory+Connector) (Crowd-specific documentation)
