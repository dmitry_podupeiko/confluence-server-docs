---
aliases:
- /server/confluence/using-standard-page-decorators-2031719.html
- /server/confluence/using-standard-page-decorators-2031719.md
category: devguide
confluence_id: 2031719
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031719
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031719
date: '2017-12-08'
guides: guides
legacy_title: Using Standard Page Decorators
platform: server
product: confluence
subcategory: learning
title: Using Standard Page Decorators
---
# Using Standard Page Decorators

## Purpose of the Standard Page Decorators

Atlassian applications support standard page decorators, allowing your plugin to generate new web pages with consistent decoration by the host application across the Atlassian products.

## Specifying a Decorator

Specify the decorator with an HTML `meta` tag in your `head` element:

``` xml
<html>
  <head>
    <meta name="decorator" content="atl.general"/>
  </head>
```

The following decorators are available.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Decorator</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>atl.admin</code></p></td>
<td><p>For application administration pages.</p>
<p><strong>Version of Atlassian Plugin Framework: 2.1 and later</strong></p></td>
</tr>
<tr class="even">
<td><p><code>atl.general</code></p></td>
<td><p>For the header and footer of general pages outside the administration UI.</p>
<p><strong>Version of Atlassian Plugin Framework: 2.1 and later</strong></p></td>
</tr>
<tr class="odd">
<td><p><code>atl.popup</code></p></td>
<td><p>For content that you want placed in a new browser popup window.</p>
<p><strong>Version of Atlassian Plugin Framework: 2.3 and later</strong></p></td>
</tr>
<tr class="even">
<td><p><code>atl.userprofile</code></p></td>
<td><p>For content on a page in the user profile.<br />
This decorator will generally be accompanied by a web item link or tab. The tab, if applicable, should be specified by the <code>tab</code> meta tag. For example:</p>
<p>In the above example, the value of the <code>content</code> attribute is the ID of the tab. Since plugins can be shared among applications, we recommend that cross-application plugins define their own tab to ensure the same ID will be used everywhere.</p>
<pre><code>&lt;html&gt;
  &lt;head&gt;
    &lt;meta name=&quot;decorator&quot;
          content=&quot;atl.userprofile&quot;/&gt;
    &lt;meta name=&quot;tab&quot;
          content=&quot;foo.bar&quot;&gt;
  &lt;/head&gt;
&lt;/html&gt;</code></pre>
<p>Note: The profile decorator is still experimental. In some applications it may function in the same way as <code>atl.general</code>. Tabs are not yet supported by all Atlassian applications. If not supported, the tab will simply be ignored.</p>
<p><strong>Version of Atlassian Plugin Framework: 2.3 and later</strong></p></td>
</tr>
</tbody>
</table>

## Limitations on Standard Page Decoration in Confluence

In this version of Confluence, the standard page decorators are only available on the following URL patterns:

-   `*.action`
-   `*.vm`
-   `/display/*`
-   `/label/*`

Other URLs do not pass through the Sitemesh decoration filter, so the HTML they return will not be decorated.

##### RELATED TOPICS

[Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)

-   [Creating your Plugin Descriptor](/server/confluence/creating-your-plugin-descriptor)
-   [Accessing Confluence Components from Plugin Modules](/server/confluence/accessing-confluence-components-from-plugin-modules)
-   [Including Javascript and CSS resources](/server/confluence/including-javascript-and-css-resources)
-   [Adding Plugin and Module Resources](/server/confluence/adding-plugin-and-module-resources)
-   [Adding a Configuration UI for your Plugin](/server/confluence/adding-a-configuration-ui-for-your-plugin)
-   [Using Standard Page Decorators](/server/confluence/using-standard-page-decorators)
-   [Making your Plugin Modules State Aware](/server/confluence/making-your-plugin-modules-state-aware)
-   [Form Token Handling](/server/confluence/form-token-handling)
-   [Converting a Plugin to Plugin Framework 2](/server/confluence/converting-a-plugin-to-plugin-framework-2)
-   [Enabling TinyMCE Plugins](/server/confluence/enabling-tinymce-plugins)

