---
aliases:
- /server/confluence/-cql-29952006.html
- /server/confluence/-cql-29952006.md
category: devguide
confluence_id: 29952006
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29952006
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29952006
date: '2017-12-08'
legacy_title: _CQL
platform: server
product: confluence
subcategory: other
title: _CQL
---
# \_CQL

This page is the parent of inclusion pages for [Advanced Searching using CQL](/server/confluence/advanced-searching-using-cql) and it's children.

-   [\_CQLListDateFunctions](/server/confluence/cqllistdatefunctions.snippet)
-   [\_CQLListUserFields](/server/confluence/cqllistuserfields.snippet)
-   [\_CQLListUserFunctions](/server/confluence/cqllistuserfunctions.snippet)
-   [\_CQLSupportedOpEquality](/server/confluence/cqlsupportedopequality.snippet)
-   [\_CQLSupportedOpsRange](/server/confluence/cqlsupportedopsrange.snippet)
-   [\_CQLSupportedOpsSet](/server/confluence/cqlsupportedopsset.snippet)
-   [\_CQLSupportedOpsText](/server/confluence/cqlsupportedopstext.snippet)
