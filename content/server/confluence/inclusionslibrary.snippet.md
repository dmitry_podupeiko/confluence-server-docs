---
aliases:
- /server/confluence/-inclusionslibrary-2031625.html
- /server/confluence/-inclusionslibrary-2031625.md
category: devguide
confluence_id: 2031625
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031625
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031625
date: '2017-12-08'
legacy_title: _InclusionsLibrary
platform: server
product: confluence
subcategory: other
title: _InclusionsLibrary
---
# \_InclusionsLibrary

The children of this page contain information which is **included in other pages**. This is a library of re-usable information chunks.

If you want to change any of these pages, be aware that:

-   Changing page names is problematic -- you will need to change all the {include} and {excerpt-include} macros manually.
-   The content is used in many places -- make sure your change is generic enough to fit the contexts in which the pages are used.

To include an excerpt from a page:

``` javascript
{excerpt-include:_page name|nopanel=true}
```

Note that the page titled '\_page name' must contain the {excerpt} macro, otherwise the {excerpt-include} will not work.

To include the entire contents of a page"

``` javascript
{include:page name|nopanel=true}
```

###### Children of this Page

-   [\_Note about REST APIs](/server/confluence/note-about-rest-apis.snippet)
-   [\_REST URI](/server/confluence/rest-uri.snippet)
-   [Blueprint Development](/server/confluence/blueprint-development-22020139.html)
    -   [Writing a Blueprint - Advanced](/server/confluence/writing-a-blueprint-advanced)
    -   [Writing a Blueprint - Intermediate](/server/confluence/writing-a-blueprint-intermediate)
-   [\_CQL](/server/confluence/cql.snippet)
    -   [\_CQLListDateFunctions](/server/confluence/cqllistdatefunctions.snippet)
    -   [\_CQLListUserFields](/server/confluence/cqllistuserfields.snippet)
    -   [\_CQLListUserFunctions](/server/confluence/cqllistuserfunctions.snippet)
    -   [\_CQLSupportedOpEquality](/server/confluence/cqlsupportedopequality.snippet)
    -   [\_CQLSupportedOpsRange](/server/confluence/cqlsupportedopsrange.snippet)
    -   [\_CQLSupportedOpsSet](/server/confluence/cqlsupportedopsset.snippet)
    -   [\_CQLSupportedOpsText](/server/confluence/cqlsupportedopstext.snippet)
-   [\_CQLListDateFields](/server/confluence/cqllistdatefields.snippet)

Error formatting macro: snippet: java.lang.IndexOutOfBoundsException: Index: 20, Size: 20
