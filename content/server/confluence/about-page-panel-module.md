---
aliases:
- /server/confluence/about-page-panel-module-21463854.html
- /server/confluence/about-page-panel-module-21463854.md
category: reference
confluence_id: 21463854
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=21463854
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=21463854
date: '2017-12-08'
legacy_title: About Page Panel Module
platform: server
product: confluence
subcategory: modules
title: About Page Panel module
---
# About Page Panel module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>Confluence 5.2 and later</p></td>
</tr>
</tbody>
</table>

## Purpose of this module

As a plugin developer, you can use an About Page Panel module to add information to the 'About Confluence' dialog in the Confluence user interface. This dialog appears when people click the help icon ![](/server/confluence/images/helpicon.png) and choose 'About Confluence'. The About Page Panel module adds a section (panel) to that dialog. You can include a panel containing an introduction and/or conclusion section, as well as the GNU LGPL license information for your plugin's dependencies - the libraries your plugin is using.*  
*

*Screenshot: Example of plugin information shown in the 'About Confluence' dialog*

<img src="/server/confluence/images/aboutconfluenceforplugins.png" class="confluence-thumbnail" width="300" />

**Notes:**

-   **License information:** 
    -   The license information is derived from the license file included in your plugin, as referenced by the `licenses`  element in the About Page Panel module. The format of the license file and the XML element are described below.
    -   The 'About Confluence' dialog will show only the LGPL licenses. You may include other licenses in the license file, but they will not appear in the dialog.
-   **Text:**  
    -   The first line displays the name of your plugin, derived from the plugin descriptor `atlassian-plugin.xml` file.
    -   The following text is hard-coded and appears automatically above the list of licenses:

        {{% note %}}

This plugin includes the following libraries covered by the GNU LGPL license:

        {{% /note %}}

    -   All other text is derived from the three elements included in your About Page Panel module: `introduction`, `licenses`, and/or `conclusion`, as described below.

## Configuration

The root element for the About Page Panel module is `about-page-panel`. It must contain at least one of the three elements: `introduction`, `licenses`, and/or `conclusion`.

#### Element: `about-page-panel`

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Attribute</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>name</p></td>
<td><p>A name for this 'about' panel.</p>
<p><strong>Required.</strong></p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The identifier of the plugin module. This key must be unique within your plugin.</p>
<p><strong>Required.</strong></p></td>
</tr>
</tbody>
</table>

#### Element: `introduction`

The `introduction` element defines the introductory section at the top of the 'about' panel, above the optional license information.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Attribute</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>module-key</p></td>
<td><p>The key of a <code>web-resource</code> module that identifies a Soy template defining the introductory text for your 'about' panel.</p>
<p><strong>Required.</strong></p></td>
</tr>
<tr class="even">
<td>function</td>
<td><p>A Soy template, identified by a combination of the Soy namespace and template tag.</p>
<p><strong>Required.</strong></p></td>
</tr>
</tbody>
</table>

#### Element: `licenses`

The license information is defined in a CSV (comma-separated values) file, containing one license per row. See the section about this file [below](#the-license-file).

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Attribute</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>location</td>
<td><p>The path to the CSV file located in your plugin JAR.</p>
<p><strong>Required.</strong></p></td>
</tr>
</tbody>
</table>

#### Element: `conclusion`

The `conclusion` element defines the final section at the bottom of the 'about' panel, below the optional license information.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Attribute</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>module-key</p></td>
<td><p>The key of a <code>web-resource</code> module that identifies a Soy template defining the conclusion text for your 'about' panel.</p>
<p><strong>Required.</strong></p></td>
</tr>
<tr class="even">
<td>function</td>
<td><p>A Soy template, identified by a combination of the Soy namespace and template tag.</p>
<p><strong>Required.</strong></p></td>
</tr>
</tbody>
</table>

## Example plugin descriptor

Here is an example `atlassian-plugin.xml` file containing an 'about' panel - see the `about-page-panel` module and the associated `web-resource`:

``` xml
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="test-plugin" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>

<!-- RELEVANT ITEMS START HERE -->
<!-- ========================= -->

    <web-resource key="about-panel">
        <resource type="soy" name="about-panel.soy" location="about-panel.soy"/>
    </web-resource>

    <about-page-panel name="about-page-section-myplugin" key="about-page-section-myplugin">
        <introduction module-key="${project.groupId}.${project.artifactId}:about-panel" function="about.introduction"/>
        <licenses location="dependencies.csv"/>
        <conclusion module-key="${project.groupId}.${project.artifactId}:about-panel" function="about.conclusion"/>
    </about-page-panel>

<!-- RELEVANT ITEMS END HERE -->
<!-- ========================= -->

 <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="test-plugin"/>
    
    <!-- add our web resources -->
    <web-resource key="test-plugin-resources" name="test-plugin Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        
        <resource type="download" name="test-plugin.css" location="/css/test-plugin.css"/>
        <resource type="download" name="test-plugin.js" location="/js/test-plugin.js"/>
        <resource type="download" name="images/" location="/images"/>

        <context>test-plugin</context>
    </web-resource>
    
    <!-- publish our component -->
    <component key="myPluginComponent" class="com.atlassian.example.MyPluginComponentImpl" public="true">
        <interface>com.atlassian.example.MyPluginComponent</interface>
    </component>
    
    <!-- import from the product container -->
    <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
    
</atlassian-plugin>
```

## Example Soy template

This is a sample Soy template `about-panel.soy`:

``` xml
{namespace about}
/**
 * Displays the introductory section at the top of the panel, before the optional license information
 */
{template .introduction}
<h5>Introduction</h5>
<p>this is the intro. Os says it's a cool plugin so it is.</p>
<p>this is another paragraph!</p>
{/template}

/**
 * Displays the last section at the bottom of the panel, after the optional license information
 */
{template .conclusion}
<h5>Conclusion</h5>
<p><b>this is the conclusion. Os says it's a cool conclusion so it is.</b></p>
<p>this is the last paragraph!</p>
{/template}
```

## The license file

The license information is defined in CSV (comma-separated values) file, where each entry (line) defines a single license. Such a CSV file is sometimes called a BOM (Bill of Materials), defining the dependencies and the license for each dependency.

Each entry in the CSV file has 5 fields:

-   Name of the code library or dependency. For example:` Exampletron Library`
-   A unique name of the dependency, or a version. For Maven artifacts, this can be the GAV (group, artifact ID and version). For example: `com.atlassian:tasty-snax:bundle:1.1`

-   The license name. For example: `GNU Lesser General Public License 2.1. `(The 'About Confluence' box will display only LGPL licenses.)``
-   A URL to a page providing more information, such as the license page or the location of the code library.
-   The scope, set to '`binary`'. If this field is set to any other value, the license will not appear in the 'About Confluence' box.

Sample license file `dependencies.csv`:

``` text
Exampletron Library,com.atlassian:tasty-snax:bundle:1.1,Apache License 2.0,http://example.com,binary
Both,Both,GNU Lesser General Public License 2.1,http://www.example.com/,binary
GAV,GAV,GNU Lesser General Public License 2.1,,binary
URL,,GNU Lesser General Public License 2.1,http://www.example.com/,binary
None,,GNU Lesser General Public License 2.1,,binary
```
