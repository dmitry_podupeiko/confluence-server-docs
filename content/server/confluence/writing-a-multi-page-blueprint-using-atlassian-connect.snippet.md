---
aliases:
- /server/confluence/writing-a-multi-page-blueprint-using-atlassian-connect-38437319.html
- /server/confluence/writing-a-multi-page-blueprint-using-atlassian-connect-38437319.md
category: devguide
confluence_id: 38437319
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=38437319
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=38437319
date: '2017-12-08'
legacy_title: Writing a Multi-page Blueprint using Atlassian Connect
platform: server
product: confluence
subcategory: other
title: Writing a Multi-page Blueprint using Atlassian Connect
---
# Writing a Multi-page Blueprint using Atlassian Connect

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to Confluence OnDemand.</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>This is an intermediate tutorial.</p>
<p>You should have completed the <a href="https://developer.atlassian.com/static/connect/docs/latest/guides/getting-started.html">Atlassian Connect Getting Started</a> tutorial before working through this tutorial.</p></td>
</tr>
<tr class="odd">
<td><p>Time estimate:</p></td>
<td><p>It should take you approximately 1 hour to complete this tutorial.</p></td>
</tr>
</tbody>
</table>

## Overview of the tutorial

In this tutorial you'll create an Atlassian Connect multi-page blueprint, which allows you to create multiple pages with control over the resulting page structure, like the one shown below. 

![](/server/confluence/images/image2016-1-4-14:59:22.png)

## Configuring your development environment 

Before you begin this tutorial, see  `Configuring your development environment`  from [Tutorial: Manage your Confluence instance](https://developer.atlassian.com/static/connect/docs/latest/guides/confluence-gardener-tutorial.html) to find out how set up your development environment.

{{% tip %}}

Instead of cloning the Confluence Gardener repository, please follow the steps below for **step 1 and 2**.

1.  Clone the ` Confluence Multi-page Blueprint Example`

    ``` javascript
    $ git clone git@bitbucket.org:atlassianlabs/confluence-multipage-blueprint-example.git
    ```

2.  Change into the `confluence-multipage-blueprint-example` directory.

    ``` javascript
    $ cd confluence-multipage-blueprint-example
    ```

{{% /tip %}}

Once you've logged in to Confluence as an administrator you can continue with this tutorial.

## Review the Atlassian Connect descriptor

The `atlassian-connect.json` descriptor is located at the project root directory. For more about the descriptor see <https://developer.atlassian.com/static/connect/docs/latest/modules/> .

The descriptor defines 2 modules in  `modules`  section:

1.  Blueprint: Allows the add-on to provide a content creation template.

2.  Webhook: Lets Confluence call the given url when the blueprint creates the page.

It will look like something like this:

``` javascript
{
  "key": "confluence-multipage-blueprint-example",
  "name": "Confluence Multi-page Blueprint Example",
  "description": "Create multiple pages with control over the structure",
  "vendor": {
    "name": "Atlassian Labs",
    "url": "https://www.atlassian.com"
  },
  "baseUrl": "http://localhost:3000",
  "links": {
    "self": "http://localhost:3000/atlassian-connect.json",
    "homepage": "http://localhost:3000/atlassian-connect.json"
  },
  "authentication": {
    "type": "jwt"
  },
  "lifecycle": {
    "installed": "/installed"
  },
  "scopes": [
    "READ",
    "WRITE"
  ],
  "modules": {
    "blueprints": [
      {
        "template": {
          "url": "/blueprint.xml"
        },
        "createResult": "edit",
        "key": "remote-blueprint",
        "name": {
          "value": "Simple Remote Blueprint"
        }
      }
    ],
    "webhooks": [
      {
        "event": "blueprint_page_created",
        "url": "/created"
      }
    ]
  }
}
```

## Add code to route handler

The controllers for serving `atlassian-connect.json` and `blueprint.xml` are defined in `./routes/index.js`   
Now we need to write a controller to handle the blueprint creation callback so we can create a multi-page structure.

1.  Open `./routes/created.js.`This is the controller that handles POST requests to `/created` when a blueprint page is created.
2.  Add some code to check if the callback event was trigged by the blueprint we specified.

    ``` javascript
    /**
     * Import the descriptor at the beginning of the created.js as we want to read the blueprint key from it.
     */
    const descriptor = require('../atlassian-connect.json');
     
    /**
     * Add following code into the body of route handler function.
     */
    // Create a wrapper for request (https://www.npmjs.com/package/request) 
    // which handles authorisation and signing automatically
    var httpClient = addon.httpClient(req);
    var body = req.body;

    // Check if the blueprint creation callback is triggered by current add-on
    if (body.blueprint.indexKey !== descriptor.modules.blueprints[0].key) {
        return;
    }
    ```

3.  Then we can create the child pages using the REST API.

{{% tip %}}

Remember, you can't create multiple pages with same name in the same space. Because your blueprint add-on will be able to be executed multiple times, you may want to add some random text to the title to ensure it is unique, like we've done in the following example.

{{% /tip %}}

    ``` javascript
    // Create content for child pages
    for (var i = 1; i <= 5; i++) {
        // Randomize the title because we can't create pages with same name in one space
        var title = 'Test child page ' + i + ' - ' + String(Math.random()).slice(2);

        // Content of the page
        var contentBody = '<p>Data for child page ' + i + '.</p>';

        var content = {
            'type': 'page',
            'title': title,
            'space': {
                'key': body.page.spaceKey
            },
            'ancestors': [{'id': body.page.id}],
            'body': {
                'storage': {
                    'value': contentBody,
                    'representation': 'storage'
                }
            }
        };

        // Create child pages
        httpClient.post({
            url: '/rest/api/content',
            headers: {
                'X-Atlassian-Token': 'nocheck'
            },
            json: content
        }, function (err, res, body) {
            if (err) {
                console.error(err);
            }
        });
    }
    ```

4.  The final result of created.js should look like this:

    **created.js**  Expand source

    ``` javascript
    const descriptor = require('../atlassian-connect.json');
    module.exports = function (app, addon) {
        app.post('/created', addon.authenticate(), function (req, res) {
            // Create a wrapper for request (https://www.npmjs.com/package/request)
            // which handles authorisation signing automatically
            var httpClient = addon.httpClient(req);
            var body = req.body;

            // Check if the blueprint creation callback is current registered blueprint
            if (body.blueprint.indexKey !== descriptor.modules.blueprints[0].key) {
                return;
            }

            // Create content for child pages
            for (var i = 1; i <= 5; i++) {
                // Randomize the title because we can't create pages with same name in one space
                var title = 'Test child page ' + i + ' - ' + String(Math.random()).slice(2);

                // Content of the page
                var contentBody = '<p>Data for child page ' + i + '.</p>';

                var content = {
                    'type': 'page',
                    'title': title,
                    'space': {
                        'key': body.page.spaceKey
                    },
                    'ancestors': [{'id': body.page.id}],
                    'body': {
                        'storage': {
                            'value': contentBody,
                            'representation': 'storage'
                        }
                    }
                };

                // Create child pages
                httpClient.post({
                    url: '/rest/api/content',
                    headers: {
                        'X-Atlassian-Token': 'nocheck'
                    },
                    json: content
                }, function (err, res, body) {
                    if (err) {
                        console.error(err);
                    }
                });
            }
        });
    };
    ```

## Host and install your add-on

Now that we've created our add-on, we need to host it somewhere, so Confluence can access the blueprint template and webhook callback. In this case we'll use node.js to serve our required files.

Install the npm dependencies and start the server.

``` bash
$ npm install
$ npm start
```

The terminal log will indicate the HTTP server is now serving ./static folder on port 3000. You'll see something like: 

``` bash
Watching atlassian-connect.json for changes
Add-on server running at http://localhost:3000
Registering add-on...
Initialized sqlite3 storage adapter
Add-on not registered; no compatible hosts detected
```

In your browser, navigate to your descriptor file at <a href="http://localhost:3000/atlassian-connect.json" class="uri external-link">localhost:3000/atlassian-connect.json</a>. You should be able to access the Atlassian Connect descriptor.

Register your add-on in Confluence:

1.  Log in to Confluence as an administrator. If you started the application using the Atlassian SDK, the default username/password combination is admin/admin.
2.  Go to <img src="/server/confluence/static/connect/docs/latest/assets/images/cog.png" alt="Settings" class="confluence-external-resource" /> &gt; **Add-ons &gt; Upload Add-on**.
3.  Enter the URL for the hosted location of your plugin descriptor. In our example, the URL is <a href="http://localhost:3000/atlassian-connect.json" class="uri external-link">localhost:3000/atlassian-connect.json</a>. 

## Test your add-on

1.  In your browser, navigate to Confluence <a href="http://localhost:1990/confluence/" class="uri external-link">http://localhost:1990/confluence/</a> 
2.  Choose the **Create from template** button on the header (Hint, it's next to the **Create** button).  
    ![](/server/confluence/images/image2016-1-4-15:14:27.png)
3.  Your new blueprint is listed in the Create dialog. Select your new blueprint and hit **Create**.   
    <img src="/server/confluence/images/image2016-1-4-15:17:38.png" height="250" />
4.  Give your page a title then **Save**.  
    <img src="/server/confluence/images/image2016-1-4-15:18:54.png" height="250" />
5.  Your page is created, along with 5 child pages.   
    ![](/server/confluence/images/image2016-1-4-15:19:53.png)

{{% tip %}}

Congratulations, you're all done!

{{% /tip %}}
