---
title: Draft Preparing for Confluence 6.6 52430438
aliases:
    - /server/confluence/draft-preparing-for-confluence-6.6-52430438.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=52430438
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=52430438
confluence_id: 52430438
platform:
product:
category:
subcategory:
---
# Confluence Server Development : DRAFT - Preparing for Confluence 6.6

 

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 6.6.

We plan to release frequent milestones during the development of Confluence 6.6. Watch this page to find out when a new milestone is available and what's changed.  We won't be publishing formal release notes over at <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> until we release a beta.

**Latest milestone**

 

|              |           |                                                                                                         |
|--------------|-----------|--------------------------------------------------------------------------------------------------------:|
| 06 Nov 2017  | 6.6.0-m10 |  <a href="https://www.atlassian.com/software/confluence/download-eap" class="external-link">Download</a>|

<sup>.</sup>

 

 

**Changes in Confluence 6.6**

**Issues with this milestone?**

Hit the Feedback button on the Confluence EAP header 

or <a href="https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=10470&amp;issuetype=10000&amp;components=43692&amp;summary=Confluence+6.6+EAP+Feedback" class="external-link">raise an issue</a> to tell us about it. 

Looking for updated documentation? Check out the <a href="https://confluence.atlassian.com/display/CONFEAP/Confluence+EAP+Documentation" class="external-link">Confluence EAP</a> space for the latest docs. 

Did you know we've got a new developer community? Head to  <a href="https://community.developer.atlassian.com/" class="uri external-link">https://community.developer.atlassian.com/</a>  to check it out!  We'll be posting in the **announcements**  category if when new EAP releases are available. 

## Milestone 15 - 13 November

We've made some significant improvements to the performance and appearance of user mentions.  The changes are still dark in this milestone, but you can go to `<confluence-url>/users/darkfeatures.action` and add the `smart.mentions` dark feature flag to try it out.

## Milestone 10 - 6 November

This is our first public milestone for Confluence 6.6. Not much has changed yet, but keep an eye on this page, as we'll be releasing a milestone every week in the lead up to Confluence 6.6.  

## Editor changes

We are in the process of upgrading the Confluence editor from TinyMCE 3 to TinyMCE 4, and expect this work to land in or before Confluence 7.0. While this work is in progress we will be taking effort to make these changes in a backwards compatible way, so as not to impact upcoming Confluence 6.x releases. While we are working on it, however, it is possible that your plugin might experience some issues with the changes.

There is one issue that we're currently aware of, relating to the editor context, and we are looking intro ways to resolve it before we release Confluence 6.6.

In preparation for the upgrade to TinyMCE 4 we are splitting the editor context into 3 separate contexts internally. "editor" will remain the official context to contribute resources as a plugin developer. Internally, we will also be conditionally loading either "editor-v3" or "editor-v4" context depending on whether TinyMCE 3 or TinyMCE 4 is active, during the transition. TinyMCE 3 will continue to be the current editor until the announcement of TinyMCE 4. This means that any code loaded currently in "editor" context will continue to work normally in 6.6. However, if a plugin is loading the "editor" context on its own, for a custom editor experience, the resources loaded by "editor-v3" may not be loaded unless the atlassian-plugin.xml resources being used declare explicit dependencies on those resources. We are looking at mitigating this in releases leading to the final editor upgrade release by automatically loading "editor-v3" when "editor" is requested, but we may not be able to automatically account for all cases.

If your plugin is currently requesting the "editor" context, you can verify that your plugin will continue to work normally by also requesting the "editor-v3" context ahead of the "editor" context (the order is important). This is a backwards compatible change, and can be easily added to any plugin that is doing this, without adverse effects to previous Confluence versions. We are planning to remove the need to do this before the final 6.6 release, but if your plugin is doing something we are not expecting, it would be helpful to hear from you in an <a href="https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=10470&amp;issuetype=10000&amp;components=43692&amp;summary=Confluence+6.6+EAP+Feedback" class="external-link">EAP Feedback ticket</a>.

The specific bugs we are aware of have to do specifically with link insert and autocomplete, as well as mentions autocomplete. This is a quick way to check if your plugin editor is affected.

Additionally, there is currently an outstanding bug in Atlassian Web Resource Manager that makes it impossible to run Confluence in unbatched resource mode. We are relying on a dark feature condition to determine which version of the editor to load so that we can offer early testing via our dark feature mechanism once it is ready to be tested. Unfortunately, the condition is ignored in unbatched mode resulting in both versions of the editor being loaded. This can cause any number of problems. Until this bug can be resolved, it is necessary to run Confluence in batched mode. You may pass the following properties to run Confluence in batched mode:

``` javascript
-Dconfluence.context.batching.disable=false
-Dplugin.webresource.batching.off=false
```

We hope to have this resolved before the final 6.6 release.

We'll keep you updated throughout the process, and will provide additional communication about any API changes that might be necessary before the final upgrade release. We will also be providing additional EAP milestones so you can test the editor upgrade with your plugins.
