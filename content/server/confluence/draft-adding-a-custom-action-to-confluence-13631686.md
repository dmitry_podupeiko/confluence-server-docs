---
title: Draft Adding a Custom Action to Confluence 13631686
aliases:
    - /server/confluence/draft-adding-a-custom-action-to-confluence-13631686.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13631686
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13631686
confluence_id: 13631686
platform:
product:
category:
subcategory:
---
# Confluence Server Development : draft - Adding a Custom Action to Confluence

{{% note %}}

DRAFT under construction

This tutorial is a DRAFT and is under construction.

{{% /note %}}

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to Confluence 4.0 and higher.</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>This is an beginner tutorial.</p></td>
</tr>
<tr class="odd">
<td><p>Time estimate:</p></td>
<td><p>It should take you approximately 1 hour to complete this tutorial.</p></td>
</tr>
</tbody>
</table>

## Overview of the tutorial

This tutorial shows you how to add a custom action to Confluence. The final menu is accessible via the **Tools** menu when viewing a Confluence page or blog post. The completed menu appears as follows:

![](/server/confluence/images/tools-post.jpg)

The tutorial helps you build a Confluence plugin consisting of the following components:

-   A Plugin descriptor, to enable the plugin module in Confluence
-   Resources for adding a web item in Confluence and defining what the action does (via Confluence Web Item and XWork-Webwork modules)
-   A Java class that encapsulates the plugin logic. This implements the Confluence `PageAware` interface and injects the `LabelManager` dependency.

All of these components will be contained within a single JAR file. Each component is further discussed in the examples below.

### Prerequisite knowledge

To complete this tutorial, you need to know the following: 

-   The basics of Java development: classes, interfaces, methods, how to use the compiler, and so on.
-   How to create an Atlassian plugin project using the [Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).
-   At least a user-level understanding of Confluence.

### Plugin source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you have finished, you can find the plugin source code on Atlassian Bitbucket. Bitbucket serves a public Git repository containing the tutorial's code. To clone the repository, issue the following command:

    git clone https://bitbucket.org/atlassian_tutorial/REPO_NAME

Alternatively, you can download the source using the **get source** option here: <a href="https://bitbucket.org/atlassian_tutorial/REPO_NAME" class="uri external-link">https://bitbucket.org/atlassian_tutorial/REPO_NAME</a>. 

{{% note %}}

About these Instructions

You can use any supported combination of OS and IDE to construct this plugin. These instructions were written using Eclipse JAVA EE Juno Version on a MacBook Air running Mac OS X. If you are using another combination, you should use the equivalent operations for your specific environment.

{{% /note %}}

## Step 1. Create the Plugin Project

In this step, you'll use the two `atlas-` commands to generate stub code for your plugin and set up the stub code as an Eclipse project. The `atlas-` commands are part of the Atlassian Plugin SDK, and automate much of the work of plugin development for you.

1.  Open a terminal and navigate to your Eclipse workspace directory.  
    Enter the following command to create a plugin skeleton:

        atlas-create-confluence-plugin

    When prompted, enter the following information to identify your plugin:

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.atlassian.plugins.tutorial.confluence</code></p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>AddDraftLabelAction</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code> (the default)</p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.atlassian.plugins.tutorial.confluence.adddraftlabelaction</code></p></td>
    </tr>
    </tbody>
    </table>

2.  Confirm your entries when prompted.
3.  Change to the `AddDraftLabelAction` directory created by the previous step.
4.  Run the following command:

        atlas-mvn eclipse:eclipse

5.  Start Eclipse.
6.  Select **File &gt; Import**.   
    Eclipse starts the **Import** wizard.
7.  Filter for **Existing Projects into Workspace** (or expand the **General** folder tree).
8.  Choose **Next** and enter the root directory of your workspace.   
    Your Atlassian plugin folder should appear under **Projects**.
9.  Select your plugin and choose **Finish**.   
    Eclipse imports your project.

## Step 2. Add Plugin Metadata to the POM

Now you need to edit the Project Object Model (POM) definition file that was created in the step above to add some metadata about your plugin and your company/organisation.

1.  Edit the `pom.xml` file in the root folder of your plugin
2.  Add your company/organisation name and website to the `<organization> `element:

    ``` javascript
    <organization>
        <name>Example Company</name>
        <url>http://www.example.com/</url>
    </organization>
    ```

3.  Update the `<name> `element:

    ``` javascript
    <name>Add 'draft' label</name>
    ```

4.  Update the `<description> `element:

    ``` javascript
    <description>This plugin adds the label 'draft' to the page, via an "Add 'draft' label" action from the Tools menu</description>
    ```

5.  Save the file.

## Step 3. Add two Plugin Modules in the Plugin Descriptor

The plugin descriptor is an XML file that identifies the plugin to Confluence and defines the functionality that the plugin requires.   The plugin descriptor is contained  here `src/main/resources/atlassian-plugin.xml` in your project.  Add a web item and XWork-Webwork plugin module to the descriptor.

### Web Item Plugin Module

The [Web Item Plugin Module](/server/confluence/web-item-plugin-module) allows plugins to define new links in application menus.   This is a common plugin module meaning you could create one in an Atlassian appliction.  The web item defines the location of your custom action in the Confluence **Tools** menu. In this example you add this action to the "primary" section of the **Tools** menu on a Confluence page or blog post.

Because it is a common module, you can use the atlas-create-confluence-plugin-module to generate all the descriptor and dependencies for this module. Do the following to add the web item to your project:

1.  Open a command window and go to the plugin root folder (where the `pom.xml` is located).
2.  Run `atlas-create-confluence-plugin-module`.

3.  Choose the option labeled 13: W`eb Item`.
4.  Supply the following information as prompted:

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p><strong>Enter Plugin Module Name</strong></p></td>
    <td><p><code>add-draft-label-action-web-ui</code></p></td>
    </tr>
    <tr class="even">
    <td><p><strong>Enter Section</strong></p></td>
    <td><p><code>system.content.action/primary</code></p></td>
    </tr>
    <tr class="odd">
    <td><strong>Enter Link URL</strong></td>
    <td><code>/plugins/add-draft-label/add-label.action?pageId=$page.id</code></td>
    </tr>
    </tbody>
    </table>

5.  Choose `N` for **Show Advanced Setup**.
6.  Choose `N` for **Add Another Plugin Module**.
7.  Confirm your choices.
8.  Return to Eclipse and **Refresh** your project.

Take a moment to review what was generated.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>File</strong></td>
<td><strong>Generation</strong></td>
</tr>
<tr class="even">
<td><p><code>pom.xml</code></p></td>
<td><div class="pdl code panel" style="border-width: 1px;">
<div class="panelContent pdl codeContent">
<pre class="sourceCode xml" data-syntaxhighlighter-params="brush: html/xml; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode xml"><div class="sourceLine" id="1" href="#1" data-line-number="1">       <span class="kw">&lt;dependency&gt;</span></div>
<div class="sourceLine" id="2" href="#2" data-line-number="2">            <span class="kw">&lt;groupId&gt;</span>org.mockito<span class="kw">&lt;/groupId&gt;</span></div>
<div class="sourceLine" id="3" href="#3" data-line-number="3">            <span class="kw">&lt;artifactId&gt;</span>mockito-all<span class="kw">&lt;/artifactId&gt;</span></div>
<div class="sourceLine" id="4" href="#4" data-line-number="4">            <span class="kw">&lt;version&gt;</span>1.8.5<span class="kw">&lt;/version&gt;</span></div>
<div class="sourceLine" id="5" href="#5" data-line-number="5">            <span class="kw">&lt;scope&gt;</span>test<span class="kw">&lt;/scope&gt;</span></div>
<div class="sourceLine" id="6" href="#6" data-line-number="6">        <span class="kw">&lt;/dependency&gt;</span></div></code></pre>
</div>
</div></td>
</tr>
<tr class="odd">
<td><p>atlassian-plugin.xml</p></td>
<td><div class="pdl code panel" style="border-width: 1px;">
<div class="panelContent pdl codeContent">
<pre class="sourceCode xml" data-syntaxhighlighter-params="brush: html/xml; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode xml"><div class="sourceLine" id="1" href="#1" data-line-number="1">  <span class="kw">&lt;resource</span><span class="ot"> type=</span><span class="st">&quot;i18n&quot;</span><span class="ot"> name=</span><span class="st">&quot;i18n&quot;</span><span class="ot"> location=</span><span class="st">&quot;com.atlassian.plugins.tutorial.confluence.AddDraftLabelAction&quot;</span><span class="kw">/&gt;</span></div>
<div class="sourceLine" id="2" href="#2" data-line-number="2">  <span class="kw">&lt;web-item</span><span class="ot"> name=</span><span class="st">&quot;add-draft-label-action-web-ui&quot;</span><span class="ot"> i18n-name-key=</span><span class="st">&quot;add--draft--label--action--web--ui.name&quot;</span><span class="ot"> key=</span><span class="st">&quot;add--draft--label--action--web--ui&quot;</span><span class="ot"> section=</span><span class="st">&quot;system.content.action/primary&quot;</span><span class="ot"> weight=</span><span class="st">&quot;1000&quot;</span><span class="kw">&gt;</span></div>
<div class="sourceLine" id="3" href="#3" data-line-number="3">    <span class="kw">&lt;description</span><span class="ot"> key=</span><span class="st">&quot;add--draft--label--action--web--ui.description&quot;</span><span class="kw">&gt;</span>The add-draft-label-action-web-ui Plugin<span class="kw">&lt;/description&gt;</span></div>
<div class="sourceLine" id="4" href="#4" data-line-number="4">    <span class="kw">&lt;label</span><span class="ot"> key=</span><span class="st">&quot;add--draft--label--action--web--ui.label&quot;</span><span class="kw">&gt;&lt;/label&gt;</span></div>
<div class="sourceLine" id="5" href="#5" data-line-number="5">    <span class="kw">&lt;link</span><span class="ot"> linkId=</span><span class="st">&quot;add--draft--label--action--web--ui-link&quot;</span><span class="kw">&gt;</span>/plugins/add-draft-label/add-label.action?pageId=$page.id<span class="kw">&lt;/link&gt;</span></div>
<div class="sourceLine" id="6" href="#6" data-line-number="6">  <span class="kw">&lt;/web-item&gt;</span></div></code></pre>
</div>
</div></td>
</tr>
<tr class="even">
<td><code>AddDraftLabelAction.properties</code></td>
<td>Added to the <code>src/main/resources/com/atlassian/plugins/tutorial/confluence</code> directory.</td>
</tr>
</tbody>
</table>

 

 

OLD

Add the following code to your plugin descriptor, after the `plugin-info` element:

``` javascript
       <web-item name="add-draft-label-action-web-ui" key="add-draft-label-action-web-ui" section="system.content.action/primary" weight="10">
        <description key="item.add-draft-label-action-web-ui.link.desc">Adds the "Add 'draft' label" action into the Tools menu.</description>
        <label key="Add ''draft'' label"/>
        <link linkId="add-draft-label-action">/plugins/add-draft-label/add-label.action?pageId=$page.id</link>
    </web-item>
```

### XWork-Webwork Plugin Module

The [XWork-Webwork Plugin Module](https://developer.atlassian.com/display/CONFDEV/XWork-WebWork+Module) is a Confluence specific module.  It defines:

-   Each segment of the `link` in the Web Item module definition, including the link's "action" component
-   The Java class (or classes) used to encapsulate the logic and functionality that this action performs. Based on the Java code below, you will only be using one Java class -  `AddDraftLabelAction`
-   The "result" of the action - what happens after an object of the Java class has executed successfully.

You cannot use an SDK command to generate this module.  You need to add it manually.  To do this:

1.  Edit the atlassian-plugin.xml file in your project.
2.  Add the following code after the web-item module:

    ``` javascript
        <xwork name="add-draft-label-action-xwork" key="add-draft-label-action-xwork">
            <description key="item.add-draft-label-action-xwork.link.desc">Defines what the "Add 'draft' label" action does.</description>
            <package name="add-draft-label" extends="default" namespace="/plugins/add-draft-label">
                <default-interceptor-ref name="defaultStack"/>
                <action name="add-label" class="com.atlassian.plugins.tutorial.AddDraftLabelAction">
                    <result name="success" type="redirect">${page.urlPath}</result>
                </action>
            </package>
        </xwork>
    ```

3.  Save and close the file.

Here is what your plugin descriptor should look like:

``` javascript
 REDO
   <atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>
    
    <web-item name="add-draft-label-action-web-ui" key="add-draft-label-action-web-ui" section="system.content.action/primary" weight="10">
        <description key="item.add-draft-label-action-web-ui.link.desc">Adds the "Add 'draft' label" action into the Tools menu.</description>
        <label key="Add ''draft'' label"/>
        <link linkId="add-draft-label-action">/plugins/add-draft-label/add-label.action?pageId=$page.id</link>
    </web-item>
    
    <xwork name="add-draft-label-action-xwork" key="add-draft-label-action-xwork">
        <description key="item.add-draft-label-action-xwork.link.desc">Defines what the "Add 'draft' label" action does.</description>
        <package name="add-draft-label" extends="default" namespace="/plugins/add-draft-label">
            <default-interceptor-ref name="defaultStack"/>
            <action name="add-label" class="com.atlassian.plugins.tutorial.AddDraftLabelAction">
                <result name="success" type="redirect">${page.urlPath}</result>
            </action>
        </package>
    </xwork>
</atlassian-plugin>
```

Now you are ready to move on to writing some Java code that makes your plugin execute the action.

## Step 4. Write your Java Class

Next, you use a Java class called `ExampleMacro` to encapsulate what these modules do in Confluence.

### Do a little Refactoring

By default, the generator created a Java class called ExampleMacro.  There are also references to this class in all your files.   Let's refactor the ExampleMacro.java and its references. To refactor means to rewrite existing source code to  improve its readability, reusability or structure without affecting its meaning or behavior.

1.  Search for the string `ExampleMacro` in your project files and change the string to be `AddDraftLabelAction`.
2.  Rename  the `ExampleMacro.java` file to `AddDraftLabelAction.java`. 

 

{{% note %}}

A sophisticated IDE like Eclipse or IDEA have build-in commands for refactoring.

{{% /note %}}

### Update the Source Code 

1.  Edit the `AddDraftLabelAction.java` file.
2.  Add the following import statements:

    ``` javascript
    import com.atlassian.confluence.core.ConfluenceActionSupport;
    import com.atlassian.confluence.labels.LabelManager;
    import com.atlassian.confluence.pages.AbstractPage;
    import com.atlassian.confluence.pages.actions.PageAware;
    ```

    If you are running in an IDE, you can get warnings about unused imports. You can ignore these for now.

3.  Edit your AddDraftLabelAction class so that it implements PageAware and extends

 will also need to:

-   Implement the Confluence `PageAware` interface, and
-   Inject the `LabelManager` dependency.

Some sophisticated IDEs will allow you to automatically implement the following methods associated with the `PageAware` interface:

-   `AbstractPage`
-   `setPage`
-   `isPageRequired`
-   `isLatestVersionRequired`
-   `isViewPermissionRequired`

However, you will need to modify these methods so that the current page is returned. When you have done this, here is what your Java code should look like:

``` javascript
package com.atlassian.plugins.tutorial;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.actions.PageAware;

/**
 * This Confluence action adds the label 'draft' to the page or blog post when a user selects it from the
 * 'Tools' menu in Confluence. Refer to the 'atlassian-plugin.xml' file for details on how this action is
 * implemented in Confluence.
 */
public class AddDraftLabelAction extends ConfluenceActionSupport implements PageAware
{
    private AbstractPage page;
    private LabelManager labelManager;

    /**
     * Implementation of PageAware
     */
    public AbstractPage getPage()
    {
        return page;
    }

    /**
     * Implementation of PageAware
     */
    public void setPage(AbstractPage page)
    {
        this.page = page;
    }

    /**
     * Implementation of PageAware:
     * Returning 'true' ensures that the
     * page is set before the action commences.
     */
    public boolean isPageRequired()
    {
        return true;
    }

    /**
     * Implementation of PageAware:
     * Returning 'true' ensures that the
     * current version of the page is used.
     */
    public boolean isLatestVersionRequired()
    {
        return true;
    }

    /**
     * Implementation of PageAware:
     * Returning 'true' ensures that the user
     * requires page view permissions.
     */
    public boolean isViewPermissionRequired()
    {
        return true;
    }

    /**
     * Dependency-injection of the Confluence LabelManager.
     */
    public void setLabelManager(LabelManager labelManager)
    {
        this.labelManager = labelManager;
    }
}
```

#### Add the `execute` method to your `AddDraftLabelAction` class

Finally, you will need to add the logic to your custom action that will add the label 'draft' to your Confluence page or blog post. To do this, include the following code in your `AddDraftLabelAction` class, after the Dependency Injection of the Confluence Label Manager:

``` javascript
    public String execute()
    {
        // page is already retrieved by Confluence's PageAwareInterceptor
        // labelManager is injected by Confluence -- see setLabelManager() below
        Label label = new Label("draft");
        labelManager.addLabel(page, label);
        return "success";
    }
```

Note that this method must return the string "success", which is interpreted by the `result` element defined in your XWork-Webwork module (above).

{{% note %}}

Ensure the following `import` statement appears near the top of your Java class file (many sophisticated IDEs will do this for you):  
`import com.atlassian.confluence.labels.Label;`

{{% /note %}}

Your `AddDraftLabelAction.java` file should now look as follows:

``` javascript
package com.atlassian.plugins.tutorial;
 
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.actions.PageAware;
 
/**
 * This Confluence action adds the label 'draft' to the page or blog post when a user selects it from the
 * 'Tools' menu in Confluence. Refer to the 'atlassian-plugin.xml' file for details on how this action is
 * implemented in Confluence.
 */
public class AddDraftLabelAction extends ConfluenceActionSupport implements PageAware
{
    private AbstractPage page;
    private LabelManager labelManager;
 
    /**
     * Implementation of PageAware
     */
    public AbstractPage getPage()
    {
        return page;
    }
 
    /**
     * Implementation of PageAware
     */
    public void setPage(AbstractPage page)
    {
        this.page = page;
    }
 
    /**
     * Implementation of PageAware:
     * Returning 'true' ensures that the
     * page is set before the action commences.
     */
    public boolean isPageRequired()
    {
        return true;
    }
 
    /**
     * Implementation of PageAware:
     * Returning 'true' ensures that the
     * current version of the page is used.
     */
    public boolean isLatestVersionRequired()
    {
        return true;
    }
 
    /**
     * Implementation of PageAware:
     * Returning 'true' ensures that the user
     * requires page view permissions.
     */
    public boolean isViewPermissionRequired()
    {
        return true;
    }
 
    /**
     * Dependency-injection of the Confluence LabelManager.
     */
    public void setLabelManager(LabelManager labelManager)
    {
        this.labelManager = labelManager;
    }
 
    public String execute()
    {
        // page is already retrieved by Confluence's PageAwareInterceptor
        // labelManager is injected by Confluence -- see setLabelManager() below
        Label label = new Label("draft");
        labelManager.addLabel(page, label);
        return "success";
    }
}
```

## Step 5. Build, Install and Run the Plugin

Run the following commands to build and install your plugin so that you can test your code:

-   Open a command window and go to the plugin root folder (where the `pom.xml` is located)
-   Start the application by executing the command `atlas-run`

-   When the plugin has finished building, open a new browser window and enter the URL, typically:  
    `http://localhost:1990/confluence`
-   Log in to Confluence with the username of `admin` and password of `admin`
-   When the Confluence dashboard has loaded, check that the plugin has been successfully installed in Confluence. To do this:  
    1.  Visit the Confluence Administration console, by clicking **Browse** &gt; **Confluence Admin** and choose **Plugins** in the left navigation panel
    2.  The plugin appears under the System Plugins
-   Ensure that the 2 plugin modules associated with Add 'draft' label are enabled. If it has installed successfully, your Confluence Administration console should look similar to the screenshot below:   
      
    *Confluence Administration console*  
    <img src="/server/confluence/images/plugin.jpg" width="700" />
-   Visit any Confluence page and choose **Tools** **&gt; Add 'draft' label**   
      
    *The new 'Add 'draft' label' menu item*  
    ![](/server/confluence/images/tools-post.jpg)   
      
    The label 'draft' will be added to your Confluence page:  
      
    *'draft' label added to your page*  
      
    <img src="/server/confluence/images/label.jpg" class="confluence-thumbnail" width="150" />

{{% tip %}}

Congratulations, that's it

Have a chocolate!

{{% /tip %}}

## Step 6. Modifying your source code from here

-   From this point onwards, you can use the command line interface - run atlas-cli in another command window to start the CLI.
-   When you see the message, `Waiting for commands`, you can run `pi` to compile, package and install the updated plugin.
-   The full instructions are in the [SDK guide](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).
