---
aliases:
- /server/confluence/create-a-confluence-plugin-39368872.html
- /server/confluence/create-a-confluence-plugin-39368872.md
category: devguide
confluence_id: 39368872
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368872
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368872
date: '2017-12-08'
legacy_title: Create a Confluence Plugin
platform: server
product: confluence
subcategory: other
title: Create a Confluence Plugin
---
# Create a Confluence Plugin

Enter the command **`atlas-create-confluence-plugin`**.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Windows</td>
<td><div class="pdl code panel" style="border-width: 1px;">
<div class="panelContent pdl codeContent">
<pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:</span>...<span class="op">&gt;</span> atlas<span class="op">-</span>create<span class="op">-</span>confluence<span class="op">-</span>plugin</div></code></pre>
</div>
</div></td>
</tr>
<tr class="even">
<td>Linux/Mac</td>
<td><div class="pdl code panel" style="border-width: 1px;">
<div class="panelContent pdl codeContent">
<pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>...<span class="at">$</span> <span class="at">atlas</span><span class="op">-</span>create<span class="op">-</span>confluence<span class="op">-</span>plugin</div></code></pre>
</div>
</div></td>
</tr>
</tbody>
</table>

The **`atlas-create-confluence-plugin`** tool will prompt you for various parameters:

| Parameter  | Description                                                                                                                                                | Example     |
|------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|
| groupId    | A dot delimited string to create a namespace for plugin so that all of the modules can be uniquely identified even if another plugin users the same names. | com.example |
| artifactId | A string to uniquely identify the plugin with the **groupId** namespace                                                                                    | hello-world |
| version    | An arbitrary string to represent the version.                                                                                                              | 1.0.0       |
| package    | The package to use for the Java code. Note, dashes are not allowed in package names and the "." seperated strings will translate into a directory path.    | com.example |

 

The  tool will ask you to confirm your settings, press `y` to continue.

The tool then creates a **`helloworld`** folder with a basic plugin skeleton.

``` javascript
C:\Users\selberg\atlas-development>atlas-create-confluence-plugin
Executing: "C:\Applications\Atlassian\atlassian-plugin-sdk-6.2.2\apache-maven-3.2.1\bin\mvn.bat" com.atlassian.maven.plugins:maven-confluence-plugin:"6.2.1":create -gs C:\Applications\Atlassian\atlassian-plugin-sdk-6.2.2\apache-maven-3.2.1/conf/settings.xml
Java HotSpot(TM) 64-Bit Server VM warning: ignoring option MaxPermSize=256M; support was removed in 8.0
[INFO] Scanning for projects...
[INFO]
[INFO] Using the builder org.apache.maven.lifecycle.internal.builder.singlethreaded.SingleThreadedBuilder with a thread count of 1
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] ------------------------------------------------------------------------
[INFO]
[INFO] --- maven-confluence-plugin:6.2.1:create (default-cli) @ standalone-pom ---
[INFO] Google Analytics Tracking is enabled to collect AMPS usage statistics.
[INFO] Although no personal information is sent, you may disable tracking by adding <allowGoogleTracking>false</allowGoogleTracking> to the amps plugin configuration in your pom.xml
[INFO] Sending event to Google Analytics: AMPS:confluence - Create Plugin
[INFO] determining latest stable product version...
[INFO] using latest stable product version: 5.9.3
[INFO] determining latest stable data version...
[INFO] using latest stable data version: 5.6.6
Define value for groupId: : com.example
Define value for artifactId: : hello-world
Define value for version:  1.0.0-SNAPSHOT: : 1.0.0
Define value for package:  com.example: : com.example
Confirm properties configuration:
groupId: com.example
artifactId: hello-world
version: 1.0.0
package: com.example
 Y: :
[INFO] Setting property: classpath.resource.loader.class => 'org.codehaus.plexus.velocity.ContextClassLoaderResourceLoader'.
[INFO] Setting property: velocimacro.messages.on => 'false'.
[INFO] Setting property: resource.loader => 'classpath'.
[INFO] Setting property: resource.manager.logwhenfound => 'false'.
[INFO] Generating project in Batch mode
[INFO] Archetype repository missing. Using the one from [com.atlassian.maven.archetypes:confluence-plugin-archetype:RELEASE 
-> https://maven.atlassian.com/public] found in catalog internal
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 39,column 22] : ${atlassian.spring.scanner.version} is not a valid reference.
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 46,column 22] : ${atlassian.spring.scanner.version} is not a valid reference.
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 90,column 47] : ${atlassian.plugin.key} is not a valid reference.
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 142,column 31] : ${project.groupId} is not a valid reference.
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 142,column 50] : ${project.artifactId} is not a valid reference.
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 32.420 s
[INFO] Finished at: 2016-01-06T16:49:25-08:00
[INFO] Final Memory: 21M/328M
[INFO] ------------------------------------------------------------------------
```

{{% note %}}

The warning statements are a known issue and not a problem. See <a href="https://ecosystem.atlassian.net/browse/AMPS-57" class="uri external-link">https://ecosystem.atlassian.net/browse/AMPS-57</a> for more information.

{{% /note %}}
