---
aliases:
- /server/confluence/component-module-old-style-2031752.html
- /server/confluence/component-module-old-style-2031752.md
category: reference
confluence_id: 2031752
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031752
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031752
date: '2017-12-08'
legacy_title: Component Module - Old Style
platform: server
product: confluence
subcategory: modules
title: Component module - old style
---
# Component module - old style

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.2 and later</p></td>
</tr>
<tr class="even">
<td><p>Deprecated:</p></td>
<td><p>Confluence 2.10 - use the new <a href="/server/confluence/component-module">Component Module Type</a> instead</p></td>
</tr>
</tbody>
</table>

{{% note %}}

This is an outdated module type

The Component plugin module described below belongs to the first version of the Atlassian Plugin Framework. A [new Component plugin module](/server/confluence/component-module) is available to OSGi-based plugins using version 2.x of the Atlassian Plugin Framework, supported in **Confluence 2.10 and later**.

{{% /note %}}

## Old-Style Plugin Module Type

We recommend that you use the [new plugin module type](/server/confluence/component-module), rather than the old-style Component described below. Confluence still supports the earlier module type, but the new OSGi-based plugin framework fixes a number of bugs and limitations experienced by the old-style plugin modules.

## Purpose of this Module Type

Component plugin modules enable you to add components to Confluence's internal component system (powered by <a href="http://www.springframework.org" class="external-link">Spring</a>).

Component plugin modules are available in Confluence 1.4 and later.

## Component Plugin Module

Each component module adds a single object to Confluence's component management system.

Other plugins and objects within Confluence can then be autowired with your component. This is very useful for having a single component that is automatically passed to all of your other plugin modules (ie a Manager object).

Here is an example `atlassian-plugin.xml` file containing a single component module:

``` xml
<atlassian-plugin name="Sample Component" key="confluence.extra.component">
    ...
    <component name="Keyed Test Component" 
        key="testComponent" 
        alias="bogusComponent"
        class="com.atlassian.confluence.plugin.descriptor.BogusComponent" />
    ...
</atlassian-plugin>
```

-   the **name** attribute represents how this component will be referred to in the interface.
-   the **key** attribute represents the internal, system name for your component.
-   the **class** attribute represents the class of the component to be created
-   the **alias** attribute represents the alias this component will be stored with. This element is optional, if not specified the module **key** will be used instead.

## Accessing Your Components

Accessing your components is extremely simple.

### Autowired Objects

If your object is being autowired (for example another plugin module or an XWork action), the easiest way to access a component is to add a basic Java setter method.

For example, if you use the above `BogusComponent` module your object would retrieve the component as follows:

``` java
public void setBogusComponent(BogusComponent bogusComponent)
{
    this.bogusComponent = bogusComponent;
}
```

### Non-autowired Objects

If your object is not being autowired, you may need to retrieve the component explicitly. This is done via the `ContainerManager` like so:

``` java
BogusComponent bc = (BogusComponent) ContainerManager.getComponent("bogusComponent");
```

## Notes

Some issues to be aware of when developing a component:

-   One component module *can* depend on another component module but be careful of circular references (ie A requires B, B requires A).
-   The component "namespace" is flat at the moment, so choose a sensible alias for your component.

##### RELATED TOPICS

[Component Module](/server/confluence/component-module)  
[Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)  
<a href="#" class="unresolved">Installing a Plugin</a>
