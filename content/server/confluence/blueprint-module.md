---
aliases:
- /server/confluence/blueprint-module-24084732.html
- /server/confluence/blueprint-module-24084732.md
category: reference
confluence_id: 24084732
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24084732
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24084732
date: '2018-04-27'
legacy_title: Blueprint Module
platform: server
product: confluence
subcategory: modules
title: Blueprint module
---
# Blueprint module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Applicable:</td>
<td>Confluence 5.1 and later.</td>
</tr>
</tbody>
</table>

## Purpose of this module

As a plugin developer, you can use a blueprint module to add to the **Create** dialog in Confluence to help users
speed up the page creation process. Note that you need to define a `web-item` referring to `blueprint` module for
it to appear in the **Create** dialog.

This page only documents the `blueprint` module. For concepts and tutorials, see
[Confluence blueprints](/server/confluence/confluence-blueprints).

## Configuration

The root element for the blueprint module is `blueprint`. 

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Attributes</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>key</p></td>
<td><p>A unique identifier of the blueprint.</p>
<p><em>Required.</em></p></td>
</tr>
<tr class="even">
<td><p>index-key</p></td>
<td><p>Specifies the label added to all pages created from this blueprint. It is used to locate all pages created with this template from an Index page.</p>
<p><em>Required.</em></p></td>
</tr>
<tr class="odd">
<td><p>create-result</p></td>
<td><p>Defines the screen to go to when creating this type of blueprint. A value of <code>create-result="view"</code>
causes Confluence to bypass the page Editor and automatically create the page content. The user lands in the view of the
created page. When <code>create-result="edit"</code>, the user is sent to the editor that is prefilled with the template content.</p>
<p><em>Default:</em> edit.</p></td>
</tr>
<tr class="even">
<td><p>index-template-key</p></td>
<td>Optionally defines an Index page template for your blueprint. It must refer to a <a href="/server/confluence/content-template-module">content-template</a>
defined in your plugin.</td>
</tr>
<tr class="odd">
<td><p>i18n-index-title-key</p></td>
<td><p>The i18n key for the title of Index page.</p>
<p><em>Default:</em> Defaults to the <code>i18n-name-key</code> specified by the <strong>Create</strong> dialog <code>web-item</code> module.</p></td>
</tr>
<tr class="even">
<td><p>disable-index-page</p></td>
<td><p>Disables blueprint index page creation. This property is saved on first blueprint page creation.</p>
<p><em>Default:</em> <code>false</code></p></td>
</tr>
</tbody>
</table>

The `blueprint` element can have the following child elements.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Element</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="/server/confluence/content-template-module">content-template</a></p></td>
<td><p>A blueprint must have at least one content template. Its `ref` attribute should match a key attribute of `content-template module`.</p>
<p><em>Required.</em></p></td>
</tr>
<tr class="even">
<td><p><a href="/server/confluence/dialog-wizard-module">dialog-wizard</a></p></td>
<td>This is an optional element that allows you to easily create a dialog for your blueprint.</td>
</tr>
</tbody>
</table>

## Example

``` xml
<blueprint key="myplugin-blueprint" index-key="myplugin-index">
   <content-template ref="myplugin-template"/>
</blueprint>
```
