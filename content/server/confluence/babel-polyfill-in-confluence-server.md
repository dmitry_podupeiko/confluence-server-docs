---
category: devguide
date: '2019-01-15'
platform: server
product: confluence
subcategory: learning
guides: development resources
title: Babel Polyfill in Confluence Server
---
# Babel Polyfill in Confluence Server

From Confluence 6.7.0, we bundle the babel-polyfill in the superbatch in order to support the new Confluence Mentions. It's the first thing that's loaded in our JavaScript on every page.

## Recommendation

If you're using the Babel polyfill in your apps, we recommend you do the following to prevent the Babel polyfill being loaded multiple times, which can cause problems.

### Check if a Babel Polyfill is already loaded

We recommend you check if a Babel polyfill is already loaded, before loading.

``` JavaScript
if(!window._babelPolyfill){
    _loadPolyfill();
}
```
If your app is installed in Confluence 6.7.0+, it will use the one provided by Confluence, and your app will avoid conflicts between different versions of the polyfill provided by Confluence, or other Apps.
If your app is installed in Confluence 6.6.x or earlier, your polyfill will be used.

Caveat: We recommend Apps use the latest polyfill. If an App is using an older polyfill, there may be some unexpected results compared the polyfill loaded by your app. If this is a problem in your app, you may want to use the `noConflict` version of the polyfill, instead.

### Load the Babel Polyfill as early as possible

You should load the Babel Polyfill as early as possible. For for example, in the `atl.general` context.

``` xml
<web-resource key="polyfill">
    <resource type="download" name="polyfill.js" location="polyfill.js"/>
    <context>atl.general</context>
</web-resource>
```
Doing this will make sure there is no conflict with any other code on the page that may be trying to do its own polyfilling, like in the Code Macro.

It's still a good idea to check if a polyfill is loaded, to prevent multiple apps trying to load different versions of the polyfill.

## All polyfills used in Confluence

See [Confluence Server polyfill matrix](/server/confluence/polyfills-in-confluence-server) for a list of polyfills used in Confluence.
