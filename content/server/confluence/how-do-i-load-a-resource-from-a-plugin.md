---
aliases:
- /server/confluence/2031742.html
- /server/confluence/2031742.md
category: devguide
confluence_id: 2031742
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031742
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031742
date: '2017-12-08'
legacy_title: How do I load a resource from a plugin?
platform: server
product: confluence
subcategory: faq
title: How do I load a resource from a plugin?
---
# How do I load a resource from a plugin?

The recommended way to get resources from the classpath in Confluence is:

``` java
InputStream in = com.atlassian.core.util.ClassLoaderUtils.getResourceAsStream(filename, this);
```

ClassLoaderUtils tries a couple of different classloaders, something we've occasionally found necessary in some application servers.
