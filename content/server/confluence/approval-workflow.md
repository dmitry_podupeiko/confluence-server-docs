---
aliases:
- /server/confluence/approval-workflow-2031635.html
- /server/confluence/approval-workflow-2031635.md
category: reference
confluence_id: 2031635
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031635
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031635
date: '2018-04-26'
legacy_title: Approval Workflow
platform: server
product: confluence
subcategory: modules
title: Approval Workflow
---
# Approval Workflow

This page describes an approval workflow.

*   Users may be members of:
    * An *author* group, which may edit pages.
    * An *approver* group, which may approve edited pages.
    * Both groups (in this case they can't approve their own changes).
    * No group (in this case they are just readers of the content).

<!-- -->

*   When an *author* edits a page, the page goes into an *editing in progress* state.

<!-- -->

*   When viewing an *editing in progress* page, an *author* is presented with an option to submit the page for review.
This puts the page into the *waiting for approval* state.

<!-- -->

*   Members of the *approver* group have access to a page in Confluence that automatically lists the pages in
*waiting for approval* state.

<!-- -->

*   When visiting a *waiting for approval* page, an *approver* is presented with options to accept or reject the changes.
If they accept the changes, the page goes to the *accepted* state, where pages spend most of their life;
otherwise it goes to the *rejected* state.

<!-- -->

*   Members of the *author* group have access to a page in Confluence where they can see all the pages that they edited,
that have been rejected, or are waiting for approval. They don't see pages other authors have edited.

<!-- -->

*   When visiting a page in the *rejected* or *waiting for approval* state, an *author* has the option to withdraw the change,
which moves the page to the *accepted* state, and rolls back to the most recent approved version.

<!-- -->

*   When an *author* edits a page in the *rejected* state, it moves to the *editing in progress* state.

You can do all of described with the <a href="/server/confluence/workflow-plugin-prototype/" class="unresolved">Workflow plugin prototype</a>.

We also might want to show customers the most recently approved version of a page, not the one currently under review.
Without core Confluence changes, the best we can do is to show users a banner which says "This content is being reviewed.
The most recent approved content is *here*".
