---
title: Development Resources 2031733
aliases:
    - /server/confluence/development-resources-2031733.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031733
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031733
confluence_id: 2031733
platform:
product:
category:
subcategory:
---
# Confluence Server Development : Development Resources

-   [Confluence Build Information](/server/confluence/confluence-build-information)
-   [Building Confluence From Source Code](/server/confluence/building-confluence-from-source-code)
-   [Confluence Architecture](/server/confluence/confluence-architecture)
-   [Confluence Developer FAQ](/server/confluence/confluence-developer-faq)
-   [Preparing for Confluence 4.0](/server/confluence/preparing-for-confluence-4-0)
-   [Preparing for Confluence 4.3](/server/confluence/preparing-for-confluence-4-3)
-   [Preparing for Confluence 5.0](/server/confluence/preparing-for-confluence-5-0)
-   [Preparing for Confluence 5.1](/server/confluence/preparing-for-confluence-5-1)
-   [Preparing for Confluence 5.2](/server/confluence/preparing-for-confluence-5-2)
-   [Preparing for Confluence 5.3](/server/confluence/preparing-for-confluence-5-3)
-   [Preparing for Confluence 5.4](/server/confluence/preparing-for-confluence-5-4)
-   [Preparing for Confluence 5.5](/server/confluence/preparing-for-confluence-5-5)
-   [Preparing for Confluence 5.6](/server/confluence/preparing-for-confluence-5-6)
-   [Preparing for Confluence 5.7](/server/confluence/preparing-for-confluence-5-7)
-   [Preparing for Confluence 5.8](/server/confluence/preparing-for-confluence-5-8)
-   [Preparing for Confluence 5.9](/server/confluence/preparing-for-confluence-5-9)
-   [Preparing for Confluence 5.10](/server/confluence/preparing-for-confluence-5-10)
-   [Preparing for Confluence 6.0](/server/confluence/preparing-for-confluence-6-0)
-   [Preparing for Confluence 6.1](/server/confluence/preparing-for-confluence-6-1)
-   [Preparing for Confluence 6.2](/server/confluence/preparing-for-confluence-6-2)
-   [Preparing for Confluence 6.3](/server/confluence/preparing-for-confluence-6-3)
-   [Preparing for Confluence 6.4](/server/confluence/preparing-for-confluence-6-4)
-   [Preparing for Confluence 6.5](/server/confluence/preparing-for-confluence-6-5)
-   [Preparing for Confluence 6.6](/server/confluence/preparing-for-confluence-6-6)
-   [Preparing for the Confluence Server mobile app](/server/confluence/preparing-for-the-confluence-server-mobile-app)
-   [Preparing for Confluence 6.7](/server/confluence/preparing-for-confluence-6-7)

