---
aliases:
- /server/confluence/2031798.html
- /server/confluence/2031798.md
category: devguide
confluence_id: 2031798
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031798
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031798
date: '2017-12-08'
legacy_title: How do I get the information about Confluence such as version number,
  build number, build date?
platform: server
product: confluence
subcategory: faq
title: How do I get the information about Confluence such as version number, build
  number, build date?
---
# How do I get the information about Confluence such as version number, build number, build date?

Information about Confluence, such as the ***version number***, ***build number*** and ***build date***, can be retrieved from the <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/confluence/util/GeneralUtil.html" class="external-link">GeneralUtil</a> object.

You can use GeneralUtils public accessors to retrieve public static variables:

-   versionNumber
-   buildDate
-   buildNumber

#### In Java

``` java
String versionNumber = GeneralUtil.getVersionNumber();
```

``` java
String buildNumber = GeneralUtil.getBuildNumber();
```

``` java
String buildDate = GeneralUtil.getBuildDateString(); 
```

or

``` java
Date buildDate = GeneralUtil.getBuildDate();
```

#### In Velocity

``` javascript
$generalUtil.versionNumber 
$generalUtil.buildNumber 
$generalUtil.buildDateString
```

For instance, part of the Confluence footer is generated in the footer.vm file:

``` javascript
(Version: $generalUtil.versionNumber Build:#$generalUtil.buildNumber $generalUtil.buildDateString)
```

#### In Wiki markup

[User macros](/server/confluence/confluence-user-macro-guide) can include the Velocity markup given above. For example, create a user macro called 'version' with no body and the contents:

``` javascript
$generalUtil.versionNumber
```

You can use this user macro in a page like this:

``` bash
Congratulation, you're running Confluence version {version}!
```
