---
aliases:
- /server/confluence/2031786.html
- /server/confluence/2031786.md
category: devguide
confluence_id: 2031786
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031786
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031786
date: '2017-12-08'
legacy_title: How do I autowire a component?
platform: server
product: confluence
subcategory: faq
title: How do I autowire a component?
---
# How do I autowire a component?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I autowire a component?

Most of the time, you don't have to. All plugins will have their 'primary' objects (The macro in a macro plugin, the XWork actions in an XWork plugin, the RPC handler in an RPC plugin and so on...) autowired.

If you want to write an arbitrary object that is autowired, but that is not any particular plugin type itself, write a <a href="/server/framework/atlassian-sdk/component-plugin-module/" class="createlink">Component Plugin Module</a>. The added advantage of this is that Confluence will then autowire *other* plugins with the component you have just written.

If, however, you find you need to autowire an arbitrary object with Spring components, use `bucket.util.ContainerManager`

``` java
bucket.container.ContainerManager.autowireComponent(myObject);
```

Where `myObject` is the object instance that you wish to be autowired.
