---
aliases:
- /server/confluence/community-developed-docs-40003553.html
- /server/confluence/community-developed-docs-40003553.md
category: devguide
confluence_id: 40003553
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40003553
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40003553
date: '2017-12-08'
legacy_title: Community developed docs
platform: server
product: confluence
subcategory: other
title: Community developed docs
---
# Community developed docs

These docs were created by the community and need to be validated before they're made available.
