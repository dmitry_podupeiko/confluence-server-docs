---
aliases:
- /server/confluence/creating-a-cross-platform-admin-configuration-form-8946018.html
- /server/confluence/creating-a-cross-platform-admin-configuration-form-8946018.md
category: devguide
confluence_id: 8946018
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8946018
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8946018
date: '2017-12-08'
legacy_title: Creating a Cross-Platform Admin Configuration Form
platform: server
product: confluence
subcategory: other
title: Creating a Cross-Platform Admin Configuration Form
---
# Creating a Cross-Platform Admin Configuration Form

{{% note %}}

Redirection Notice

This page will redirect to <https://developer.atlassian.com/x/hwIr> in about 2 seconds.

{{% /note %}}
