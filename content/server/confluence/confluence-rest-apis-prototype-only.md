---
aliases:
- /server/confluence/confluence-rest-apis-prototype-only-2031875.html
- /server/confluence/confluence-rest-apis-prototype-only-2031875.md
category: devguide
confluence_id: 2031875
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031875
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031875
date: '2017-12-08'
legacy_title: Confluence REST APIs - Prototype Only
platform: server
product: confluence
subcategory: updates
title: Confluence REST APIs - prototype only
---
# Confluence REST APIs - prototype only

{{% note %}}

This prototype REST API is **deprecated since Confluence 5.5**.  
Confluence has a new [REST API](/server/confluence/confluence-server-rest-api) that is progressively replacing our existing APIs, including this prototype REST API. We recommend plugin developers use the new REST APIs where possible.

{{% /note %}}

The REST APIs are for developers who want to integrate Confluence into their application and for administrators who want to script interactions with the Confluence server.

## Introduction to Confluence's REST APIs

Confluence's REST APIs provide access to resources (data entities) via URI paths. To use a REST API, your application will make an HTTP request and parse the response. By default, the response format is XML. If you wish, you can request JSON instead of XML. Your methods will be the standard HTTP methods like GET, PUT, POST and DELETE.

Because the REST API is based on open standards, you can use any web development language to access the API.

A typical use case would be to search Confluence for a page or pages that match a given search term, then retrieve the content of the page(s).

Most of the Confluence REST API is read only. Confluence's REST APIs allow you to retrieve the following information:

-   A list of spaces, including high-level information about each space.
-   Detailed information about a space.
-   Search results using the Confluence search with various parameters.
-   The content of pages, blogs and comments.
-   A list of attachments for a given page or blog post.
-   A given attachment, specified by attachment ID.
-   Information about the user's session.
-   Translated UI text (message bundles) for a given internationalisation key.

The following resources are available for updating content:

-   In Confluence 5.1 and later: Add, delete, validate and list **labels**. (Use 'validate' instead of 'add' when adding labels to a content item that has not yet been created.)

## Getting Started

If you would like to know more about REST in general, start with the Dare Obasanjo's blog post, <a href="http://www.25hoursaday.com/weblog/2008/08/17/ExplainingRESTToDamienKatz.aspx" class="external-link">Explaining REST to Damien Katz</a>.

Then jump right in and try our REST resources:

-   Read our [guide to using the REST APIs](/server/confluence/using-the-rest-apis-prototype-only).
-   Find the REST resources you need in our <a href="http://docs.atlassian.com/atlassian-confluence/REST/latest/" class="external-link">REST resources reference guide</a>.

## Advanced Topics

Below are some links to in-depth information on developing REST APIs and plugins:

-   **Developing your own REST APIs for Confluence:** Confluence uses the Atlassian REST API to implement the Confluence APIs. The REST plugin is bundled with Confluence. You can add your own REST APIs to Confluence by [creating a Confluence plugin](/server/confluence/confluence-plugin-guide) that includes the REST plugin module.
-   **Understanding the principles behind the Atlassian REST API design:** You may be interested in the [Atlassian REST API Design Guidelines](https://developer.atlassian.com/display/DOCS/Atlassian+REST+API+Design+Guidelines+version+1)followed by the Atlassian developers who are designing REST APIs for Atlassian applications, including the Confluence REST APIs.

## Browsing and Testing your REST APIs from the Application UI

You can browse and test your remote API using the REST Application Browser (RAB). The RAB is a page in the application's administrative console. For information on using the RAB, see the [documentation for the REST API Browser](https://developer.atlassian.com/x/9oCI).

##### RELATED TOPICS

[Confluence Server developer documentation](https://developer.atlassian.com/display/CONFDEV)
