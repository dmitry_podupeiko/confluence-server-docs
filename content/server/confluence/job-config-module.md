---
aliases:
- /server/confluence/job-config-module-40000535.html
- /server/confluence/job-config-module-40000535.md
category: reference
confluence_id: 40000535
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40000535
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40000535
date: '2017-12-08'
legacy_title: Job Config Module
platform: server
product: confluence
subcategory: modules
title: Job Config module
---
# Job Config module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 5.10 and later</p></td>
</tr>
</tbody>
</table>

Job Config module (<a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/plugins/scheduler/spi/descriptor/JobConfigModuleDescriptor.html" class="external-link">JobConfigModuleDescriptor</a>)
defines an `atlassian-scheduler` `JobConfig` within a plugin. Atlassian Scheduler is used to create scheduled tasks in Atlassian applications.

*   For more information about plugins in general, read [Confluence plugin guide](/server/confluence/confluence-plugin-guide).
*   For an introduction to writing your own plugins, read [Writing Confluence plugins](/server/confluence/writing-confluence-plugins)

## JobConfig module

This module defines an `atlassian-scheduler` `JobConfig` within a plugin. Here's an example:

``` xml
<job-config name="My job" key="myJobId">
    <job key="myJobRunner" perClusterJob="true" clusteredOnly="true"/>
    <schedule cron-expression="0 * * * * ?" jitterSecs="10"/>
    <managed editable="true" keepingHistory="true" canRunAdhoc="true" canDisable="true"/>
</job-config>
```

If you are using <a href="https://bitbucket.org/atlassian/atlassian-spring-scanner" class="external-link">Atlassian Spring Scanner</a>,
ensure your job runner is defined as a Spring bean with a @org.springframework.stereotype.Component or @javax.inject.Named annotation and
implements `JobRunner` interface:

``` java
@Named
public class MyJobRunner implements JobRunner {
    // Your code here
}
```

## Configuration

### `job-config`

*   The `name` represents how this job config will be referred to in the Confluence interface.
*   The `key` represents the internal system name for the job.

### `job`

*   The `key` points to your `JobRunner`, make sure that it has the same value as your class name, except the first letter should be lowercase.
Another option is to have key value match with value of `@Named` annotation.
*   When `perClusterJob` is `true`, this job will run only once in a cluster rather than on every node.
*   When `clusteredOnly` is `true`, this job won't be scheduled in non-clustered environment. This is optional.

### `schedule`

*   Either a `cron-expression` or `repeat-interval` must be present.
*   `repeat-interval`allows you to use an interval instead of a `cron-expression`.
    *   This example repeats every hour (3600000 milliseconds) and only 5 times:

        ``` xml
        <schedule repeat-interval="3600000" repeat-count="5"/>
        ```

    *   `repeat-count` is optional when `repeat-interval` is used.
    *   To repeat indefinitely, set `repeat-count` to `-1` or omit it.
    *   To run the job only once set `repeat-count` to `0`.

*   `jitterSecs` is optional and introduces some random jitter.  This is only for Confluence Cloud.
It prevents many thousands of servers from attempting to do similar things all at once. By providing a jitter value less
than the size of the schedule frequency, we spread the load caused by a process over time.

### `managed`

*   `managed` is optional. If this is omitted the job won't appear in the <a href="https://confluence.atlassian.com/display/DOC/Scheduled+Jobs" class="external-link">Scheduled Jobs</a> administration page in Confluence.
*   If any of the following attributes are omitted, their values are assumed to be `false`: 
    *   When set to `true`, `editable` allows the job schedule to be edited. This is forced to `false` if schedule type is interval.
    *   When set to `true`, `keepingHistory` means the job's history persists and survives server restarts. If set to `false`, the job history is only stored in memory and will be lost on the next server restart.
    *   When set to `true`, `canRunAdhoc` allows the job to be executed manually on the Scheduled Jobs administration screen.
    *   When set to `true`, `canDisable` allows the job to be enabled or disabled on the Scheduled Jobs administration screen.

## Migration from [&lt;job&gt;](/server/confluence/job-module) and [&lt;trigger&gt;](/server/confluence/trigger-module)

*   There is no direct replacement for `<job>` module. Use `<job-config>` module with Atlassian Spring Scanner as described previously.
*   To make the migration easier, the XML of `<job-config>` is similar to `<trigger>`.
*   For managed jobs, the `key` of `<job-config>` is used to look up the i18n job name (`scheduledjob.desc.<key>`), and display it in the Confluence admin UI. Previously, it was the `key` of `<job>.`
