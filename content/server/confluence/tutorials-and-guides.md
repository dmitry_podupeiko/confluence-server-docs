---
platform: server
product: confluence
subcategory: learning
category: devguide
title: "Tutorials and guides"
layout: tutorials-and-guides
date: "2019-04-15"
---

# Tutorials and guides

The tutorials and guides listed on this page are for Confluence Server. 
If you're looking for guides for Confluence Cloud, you can [find them here](/cloud/confluence/tutorials-and-guides/).

## Beginner tutorials

-   [Confluence blueprints](/server/confluence/confluence-blueprints/)
-   [Write a simple Confluence space blueprint](/server/confluence/write-a-simple-confluence-space-blueprint/)
-   [Adding Confluence keyboard shortcuts](/server/confluence/adding-confluence-keyboard-shortcuts/)
-   [Create a Confluence 'Hello World' macro](/server/framework/atlassian-sdk/create-a-confluence-hello-world-macro/)
-   [Write a simple Confluence blueprint plugin](/server/confluence/write-a-simple-confluence-blueprint-plugin/)
-   [Writing a Confluence theme](/server/confluence/writing-a-confluence-theme/)
-   [Extending autoconvert](/server/confluence/extending-autoconvert/)

## Intermediate tutorials

-   [Extending the highlight actions panel](/server/confluence/extending-the-highlight-actions-panel/)
-   [Adding new Confluence to Hipchat notifications](/server/confluence/adding-new-confluence-to-hipchat-notifications/)
-   [Adding items to the info banner](/server/confluence/adding-items-to-the-info-banner/)
-   [Extending the Confluence insert link dialog](/server/confluence/extending-the-confluence-insert-link-dialog/)
-   [Extending the image properties dialog](/server/confluence/extending-the-image-properties-dialog/)
-   [Write an intermediate blueprint plugin](/server/confluence/write-an-intermediate-blueprint-plugin/)
-   [Writing a macro using JSON](/server/confluence/writing-a-macro-using-json/)
-   [Posting notifications in Confluence](/server/confluence/posting-notifications-in-confluence/)
-   [Searching using the v2 search API](/server/confluence/searching-using-the-v2-search-api/)
-   [Adding a custom action to Confluence](/server/confluence/adding-a-custom-action-to-confluence/)
-   [Unit testing plugins](/server/confluence/unit-testing-plugins/)
-   [Writing a blueprint - intermediate](/server/confluence/writing-a-blueprint-intermediate/)

## Advanced tutorials

-   [Write an advanced blueprint plugin](/server/confluence/write-an-advanced-blueprint-plugin/)
-   [Extending the v2 search API](/server/confluence/extending-the-v2-search-api/)
-   [Adding a field to CQL](/server/confluence/adding-a-field-to-cql/)
-   [Defining a pluggable service in a Confluence plugin](/server/confluence/defining-a-pluggable-service-in-a-confluence-plugin/)
-   [Creating an admin Configuration form](/server/framework/atlassian-sdk/creating-an-admin-configuration-form/)
-   [Writing a blueprint - advanced](/server/confluence/writing-a-blueprint-advanced/)

## Other tutorials

Looking for other products and for the cross-product frameworks? Start with the 
[Atlassian SDK tutorials](/server/framework/atlassian-sdk/tutorials/) page.

### Legacy tutorials

These tutorials apply to Confluence versions that have reached end of life. 

-   [Adding an option to the editor insert menu](/server/confluence/adding-an-option-to-the-editor-insert-menu/)
-   [Preventing XSS issues with macros in Confluence 4.0](/server/confluence/preventing-xss-issues-with-macros-in-confluence-4-0/)
-   [Writing Soy templates in your plugin](/server/confluence/writing-soy-templates-in-your-plugin/)
-   [Upgrading and migrating an existing Confluence macro to 4.0](/server/confluence/upgrading-and-migrating-an-existing-confluence-macro-to-4-0)
-   [Providing an image as a macro placeholder in the editor](/server/confluence/providing-an-image-as-a-macro-placeholder-in-the-editor/)
-   [Extending the macro property panel](/server/confluence/extending-the-macro-property-panel/)
-   [Sending emails in a plugin](/server/confluence/sending-emails-in-a-plugin/)
-   [Writing macros for pre-4.0 Versions of Confluence](/server/confluence/writing-macros-for-pre-4-0-versions-of-confluence/)
-   [Creating a template bundle](/server/confluence/creating-a-template-bundle/)
-   [Adding menu items to Confluence](/server/confluence/adding-menu-items-to-confluence/)

## Guides

- [Accessing Confluence components from plugin modules](/server/confluence/accessing-confluence-components-from-plugin-modules/)
- [Adding a Configuration UI for your plugin](/server/confluence/adding-a-configuration-ui-for-your-plugin/)
- [Adding feature discovery to your plugin](/server/confluence/adding-feature-discovery-to-your-plugin/)
- [Adding plugin and module resources](/server/confluence/adding-plugin-and-module-resources/)
- [Advanced HTML encoding](/server/confluence/advanced-html-encoding/)
- [Atlassian cache 2 overview](/server/confluence/atlassian-cache-2-overview/)
- [Bandana caching](/server/confluence/bandana-caching/)
- [Basic introduction to Velocity](/server/confluence/basic-introduction-to-velocity/)
- [Confluence architecture](/server/confluence/confluence-architecture/)
- [Confluence blueprints](/server/confluence/confluence-blueprints/)
- [Confluence bootstrap process](/server/confluence/confluence-bootstrap-process/)
- [Confluence caching architecture](/server/confluence/confluence-caching-architecture/)
- [Confluence internals](/server/confluence/confluence-internals/)
- [Confluence internals history](/server/confluence/confluence-internals-history/)
- [Confluence journal service](/server/confluence/confluence-journal-service/)
- [Confluence macro manager](/server/confluence/confluence-macro-manager/)
- [Confluence objects accessible from Velocity](/server/confluence/confluence-objects-accessible-from-velocity/)
- [Confluence permissions architecture](/server/confluence/confluence-permissions-architecture/)
- [Confluence services](/server/confluence/confluence-services/)
- [Confluence test data for plugin developers](/server/confluence/confluence-test-data-for-plugin-developers/)
- [Confluence UI architecture](/server/confluence/confluence-ui-architecture/)
- [Confluence UI guidelines](/server/confluence/confluence-ui-guidelines/)
- [Confluence user macro guide](/server/confluence/confluence-user-macro-guide/)
- [Converting a Plugin to Plugin Framework 2](/server/confluence/converting-a-plugin-to-plugin-framework-2/)
- [Creating a Wikipedia macro](/server/confluence/creating-a-wikipedia-macro/)
- [Creating your plugin descriptor](/server/confluence/creating-your-plugin-descriptor/)
- [Custom user directories in Confluence](/server/confluence/custom-user-directories-in-confluence/)
- [Date formatting with time zones](/server/confluence/date-formatting-with-time-zones/)
- [Deprecation guidelines](/server/confluence/deprecation-guidelines/)
- [DTDs and schemas](/server/confluence/dtds-and-schemas/)
- [Dynamically migrate macro parameters](/server/confluence/dynamically-migrate-macro-parameters/)
- [Enabling TinyMCE plugins](/server/confluence/enabling-tinymce-plugins/)
- [Exception handling guidelines](/server/confluence/exception-handling-guidelines/)
- [Form Token handling](/server/confluence/form-token-handling/)
- [Generating JSON output in Confluence with Jsonator](/server/confluence/generating-json-output-in-confluence-with-jsonator/)
- [Hibernate session and transaction management for bulk operations](/server/confluence/hibernate-session-and-transaction-management-for-bulk-operations/)
- [Hibernate sessions and transaction management guidelines](/server/confluence/hibernate-sessions-and-transaction-management-guidelines/)
- [High level architecture overview](/server/confluence/high-level-architecture-overview/)
- [How to fix broken extractors](/server/confluence/how-to-fix-broken-extractors/)
-[HTML to markup conversion for the rich text editor](/server/confluence/html-to-markup-conversion-for-the-rich-text-editor/)
- [HTTP authentication with Seraph](/server/confluence/http-authentication-with-seraph/)
- [I18N architecture](/server/confluence/i18n-architecture/)
- [Icons](/server/confluence/icons/)
- [Including JavaScript and CSS resources](/server/confluence/including-javascript-and-css-resources/)
- [Instruction text in blueprints](/server/confluence/instruction-text-in-blueprints/)
- [Internationalising Confluence plugins](/server/confluence/internationalising-confluence-plugins/)
- [Javadoc standards](/server/confluence/javadoc-standards/)
- [JavaScript API for blueprint wizards](/server/confluence/javascript-api-for-blueprint-wizards/)
- [JavaScript components](/server/confluence/javascript-components/)
- [Logging guidelines](/server/confluence/logging-guidelines/)
- [Making your plugin modules state aware](/server/confluence/making-your-plugin-modules-state-aware/)
- [Packages available to OSGi plugins](/server/confluence/packages-available-to-osgi-plugins/)
- [Page tree API documentation](/server/confluence/page-tree-api-documentation/)
- [Password hash algorithm](/server/confluence/password-hash-algorithm/)
- [Persistence in Confluence](/server/confluence/persistence-in-confluence/)
- [Plugin cookbook](/server/confluence/plugin-cookbook/)
- [Plugin resources](/server/confluence/plugin-resources/)
- [Rendering Velocity templates in a macro](/server/confluence/rendering-velocity-templates-in-a-macro/)
- [Space blueprints](/server/confluence/space-blueprints/)
- [Spring IoC in Confluence](/server/confluence/spring-ioc-in-confluence/)
- [Spring usage guidelines](/server/confluence/spring-usage-guidelines/)
- [Templating in JavaScript with Soy](/server/confluence/templating-in-javascript-with-soy/)
- [Using standard page decorators](/server/confluence/using-standard-page-decorators/)
- [Velocity template overview](/server/confluence/velocity-template-overview/)
- [Writing Confluence plugins](/server/confluence/writing-confluence-plugins/)