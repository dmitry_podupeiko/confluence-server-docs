---
aliases:
- /server/confluence/content-properties-in-the-rest-api-32243810.html
- /server/confluence/content-properties-in-the-rest-api-32243810.md
category: reference
confluence_id: 32243810
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=32243810
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=32243810
date: '2018-04-26'
legacy_title: Content Properties in the REST API
platform: server
product: confluence
subcategory: api
title: Content properties in the REST API
---
# Content properties in the REST API

## What are content properties?

Content properties are a form of key-value storage, where the key is associated with a piece of Confluence Content.
Content properties are one of the forms of persistence available to you as an add-on developer. Content properties allow
to store up to 32 KB of JSON with each piece of content (for example, a page, blog post, or attachment), so,
you do not have to use your own data store.
If you need to store metadata, for example, about a piece (or pieces) of content, content property is a great way to do it.

Content properties are accessible both by Java and REST APIs.

## Query content properties with CQL

You can use CQL to query content properties in your Confluence instance. For example,
a plugin can store the number of likes on a page in a content property. You can create an index schema module for that property,
and then you can query the values to see how many pieces of content have more than 20 likes.

## GET, POST, and DELETE content properties

**GET content properties**

To retrieve any existing content properties for a piece of content, perform a **GET** on the
endpoint `/rest/api/content/{content_ID}/property`.

``` bash
# Retrieves content properties associated with a piece of content with ID 12345
curl -u admin:admin -X GET "http://localhost:8080/confluence/rest/api/content/12345/property" | python -mjson.tool
```

**POST new content properties**

To add content properties to a piece of Confluence content, perform a **POST** to the same endpoint. The key in your
content property can be an arbitrary string (like "myprop"), whereas the value must be a structured JSON.

``` bash
# Stores a JSON document under the key "myprop" against content with ID 12345
curl -i -u admin:admin -X POST -H "Content-Type: application/json" \
-d '{ "key" : "myprop", "value" : 
{
    "id": "507f1f77bcf86cd799439011",
    "editDate": "2000-01-01T11:00:00.000+11:00",
    "description": "If you have any questions please address them to admin@example.com",
    "content": {
        "likes": 5,
        "tags": ["cql", "confluence"]
    }
}
 }' http://localhost:8080/confluence/rest/api/content/12345/property
```

**DELETE content properties**

If you need to delete content properties, you can perform a **DELETE** on the
<a href="https://docs.atlassian.com/ConfluenceServer/rest/latest/#content/{id}/property" class="external-link">endpoint</a>.

``` bash
# Removes JSON document associated with key "myprop from content with ID 12345
curl -i -u admin:admin -X DELETE "http://localhost:8080/confluence/rest/api/content/12345/property/myprop"
```

## Fetch content properties as an expansion when retrieving content

Content properties are available as an expansion on the content resource. This allows content properties to be
retrieved in the same REST call as fetching the content itself. This expansion is available from any resource that
returns content, including the [CQL search resource](/server/confluence/advanced-searching-using-cql).

**GET content properties as an expansion on content.**

``` bash
# fetch properties at the same time as fetching content, note the expand=metadata.properties.myprop
curl -u admin:admin -X GET 
    "http://localhost:8080/confluence/rest/api/content/12345?expand=metadata.properties.myprop" | python -mjson.tool
{   
    id: "12345",
    type: "page",
    status: "current",
    title: "New in the platform team? Read me first",
    metadata: {
        _expandable: {
            currentuser: "",
            labels: "",
            properties: {
                myprop: {
                    "id": "507f1f77bcf86cd799439011",
                    "editDate": "2000-01-01T11:00:00.000+11:00",
                    "description": "If you have any questions please address them to admin@example.com",
                    "content": {
                            "likes": 5,
                            "tags": ["cql", "confluence"]
                    }
                }
            }
        }
    }
}
```

## CQL search extension

To allow searching of content properties using CQL, you can enable indexing of data stored as content properties by
defining an index schema. You can define an exclusive index schema for each content property key; then, whenever a
content property is saved, Confluence checks if a schema is defined for its key. If a schema exists, indexable values
are extracted from content property values and stored in an index. There can only be one index schema definition for
a content property key, so any duplicates are disabled. In the case of offending keys, an appropriate log message is available.

### Index schema definition

When defining an index schema for content properties, you need to provide a set of extraction expressions and field
type pairs, which are used to retrieve a specific value from a JSON document and transform it into the desired representation.
Supported index field types are: `string`, `text`, `number`, and `date`.

For an example, see the JSON document and its index schema later on this page.

*Example of content property value.*

``` javascript
{
    "id": "507f1f77bcf86cd799439011",
    "editDate": "2000-01-01T11:00:00.000+11:00",
    "description": "If you have any questions please address them to admin@example.com",
    "content": {
        "likes": 5,
        "tags": ["cql", "confluence"]
    }
}
```

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Extraction definition for a P2 plugin</th>
<th>Extracted value</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>&lt;extract path=&quot;id&quot; type=&quot;string&quot; /&gt;</code></td>
<td><code>&quot;507f1f77bcf86cd799439011&quot;</code></td>
</tr>
<tr class="even">
<td><code>&lt;extract path=&quot;editDate&quot; type=&quot;date&quot; /&gt;</code></td>
<td>1st of Jan 2000 00:00:00.000 UTC</td>
</tr>
<tr class="odd">
<td><code>&lt;extract path=&quot;content.tags&quot; type=&quot;string&quot; /&gt;</code></td>
<td>An array containing <code>&quot;cql&quot;</code>, <code>&quot;confluence&quot;</code></td>
</tr>
<tr class="even">
<td><code>&lt;extract path=&quot;content.likes&quot; type=&quot;number&quot; /&gt;</code></td>
<td>5</td>
</tr>
</tbody>
</table>

To access an embedded field in a document, use dot notation as shown in the following example. After successful validation,
all extracted values are stored inside an index, and you can address them in CQL queries.

Putting it all together, here's a sample index schema definition:

`atlassian-plugin.xml`

*Example of P2 add-on index schema definition*

``` xml
<content-property-index-schema key="module-key">
    <key property-key="attachments">
        <extract path="id" type="string" />
        <extract path="description" type="text" />
        <extract path="editDate" type="date" />
    </key>
    <key property-key="metadata">
        <extract path="content.tags" type="string" />
        <extract path="content.likes" type="number" />
    </key>
</content-property-index-schema>
```

`atlassian-connect.json`

*Example of connect add-on index schema definition.*

``` javascript
"confluenceContentProperties": [{
    "name": {
        "value" :"Attachments index",
        "i18n": "attachments.index"
    },
    "keyConfigurations": [{
        "propertyKey" : "attachments",
        "extractions" : [{
            "objectName" : "id",
            "type" : "string"
        }]
    }]
}]
```

*Note that index schemas for connect were not released in 5.7-OD-42.*

Field type doesn't only specify how data is stored in the index, but also determines which CQL operators are available in your query.

#### Supported index field types

<table>
<colgroup>
<col style="width: 10%" />
<col style="width: 80%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th>Type</th>
<th>Description</th>
<th>Supported CQL operators</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>string</code></td>
<td>Entire extracted value will be indexed as a single token, without any filtering. When extraction expression evaluates to a JSON array, each element will be indexed separately. Enables searching for an exact value, e.g. unique identifier.</td>
<td><code>IN</code>, <code>NOT IN</code>, <code>=</code>, <code>!=</code></td>
</tr>
<tr class="even">
<td><code>text</code></td>
<td>Extracted value will be tokenized before indexing, allowing searching for a particular words.</td>
<td><code>~</code>, <code>!~</code></td>
</tr>
<tr class="odd">
<td><code>number</code></td>
<td>Extracted number will be indexed as a <code>double</code> value for efficient range filtering and sorting.</td>
<td><code><</code>, <code><=</code>, <code>=</code>, <code>!=</code>, <code>></code>, <code>>=</code></td>
</tr>
<tr class="even">
<td><code>date</code></td>
<td>Two representation are possible, either a <code>String</code> following the <a href="http://en.wikipedia.org/wiki/ISO_8601" class="external-link">ISO 8601</a> datetime format, or a <code>long</code> value in the <a href="http://en.wikipedia.org/wiki/Unix_time" class="external-link">Unix time</a>. Enables efficient range filtering and sorting.</td>
<td><code><</code>, <code><=</code>, <code>=</code>, <code>!=</code>, <code>></code>, <code>>=</code></td>
</tr>
</tbody>
</table>

### Querying with CQL

You can address indexed content properties in a CQL query using the `content.property` field handler.
Any content that contains content properties matching the query is returned in the results.

The query syntax is as follows: `content.property[<KEY>].<PATH> <OPERATOR> value`.

*Examples of CQL queries on content properties.*

``` sql
content.property[attachments].editDate >= 2001-01-01
content.property[attachments].description ~ "questions"
content.property[metadata].content.tags IN ("cql", "help")
content.property[metadata].content.likes <= 5
```

Note the dot notation when referencing embedded fields like 'content.likes'.

**Legend**

<table>
<colgroup>
<col style="width: 10%" />
<col style="width: 90%" />
</colgroup>
<thead>
<tr class="header">
<th>Symbol</th>
<th>Meaning</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>KEY</td>
<td>The key of the content properties you search.</td>
</tr>
<tr class="even">
<td>PATH</td>
<td>The path to the value you'd like to search in the JSON document (use dot notation for embedded fields).</td>
</tr>
<tr class="odd">
<td>OPERATOR</td>
<td>One of the supported CQL operators for the field type.</td>
</tr>
</tbody>
</table>

## Read also

* [Custom actions with the blueprint API.](/server/confluence/custom-actions-with-the-blueprint-api/)
* [Expansions in the REST API.](/server/confluence/expansions-in-the-rest-api/)
* [Pagination in the REST API.](/server/confluence/pagination-in-the-rest-api/)
