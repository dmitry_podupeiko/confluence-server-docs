---
aliases:
- /server/confluence/2031766.html
- /server/confluence/2031766.md
category: devguide
confluence_id: 2031766
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031766
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031766
date: '2017-12-08'
legacy_title: How do I check which Jar file a class file belong to?
platform: server
product: confluence
subcategory: faq
title: How do I check which jar file a class file belong to?
---
# How do I check which jar file a class file belong to?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I check which Jar file a class file belong to?

Knowing a jar file where a stack trace originates from can be handy when troubleshooting for library conflict. If you want to find out, this can be easily done using <a href="#how-do-i-check-which-jar-file-a-class-file-belong-to" class="unresolved">user macros</a>.

For example:  
![](/server/confluence/images/foo.png)

You can copy and paste the following code, which is the same as the screenshot above:

``` java
<pre>$action.getClass().getClassLoader().loadClass("org.apache.commons.lang.StringUtils").getProtectionDomain().toString()</pre>
```

If the macro is run, it will print the path of the loaded jar file in your application server ie. the above user macro will print the file path to the jar file where `org.apache.commons.lang.StringUtils` class belongs.
