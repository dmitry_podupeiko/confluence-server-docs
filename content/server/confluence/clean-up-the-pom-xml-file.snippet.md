---
aliases:
- /server/confluence/clean-up-the-pom.xml-file-39368874.html
- /server/confluence/clean-up-the-pom.xml-file-39368874.md
category: devguide
confluence_id: 39368874
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368874
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368874
date: '2017-12-08'
legacy_title: Clean Up the POM.xml File
platform: server
product: confluence
subcategory: other
title: Clean Up the POM.xml File
---
# Clean Up the POM.xml File

The command **atlas-create-confluence-plugin** will create a POM.xml file at the top level.  This file is used by the Apache Maven system to figure out dependencies.  The POM provided by default works but has some warnings.  It's good to add some extra information about the plugin and take care of the warnings.

 When working with the POM.xml file, it's important to know changes will not be picked up till the maven is exited and restarted (**atlas-run**, **atlas-package** or **atlas-cli**)

The POM file supports property variables. The body of an xml child element of the properties elemnt with the name **X** and can be inserted anywhere else in the POM.xml file using the syntax **${X}**. See below for a quick example.

``` javascript
<useMyValue>${myValue}</useMyValue>
...
<properties>
   <myKey>myValue</myKey>
</properties>
```

### Setting the Plugin Version and Ownership

While the groupId, artifactId and version were set when the plugin was created with **atlas-create-confluence-plugin**, it's a good idea to edit the POM.xml file and provide real values for the name of the plugin and it's description.  This is also where the version number will be set.

``` javascript
    <groupId>com.atlassian.tutorial</groupId>
    <artifactId>newtest</artifactId>
    <version>1.0.0</version>

    <organization>
        <name>Example Company</name>
        <url>http://www.example.com/</url>
    </organization>

    <name>newtest</name>
    <description>This is the com.atlassian.tutorial:newtest plugin for Atlassian Confluence.</description>
```

{{% note %}}

In the atlassian-plugin.xml file, there will be references to the following variables. They are defined here in the POM.xml file

${project.groupId}  
${project.artifactId}  
${<a href="http://project.name" class="external-link">project.name</a>}  
${<a href="http://project.organization.name" class="external-link">project.organization.name</a>}  
${project.orgainzation.url} 

{{% /note %}}

### Plugin Version Specification

When starting the command line interface, maven may complain that some of the plugin versions have not been specified

{{% note %}}

    [INFO] Scanning for projects...
    [WARNING]
    [WARNING] Some problems were encountered while building the effective model for com.atlassian.tutorial:newtest:atlassian-plugin:1.0.0
    [WARNING] 'build.plugins.plugin.version' for org.apache.maven.plugins:maven-resources-plugin is missing. @ line 147, column 14
    [WARNING]
    [WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
    [WARNING]
    [WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
    [WARNING]

{{% /note %}}

It's unclear how critical this message is; however, it seems to be a pretty strong message.  Experimentally, the following plugins need to have the version explicitly called out to avoid the warning.

``` javascript
....
      <plugin>
         <artifactId>maven-resources-plugin</artifactId>
         <version>${maven.resources.plugin.version}</version>
      </plugin>

      <plugin>
         <artifactId>maven-compiler-plugin</artifactId>
         <version>${maven.compiler.plugin.version}</version>
      </plugin>
 
   </plugins>
...
   <properties>
...
      <maven.resources.plugin.version>2.7</maven.resources.plugin.version>
      <maven.compiler.plugin.version>3.3</maven.compiler.plugin.version>
   </properties>
```

It can be challenging to figure out what valid version numbers are.  When an invalid version number is specified and the project it built (**atlas-package**), maven will attempt to download the version, print out the url and then exit.  Browsing to the location with a regular web browser is an easy way to learn what versions for each of the plugins are available.

In general, the Atlassian maven plugins are hosted at the <a href="https://maven.atlassian.com/content/groups/public/org/apache/maven/plugins/" class="uri external-link">https://maven.atlassian.com/content/groups/public/org/apache/maven/plugins/</a>

### File Encoding Warnings

When building on Windows, the default files encoding defaults to Cp1252 which is not platform independant and maven will show the following to indicate this error.  UTF-8 is the recommended value.

{{% note %}}

\[WARNING\] Using platform encoding (Cp1252 actually) to copy filtered resources, i.e. build is platform dependent!

{{% /note %}}

To set the file encoding, it needs to be added to maven-confluence-plugin, maven-resources-plugin and maven-compiler-plugin.

``` javascript
....
   <plugins>
      <plugin>
         <groupId>com.atlassian.maven.plugins</groupId>
         ...
         <configuration>
            ...
            <encoding>${encoding}</encoding>
         </configuration>
      </plugin>
 
      ...


      <plugin>
         <artifactId>maven-resources-plugin</artifactId>
         ...
         <configuration>
            <encoding>${encoding}</encoding>
         </configuration>
      </plugin>

      <plugin>
         <artifactId>maven-compiler-plugin</artifactId>
         ...
         <configuration>
            <encoding>${encoding}</encoding>
         </configuration>
      </plugin>
   </plugins>
....
   <properties>
...
      <encoding>UTF-8</encoding>
   </properties></project>
```

### Manifest Warnings

When the plugin is first pushed to your development instance of Confluence, there may be a warning published such as:

``` javascript
[INFO] Manifest found, validating...
[WARNING] The manifest should contain versions for all imports to prevent 
ambiguity at install time due to multiple versions of a package.  Here are 
some suggestions for the maven-confluence-plugin configuration generated 
for this project to start from:
  <configuration>
    <instructions>
      <Import-Package>
        org.springframework.context.annotation;version="0.0.0",
        org.springframework.beans;version="3.2.10.RELEASE",
        org.w3c.dom;version="2.11.0-atlassian-01",
        com.atlassian.tutorials.helloworld.api;version="0.0.0",
        com.google.common.*;version="11.0.2",
        org.eclipse.gemini.blueprint.service.*;version="0.0.0",
        org.osgi.util.tracker;version="4.2.1",
        com.atlassian.plugin.osgi.factory;version="4.0.8",
        org.osgi.framework;version="4.2.1",
        org.apache.commons.logging;version="1.7.12",
        com.atlassian.sal.api;version="3.0.5",
        org.springframework.osgi.service.*;version="0.0.0",
        org.springframework.stereotype;version="2.0.8",
        org.springframework.beans.factory*;version="3.2.10.RELEASE",
        org.apache.commons.lang;version="2.6",
        org.springframework.util;version="3.2.8.RELEASE",
        javax.inject;version="1"
      </Import-Package>
    </instructions>
  </configuration>
You may notice many packages you weren't expecting.  This is usually because 
of a bundled jar that references packages that don't apply.  You can usually 
remove these or if necessary, mark them as optional by adding ';resolution:=optional' 
to the package import.  Packages that are detected as version '0.0.0' usually mean 
either they are JDK packages or ones that aren't referenced in your project, 
and therefore, likely candidates for removal entirely.
[INFO] Manifest validated
```

The culprit for the warning comes from the &lt;instructions&gt; element under the maven-confluence-plugin artifact in the POM.xml

``` javascript
<instructions>
    <Atlassian-Plugin-Key>${atlassian.plugin.key}</Atlassian-Plugin-Key>
        
    <!-- Add package to export here -->
    <Export-Package>
        com.atlassian.tutorials.helloworld.api,
    </Export-Package>
    
    <!-- Add package import here -->
    <Import-Package>
        org.springframework.osgi.*;resolution:="optional",
        org.eclipse.gemini.blueprint.*;resolution:="optional",
        *
    </Import-Package>
 
    <!-- Ensure plugin is spring powered - see https://extranet.atlassian.com/x/xBS9hQ  -->
    <Spring-Context>*</Spring-Context>
</instructions>
```

The recommendation from Atlassian is to be explicit about the list to avoid version issues; however, getting the list right is often described as part science and part art.  For more detail, see the page [Constructing the Dependency List in the POM.xml file's &lt;Import-Package&gt; element](/server/confluence/clean-up-the-pom-xml-file.snippet)
