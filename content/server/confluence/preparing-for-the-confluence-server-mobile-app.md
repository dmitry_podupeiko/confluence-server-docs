---
aliases:
- /server/confluence/preparing-for-the-confluence-server-mobile-app-52430665.html
- /server/confluence/preparing-for-the-confluence-server-mobile-app-52430665.md
category: devguide
confluence_id: 52430665
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=52430665
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=52430665
date: '2018-08-04'
legacy_title: Preparing for the Confluence Server mobile app
platform: server
product: confluence
subcategory: development resources
title: Preparing for the Confluence Server mobile app
---
# Preparing for the Confluence Server mobile app

This documentation is intended for Confluence developers who want to ensure that their existing plugins and Marketplace apps are compatible with the Confluence Server mobile app.

See [Confluence Mobile](https://confluence.atlassian.com/display/DOC/Confluence+Mobile) for information on compatible Confluence versions, and information on how to get the app.

## About the app

The Confluence Server mobile app is available for Confluence Server and Confluence Data Center. Its functionality is similar to the Confluence Cloud app, and allows viewing, creating and editing content. Apps are available for iOS and Android.

As with mobile web, some macros don't display in the app and users will need to view the page in their browser (or switch to full desktop mode on their device).  If your Marketplace app includes macros, you may want to make sure they display in the app, if possible.  

## How to make your macro display in the mobile app

Confluence automatically displays a "View desktop version" link whenever a macro can't be displayed on a page or blog post. 

To allow your macro display in the app, follow the steps below. 

### Configure your macro to support mobile mode

In the `atlassian-plugin.xml`, add the device type.  Here's an example from the JIRA Chart macro:

``` java
<xhtml-macro name='jirachart' class='com.atlassian.confluence.plugins.jiracharts.JiraChartMacro'
     key='jirachart-xhtml'
     icon="/download/resources/confluence.extra.jira/images/jira-chart-macro-icon.png"
     documentation-url="help.jirachart.macro">
  <category name="development"/>
  <device-type>mobile</device-type> 
  <parameters>
     <parameter name="server" type="string" required="true">
        <option key="showKeyInPlaceholder" value="false"/>
        <option key="showValueInPlaceholder" value="false"/>
     </parameter>
  </parameters>
</xhtml-macro>
```

If your plugin is using the Transformer form plugin you can skip this step, as the mobile app already supports the Transformer form plugin.  

### Render static content

Use the value of `context.getOutputDeviceType()` of execute method in `YourMacro` class to render mobile mode, as shown in this example. 

``` java
public class YourMacro implements Macro {
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        if("mobile".equals(context.getOutputDeviceType()) {
            //add your code to render in mobile here
        }
    }
}
```

At the moment, the mobile app only supports static content. You can use current PDF or email rendering in mobile. 

## If you're using a Transformer to render the view

### Configure your macro to support mobile mode

The mobile app already supports the Transformer form plugin, so you don't need to add any specific configuration to support it. 

### Render static content

This only applies for `chain="storageToView"` transformer (display mode). 

Use the value of `context.getOutputDeviceType()` of transform method in `YourTransformer` class to render mobile mode, as shown in this example. 

``` java
public class YourTransformer implemenent Transformer {
    public String transform(Reader reader, ConversionContext context) throws XhtmlException {
        if("mobile".equals(context.getOutputDeviceType()) {
            //add your code to render in mobile here
        }
    }
}
```

## How to test your macro

You can download the Confluence Server mobile app from the Google Play store or App store. 
