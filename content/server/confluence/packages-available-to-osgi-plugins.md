---
aliases:
- /server/confluence/packages-available-to-osgi-plugins-2031838.html
- /server/confluence/packages-available-to-osgi-plugins-2031838.md
category: devguide
confluence_id: 2031838
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031838
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031838
date: '2017-12-08'
guides: guides
legacy_title: Packages available to OSGi plugins
platform: server
product: confluence
subcategory: learning
title: Packages available to OSGi plugins
---
# Packages available to OSGi plugins

Below are the Java packages exposed by Confluence. All of them, along with their sub-packages, are available to OSGi plugins running in the [Atlassian Plugin Framework](https://developer.atlassian.com/display/PLUGINFRAMEWORK).

-   com.atlassian\*
-   com.sun\*
-   com.thoughtworks.xstream\*
-   bucket\*
-   net.sf.cglib\*
-   net.sf.hibernate\*
-   com.opensymphony.\*
-   org.apache\*
-   org.xml.\*
-   javax.\*
-   org.w3c.\*
-   org.dom4j\*
-   org.quartz\*
-   org.bouncycastle\*

Inside the application, this list is configured as a parameter to the `packageScanningConfiguration` component in the `pluginServiceContext.xml` file. The XML file is in the `services` folder within the `confluence/WEB-INF/lib/confluence-x.x.x.jar`.

##### RELATED TOPICS

[Converting a Plugin to Plugin Framework 2](/server/confluence/converting-a-plugin-to-plugin-framework-2)  
[Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)
