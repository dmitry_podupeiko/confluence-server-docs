---
aliases:
- /server/confluence/instruction-text-in-blueprints-18252354.html
- /server/confluence/instruction-text-in-blueprints-18252354.md
category: devguide
confluence_id: 18252354
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=18252354
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=18252354
date: '2018-04-09'
guides: guides
legacy_title: Instruction text in blueprints
platform: server
product: confluence
subcategory: learning
title: Instruction text in blueprints
---
# Instruction text in blueprints

## Including instructional text in blueprints

Blueprints (and other add-ons) can make use of the new *instructional text* feature for templates. The purpose of instructional text is to:

*   Make it easy for a template creator to include information on how to fill out the template.
*   Make it easy for the template end users to fill out a template. 
This is done using the following features:
    *   Automatically clear all instructional text as a user types in a specific text block.
    *   Automatically trigger a @mention prompt for user selection.
    *   Automatically trigger the Jira Issues macro dialog to let a user search for or create an issue.

<img src="/server/confluence/images/instructional-text-button-example.png" width="1011" height="127" />

You can insert instructional text into a blueprint or template using the `<ac:placeholder>instruction text</ac:placeholder>` notation in the storage format.

There is also an optional `ac:type` attribute that specifies the type of instructional text that the placeholder represents.
Confluence provides support for the following types: 

*   `mention`  – activates the user search autocomplete to insert a user mention into the page. 
*   `jira` – launches the Jira Issues macro dialog and lets a user to search for or create an issue. 

Here is a sample storage format for an instructional text:

``` xml
<ul>
    <li><ac:placeholder>This is an example of instruction text that will get replaced when a user selects the text and begins typing.</ac:placeholder></li>
    <li><ac:placeholder ac:type="jira">This placeholder will launch the Jira Issues macro dialog when clicked</ac:placeholder></li>
</ul>
<ac:task-list>
    <ac:task>
        <ac:task-status>incomplete</ac:task-status>
        <ac:task-body><ac:placeholder ac:type="mention">@mention example. This placeholder will automatically search for a user to mention in the page when the user begins typing.</ac:placeholder></ac:task-body>
    </ac:task>
</ac:task-list>
```

## Adding instruction text types

It is possible to add support for additional types of instructional text via a plugin.
To do this, add a `tinymce` plugin inside your plugin as follows.

### Add a new JavaScript file to extend the editor

The following code sample is a skeleton file to add support for another type of instructional text.

**editor\_plugin\_src.js**

``` javascript
(function($) {
    tinymce.create('tinymce.plugins.InstructionalTextExample', {
        init : function(ed) {
            // This adds support for this type of instructional text into the template editor
            if(AJS.Rte.Placeholder && AJS.Rte.Placeholder.addPlaceholderType) {
                AJS.Rte.Placeholder.addPlaceholderType({
                    type: 'example',
                    label: AJS.I18n.getText("property.panel.textplaceholder.display.example"),
                    tooltip: AJS.I18n.getText("property.panel.textplaceholder.display.example.tooltip"),
                    // The following defines how the placeholder can be activated. It is optional, and if omitted will have the following default values
                    activation: {
                        click: false,
                        keypress: true
                    }
                });
            }
            // This adds support to responding to this instruction text being replaced in the editor
            AJS.bind('editor.text-placeholder.activated', function(e, data) {
                if(data && data.placeholderType === "example") {
                    // do something special here for this type of instruction text
                }
            });
        },
        getInfo : function() {
            return {
                longname : 'Instructional Text Type Example',
                author : 'Atlassian',
                authorurl : 'http://www.atlassian.com',
                version : tinymce.majorVersion + "." + tinymce.minorVersion
            };
        }
    });
    tinymce.PluginManager.add('instructionaltextexample', tinymce.plugins.InstructionalTextExample);
})(AJS.$);


AJS.Rte.BootstrapManager.addTinyMcePluginInit(function(settings) {
    settings.plugins += ",instructionaltextexample";
});
```

Here is an example of `web-resource` and i18n `resource` for `atlassian-plugin.xml` file:

``` xml
    <resource type="i18n" name="i18n" location="instructional-text-example" />

    <web-resource key="editor-example-instructional-text-resources" name="Example Instruction Text Resources">
        <description>Example Instruction Text Resources</description>
        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>
        <resource name="tinyMce-plugins-example-instructional-text.js" type="download" location="js/editor_plugin_src.js"/>
        <context>editor</context>
        <dependency>com.atlassian.auiplugin:ajs</dependency>
    </web-resource>
```

Here is an example of i18n properties in `instructional-text-example.properties`:

``` properties
property.panel.textplaceholder.display.example=Example Instructional Text Type
property.panel.textplaceholder.display.example.tooltip=This is only example
```
