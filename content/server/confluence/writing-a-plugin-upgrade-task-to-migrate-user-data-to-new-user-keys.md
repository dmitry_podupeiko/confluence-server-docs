---
aliases:
- /server/confluence/writing-a-plugin-upgrade-task-to-migrate-user-data-to-new-user-keys-20218517.html
- /server/confluence/writing-a-plugin-upgrade-task-to-migrate-user-data-to-new-user-keys-20218517.md
category: devguide
confluence_id: 20218517
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=20218517
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=20218517
date: '2017-12-08'
legacy_title: Writing a Plugin Upgrade Task to Migrate User Data to New User Keys
platform: server
product: confluence
subcategory: updates
title: Writing a plugin upgrade task to migrate user data to new user keys
---
# Writing a plugin upgrade task to migrate user data to new user keys

With Confluence 5.2, we are introducing schema and API changes necessary to support the ability to change usernames. To make that possible, users will have a new, unique, permanent key as well as the already-existing, unique, changeable username. See [Renamable Users in Confluence 5.2](/server/confluence/renamable-users-in-confluence-5-2).

This page tells you how to create an upgrade task for your plugin, to migrate existing user data from older versions of Confluence, on upgrade to Confluence 5.2 and later.

{{% note %}}

The complete rename user functionality will be delivered in 5.3. However the schema and API changes necessary will be released in 5.2 and recommend developers begin updating their add-ons now.

{{% /note %}}

## Using SAL as a framework for your upgrade task

SAL (the [Atlassian Shared Access Layer](https://developer.atlassian.com/display/DOCS/Shared+Access+Layer)) provides a plugin upgrade manager that listens for the 'Framework started' event and looks for plugin upgrade tasks to run. As a plugin developer, you can provide a plugin upgrade task component, and SAL will guarantee that the upgrade task is run on Confluence startup.

## Step 1. Check prerequisites

Your plugin needs to use version 2 of the Atlassian Plugin Framework. Check that you have the `plugins-version="2"` attribute in `atlassian-plugins.xml`:

``` xml
<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" system="true" plugins-version="2">
...
```

## Step 2. Add dependencies on SAL and the Confluence User Compatibility Library

Your plugin project needs access to the SAL API and Confluence User Compatibility library. Add the following dependencies to the plugin's `pom.xml`:

``` xml
...
  <dependency>
     <groupId>com.atlassian.sal</groupId>
     <artifactId>sal-api</artifactId>
     <version>2.10.0</version>
     <scope>provided</scope>
  </dependency>
  <dependency>
     <groupId>com.atlassian.usercompatibility</groupId>
     <artifactId>usercompatibility-confluence</artifactId>
     <version>2.1</version>
  </dependency>
...
```

## Step 3. Write the upgrade task

Write a class that implements the `PluginUpgradeTask` interface provided by SAL. Here is an example implementation:

``` java
package com.atlassian.example;

import com.atlassian.confluence.usercompatibility.UserCompatibilityHelper;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.atlassian.sal.usercompatibility.UserKey;

import java.util.Collection;
import java.util.Collections;

public class UserUpgradeTask implements PluginUpgradeTask
{
    PluginSettings settings;

    public UserUpgradeTask(PluginSettingsFactory pluginSettingsFactory)
    {
        settings = pluginSettingsFactory.createGlobalSettings();
    }

    @Override
    public int getBuildNumber()
    {
        return UserCompatibilityHelper.isRenameUserImplemented() ? 1 : 0;
    }

    @Override
    public String getShortDescription()
    {
        return "Migrates username property to userKey property";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception
    {
        String username = (String) settings.get("com.atlassian.example:username");
        UserKey userKey = UserCompatibilityHelper.getKeyForUsername(username);
        settings.put("com.atlassian.example:userKey", userKey.getStringValue());
        settings.remove("com.atlassian.example:username");
        return Collections.emptySet();
    }

    @Override
    public String getPluginKey()
    {
        return "com.atlassian.example";
    }
}
```

## Step 4. Register the upgrade task in your plugin

Add the upgrade task as a component in the plugin:

``` java
...
 <component key="userUpgradeTask" name="User Upgrade Task"
 class="com.atlassian.example.UserUpgradeTask" public="true">
     <interface>com.atlassian.sal.api.upgrade.PluginUpgradeTask</interface>
 </component>
...
```

The PluginUpgradeManager in SAL will automatically scan for components that implement the `PluginUpgradeTask` interface. Please note that they have to be declared as `public="true"`.

## Step 5. Repackage and deploy

Repackage the plugin, deploy it to the instance of Confluence that you want to upgrade and restart the Confluence instance. The plugin upgrade task should be executed when Confluence starts up.
