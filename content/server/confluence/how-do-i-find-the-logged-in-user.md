---
aliases:
- /server/confluence/2031739.html
- /server/confluence/2031739.md
category: devguide
confluence_id: 2031739
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031739
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031739
date: '2019-04-18'
legacy_title: How do I find the logged in user?
platform: server
product: confluence
subcategory: faq
title: How do I find the logged in user?
---

# How do I find the logged in user?

This page describes how to access the logged in user when writing a Confluence app. 

## Confluence prior to 5.2

For Confluence versions prior to 5.2, the user can be retrieved from the 
`com.atlassian.confluence.user.AuthenticatedUserThreadLocal` class, which will give you the current 
logged in user as a `com.atlassian.confluence.user.ConfluenceUser` object.

``` java
ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
```
Note that if there is no logged in user, then the user object returned will be `null`.

## Confluence 5.2 and above

`AuthenticatedUserThreadLocal` was deprecated in Confluence 5.2. For newer versions use 
`com.atlassian.confluence.user.AuthenticatedUserAccessor`.

``` java
Optional<ConfluenceUser> confluenceUser = AuthenticatedUserAccessor.get();
```
Which returns the currently authenticated `com.atlassian.confluence.user.ConfluenceUser`, if any.
