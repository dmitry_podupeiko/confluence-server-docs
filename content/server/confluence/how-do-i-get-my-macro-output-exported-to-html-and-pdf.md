---
aliases:
- /server/confluence/2031811.html
- /server/confluence/2031811.md
category: devguide
confluence_id: 2031811
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031811
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031811
date: '2017-12-08'
legacy_title: How do I get my macro output exported to HTML and PDF?
platform: server
product: confluence
subcategory: faq
title: How do I get my macro output exported to HTML and PDF?
---
# How do I get my macro output exported to HTML and PDF?

{{% note %}}

This is only applies to Confluence 2.7 and higher.

{{% /note %}}

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I get my macro output exported to HTML and PDF?

Macros such as the chart macro may produce images, which should be included in HTML and PDF exports. This is now possible if macros delegate the responsibility of storing the output to Confluence.

#### ExportDownloadResourceManager

The <a href="http://docs.atlassian.com/com/atlassian/confluence/atlassian-confluence/latest-server/com/atlassian/confluence/importexport/resource/ExportDownloadResourceManager" class="external-link">ExportDownloadResourceManager</a> is responsible for managing the reading and writing of macro output. Confluence uses this manager to lookup/retrieve macro output for downloads and exports. Hence, if you would like your macro to support exports, it is required that you use this manager to retrieve the correct writer to write to.

**ExportDownlaodResourceManager**

``` java
   /**
     * Returns a DownloadResourceReader for reading the stored output of the previous execution of a macro.
     * Typically used by HTML and PDF export, macro content downloads.
     *
     * @param userName the user who is viewing the macro output. Must be the same as the user who created the macro
     * output with {@link #getResourceWriter(String, String, String)}, or an UnauthorizedDownloadResourceException
     * will be thrown.
     * @param resourcePath the relative URL of the resource including the application context path. For example,
     * "/confluence/download/temp/chart1756.png". It must be the same path from the {@link DownloadResourceWriter}.
     * @throws UnauthorizedDownloadResourceException if the user requesting the macro output is different to the user
     * who created it
     * @throws DownloadResourceNotFoundException if a stored macro output associated with this resource path cannot be
     * found
     */
    public DownloadResourceReader getResourceReader(String userName, String resourcePath, Map parameters)
        throws UnauthorizedDownloadResourceException, DownloadResourceNotFoundException

   /**
     * Returns a DownloadResourceWriter for storing output of a macro in a temporary location.
     * This should be typically called by macros that generate output such as images and would like their
     * output to be exported correctly.
     *
     * @param userName the user who is creating the macro output.
     * @param prefix the prefix of the macro output's name
     * @param suffix the suffix of the macro output
     */
    public DownloadResourceWriter getResourceWriter(String userName, String prefix, String suffix)
```

The following is an example of how to retrieve the output stream for which you can use to write your macro output to.

``` java
public class ExampleMacro extends BaseMacro
{
   private ExportDownloadResourceManager exportDownloadResourceManager;

   public void setExportDownloadResourceManager(ExportDownloadResourceManager exportDownloadResourceManager)
   {
      this.exportDownloadResourceManager = exportDownloadResourceManager;
   }

   public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException 
   {
      // parse parameters and generate the output/image
      ....  

      // get the current user 
      User user = AuthenticatedUserThreadLocal.getUser();
      String userName = user == null ? "" : user.getName();

      // get the resource writer
      DownloadResourceWriter writer = exportDownloadResourceManager.getResourceWriter(userName, "example", "png");
      OutputStream outputStream = writer.getStreamForWriting();

      try
      {
         // write to the output stream
         .....
      }
      finally
      {
         // close the output stream
         if(outputStream != null) 
            outputStream.close();
      }

      return "<img src=\"" + writer.getResourcePath() + "/>";
   }
}
```
