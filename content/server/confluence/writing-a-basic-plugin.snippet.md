---
aliases:
- /server/confluence/writing-a-basic-plugin-33723233.html
- /server/confluence/writing-a-basic-plugin-33723233.md
category: devguide
confluence_id: 33723233
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33723233
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33723233
date: '2017-12-08'
legacy_title: Writing a Basic Plugin
platform: server
product: confluence
subcategory: other
title: Writing a Basic Plugin
---
# Writing a Basic Plugin
