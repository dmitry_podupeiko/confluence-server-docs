---
aliases:
- /server/confluence/extending-autoconvert-8947120.html
- /server/confluence/extending-autoconvert-8947120.md
category: devguide
confluence_id: 8947120
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8947120
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8947120
date: '2018-04-13'
legacy_title: Extending Autoconvert
platform: server
product: confluence
subcategory: learning
title: Extending autoconvert
---
# Extending autoconvert

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to Confluence Server 5.9.1 and higher.</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>Beginner.</p></td>
</tr>
<tr class="odd">
<td><p>Time estimate:</p></td>
<td><p>It should take you about 1 hour to complete this tutorial.</p></td>
</tr>
</tbody>
</table>

## Tutorial overview

This tutorial shows you how to create a plugin that extends Confluence autoconvert. Your plugin converts URL text into hyperlinked title text.
The conversion appears dynamically as you edit in Confluence and add URLs from the [https://developer.atlassian.com](https://developer.atlassian.com) domain.
In other words, your plugin converts URL text like the following from 

``` bash
https://developer.atlassian.com/server/confluence/extending-autoconvert/
```

into a link that is displayed as [Extending autoconvert](https://developer.atlassian.com/server/confluence/extending-autoconvert/).  
This particular plugin converts links from the *developer.atlassian.com* domain as an example, but could be modified for any domain.

**Concepts covered in this tutorial:**

-   Using a JavaScript file to provide an Autoconvert handler.
-   Using a `parseURI` JavaScript object to interpret URLs.

### Before you begin

To complete this tutorial, you should:

* Understand the basics of JavaScript development. 

### Plugin source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you have finished,
you can find the plugin source code on Atlassian Bitbucket.
To clone the repository, run the following command:

``` bash
git clone git@bitbucket.org:atlassian_tutorial/extending-autoconvert-in-confluence.git
```

Alternatively, you can download the source as [zip archieve](https://bitbucket.org/atlassian_tutorial/extending-autoconvert-in-confluence/get/rb-master-extending-autoconvert.zip).

{{% note %}}

About these instructions


You can use any supported combination of operating system and IDE to create this plugin. These instructions were written using
IntelliJ IDEA 2017.2 on macOS Sierra. If you are using another operating system or IDE combination, you should use
the equivalent operations for your specific environment.

This tutorial was last tested with Confluence 6.8.0 using the Atlassian SDK 6.3.10.

{{% /note %}}

## Step 1. Create and prune the plugin skeleton

In this step, you'll create a plugin skeleton using `atlas-` commands. Because you won't need some of the files created in the skeleton,
you'll also delete them in this step.

1.  Open a Terminal on your machine and navigate to directory where you normally keep your plugin code.  

1.  To create a Confluence plugin skeleton, run the following command:

    ``` bash
     atlas-create-confluence-plugin
    ```

1.  To identify your plugin, enter the following information:

    <table>
    <colgroup>
    <col style="width: 30%" />
    <col style="width: 70%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence</code></p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>confluence-autoconvert-dev-docs</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence</code></p></td>
    </tr>
    </tbody>
    </table>

1.  Confirm your entries with `Y` or `y`.

    Your terminal notifies you of a successful build.

1.  Navigate to the project directory created in the previous step.

    ``` bash
     cd confluence-autoconvert-dev-docs/
    ```

1.  Delete the test directories.

    Setting up testing for your plugin isn't part of this tutorial. To delete the generated test skeleton, run the following commands:

    ``` bash
     rm -rf ./src/test/java
     rm -rf ./src/test/resources/
    ```

1.  Delete the unneeded Java class files.

    ``` bash
     rm -rf ./src/main/java/com/example/plugins/tutorial/confluence/*
    ```

1. Import your project into favorite your IDE

## Step 2. Write the JavaScript for the `autoconvert` handler

Your plugin uses JavaScript to parse the URLs and return the title text. When you create your plugin project skeleton,
it automatically includes a directory for `js` and an empty file titled `confluence-autoconvert-dev-docs.js`.

In this step,
you'll add a `bind` method so your code is executed when the `init.rte` event occurs, that is when you open page editor in Confluence. You'll
also register `pasteHandler` for TinyMCE and add a condition so your plugin is executed only on `https://developer.atlassian.com` links.  

If URLs meet the criteria of the `if` conditions, you'll extract title from `URI`. This enables your plugin
to parse a URL, splitting each section after the initial `//` into single `(/)` forward slashes.

Your plugin converts http://developer.atlassian.com URLs to be displayed as page titles, so it needs to parse 4 parts of the URL: 

``` bash
https://developer.atlassian.com/server/confluence/extending-autoconvert
```

If split at the forward slashes, the link contains `developer.atlassian.com`, `server` or `cloud`, `platform`,
and the actual page title. You'll write JavaScript to use the fourth section as display text.

1.  Open `confluence-autoconvert-dev-docs.js` from `src/main/resources/js`.

1.  Define a module for your converter

    ``` javascript
        define('example/autoconvert/dacconverter', ['tinymce'], function (tinymce) {
                    "use strict";
                function DacConverter() {
                }

                return DacConverter;
         });
    ```

1.  Add a `bind` method to listen for the `init.rte` event.

    The `init.rte` event is when the editor in Confluence is loaded.

    ``` javascript
       require('confluence/module-exporter').safeRequire('example/autoconvert/dacconverter', function (DacConverter) {
           require('ajs').bind("init.rte", DacConverter);
       });
    ```

1.  Create a handler that can later call the continuation `done()`.

    Add a `pasteHandler` function within the curly brackets of the `DacConverter` method. 

    Include the following arguments:

    *  `uri` – to reference a `parseURI` object.
    * `node` – the jQuery object.
    * `done` – to act as a continuation function for `pasteHandler` to call.

    The `parseURI` object interprets URLs to get the title of the page. You'll add additional code inside the closing curly bracket `}`.

    ``` javascript
        function pasteHandler(uri, node, done) {
        }
    ```

1.  Add and assign `directoryParts`.   
    Because the URL is separated by forward slashes, you'll include this in the assignment statement:

    ``` javascript
        var directoryParts = uri.directory.split('/'),
            pageName;
    ```

1.  To only apply the autoconvert plugin to `http://developer.atlassian.com` pages, nest a condition.

    Your condition verifies that the URL qualifies for the autoconvert action by checking for four parts in the URL and that the text matches the `uri.source`.

    ``` javascript
        if (uri.host === "developer.atlassian.com" &&
            directoryParts.length >= 4 && directoryParts[4] === "" &&
            (directoryParts[1] === "server" || directoryParts[1] === "cloud") &&
            uri.anchor === "" &&
            node.text() === uri.source) {
    ```

1.  Define `pageName` as the fourth parameter ( `[3]`) and replace URL formatting with a space. 

    The `done(node)` method makes `node` available for additional URLs. 

    ``` javascript
         pageName = decodeURIComponent(directoryParts[3]);
         pageName = pageName.charAt(0).toUpperCase() + pageName.slice(1).replace(/-/g, " ");

         node.text(pageName);
         done(node);
    ```

    {{% warning %}}

Call `done()` once in all code paths

For autoconvert to work, call `done()` exactly one time in all possible code paths. You can call `done()` with
no arguments if you don't want to change the link, or you can pass the replacement or modified node to change the link.

    {{% /warning %}}

1.  To finish your alternation, direct ineligible URLs to the `done` method.

    To finalize the code path, call `done()` again. This completes the block, and uses the closing `};` that you added in step 1.

    ``` javascript
            } else {
                done();
            }
    ```

1.  Register the handler right after `pasteHandler` function.

    ``` javascript
    tinymce.plugins.Autoconvert.autoConvert.addHandler(pasteHandler);
    ```

1.  Save the changes and close the file.

Here's what your `confluence-autoconvert-dev-docs.js` file should look like at completion:

``` javascript
define('example/autoconvert/dacconverter', ['tinymce'], function (tinymce) {
    "use strict";

    function DacConverter() {
        function pasteHandler(uri, node, done) {
            var directoryParts = uri.directory.split('/'),
                pageName;
            if (uri.host === "developer.atlassian.com" &&
                directoryParts.length >= 4 && directoryParts[4] === "" &&
                (directoryParts[1] === "server" || directoryParts[1] === "cloud") &&
                uri.anchor === "" &&
                node.text() === uri.source) {

                pageName = decodeURIComponent(directoryParts[3]);
                pageName = pageName.charAt(0).toUpperCase() + pageName.slice(1).replace(/-/g, " ");

                node.text(pageName);
                done(node);
            } else {
                done();
            }
        }

        tinymce.plugins.Autoconvert.autoConvert.addHandler(pasteHandler);
    }

    return DacConverter;
});

require('confluence/module-exporter').safeRequire('example/autoconvert/dacconverter', function (DacConverter) {
    require('ajs').bind("init.rte", DacConverter);
});
```

## Step 3. Add a Web Resource module to the `atlassian-plugin.xml` descriptor 

To describe how Confluence should interact with your JavaScript file, you'll now add a `<web-resource>` module
to the `atlassian-plugin.xml` file. The `atlassian-plugin.xml` file also generates when you create your plugin skeleton. 

This module type defines the locations of other resources for your plugin to use, so to handle the conversion of URLs,
you can use the `confluence-autoconvert-dev-docs.js` that you wrote earlier . While it's possible to add
this module via the `atlas-create-confluence-plugin-module` command, it's easier in this case to add it directly to your descriptor file.

1.  Open `atlassian-plugin.xml` from `src/main/resources`.  

1.  Find the comment line `<!-- add our web resources-->`.  
    Resources are automatically generated when you create the plugin skeleton.  

1.  To define the location of your JavaScript file, replace automatically generated  `<web-resource>` with the following code:

    ``` xml
        <web-resource key="autoconvert-dev-docs" name="Autoconvert developer.atlassian.com example handler">
            <description>Changes link text for URLs pasted from https://developer.atlassian.com.</description>
            <resource type="download" name="confluence-autoconvert-dev-docs.js" location="js/confluence-autoconvert-dev-docs.js"/>
            <!-- This will ensure the resource is loaded after autoconvert, and only if autoconvert is enabled. -->
            <dependency>com.atlassian.confluence.plugins.confluence-paste:autoconvert-core</dependency>
            <dependency>com.atlassian.auiplugin:ajs</dependency>
            <!-- Assuming the dependency above is met, this context means that whenever the editor is loaded, so is your autoconvert handler. -->
            <context>editor</context>
        </web-resource>
    ```

1.  Save and close the file.  

Your `atlassian-plugin.xml` should resemble the following:

``` xml
<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>

    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="confluence-autoconvert-dev-docs"/>

    <web-resource key="autoconvert-dev-docs" name="Autoconvert developer.atlassian.com example handler">
        <description>Changes link text for URLs pasted from https://developer.atlassian.com.</description>
        <resource type="download" name="confluence-autoconvert-dev-docs.js" location="js/confluence-autoconvert-dev-docs.js"/>
        <!-- This will ensure the resource is loaded after autoconvert, and only if autoconvert is enabled. -->
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.confluence.plugins.confluence-paste:autoconvert-core</dependency>
        <!-- Assuming the dependency above is met, this context means that whenever the editor is loaded, so is your autoconvert handler. -->
        <context>editor</context>
    </web-resource>
</atlassian-plugin>
```

## Step 4. Build, install, and run your plugin

In this step, you'll install your plugin and run Confluence. Then, to test your autoconvert plugin, you'll add links to a
Confluence page from developer.atlassian.com.

1.  Open a Terminal window and navigate to the plugin root folder. 

    ``` bash
     cd confluence-autoconvert-dev-docs
    ```

1.  To start Confluence from your project root, run the following command:

    ``` bash
    atlas-run
    ```

    This command builds your plugin code, starts a Confluence instance, and installs your plugin. This may take a few minutes.

1.  Find the URL for Confluence.  
    Your Terminal outputs the location of your local Confluence instance, that is defaulted to http://localhost:1990/confluence.

    ``` bash
    [INFO] confluence started successfully in 71s at http://localhost:1990/confluence
    [INFO] Type CTRL-D to shutdown gracefully
    [INFO] Type CTRL-C to exit
    ```

1.  Open your local Confluence instance log in with `admin` / `admin`.
1.  To create a new Confluence page, click **Create**.
1.  Copy and paste various links into your page, and confirm that they convert to their page titles.  
    Here's a handful you can use:

    * https://developer.atlassian.com/server/confluence/extending-autoconvert/
     
    * https://developer.atlassian.com/server/confluence/space-blueprints/
     
    * https://developer.atlassian.com/server/confluence/searching-using-the-v2-search-api/

1.  Verify the links transform into titles as you cut and paste them onto the page.   
    Here's an example of the links as they appear:

    <img src="/server/confluence/images/finalautoconvert.png" title="Verify the URLs autoconvert" alt="Verify the URLs autoconvert" width="990" />
