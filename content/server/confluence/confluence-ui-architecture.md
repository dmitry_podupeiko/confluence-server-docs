---
aliases:
- /server/confluence/confluence-ui-architecture-2031653.html
- /server/confluence/confluence-ui-architecture-2031653.md
category: devguide
confluence_id: 2031653
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031653
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031653
date: '2017-12-08'
guides: guides
legacy_title: Confluence UI architecture
platform: server
product: confluence
subcategory: learning
title: Confluence UI architecture
---
# Confluence UI architecture

### Rendering frameworks

There are two frameworks that do the template rendering in Confluence: Webwork and Sitemesh. The confusing bit is that both of them use Velocity as their templating engine. We try to distinguish them by using \*.vm for templates processed by Webwork, and \*.vmd for those processed by Sitemesh.

### Rendering contexts

There are four different Velocity contexts used in Confluence:

-   templates processed by Webwork use the context defined in ConfluenceVelocityContext
-   templates processed by Sitemesh as a result of the `#applyDecorator()` directive use the context defined in ApplyDecoratorDirective
-   templates processed by Sitemesh as a result of the URL mapping in decorators.xml use the context defined in ProfilingPageFilter
-   templates processed by the notification queue use the context defined in VelocityRenderedQueueItem.

The two Sitemesh contexts are pretty much the same, but the Webwork velocity context contains a lot more stuff than either of the Sitemesh ones.

### Logical structure

The following diagram shows the logical structure of the Confluence UI.

![](/server/confluence/images/ui-logical-2.9.png)  
*Confluence UI Architecture - Logical Structure*

### Rendering pipeline

The following diagram shows the flow of control through the Confluence UI.

![](/server/confluence/images/ui-execution-flow-2.9.png)  
*Confluence UI Architecture - Execution Flow*

In more detail, the flow of control goes:

-   Webwork gets request, maps request URL to action using xwork.xml
-   Webwork maps response of action to a Velocity template using xwork.xml
-   Webwork launches Velocity handler on template (\*.vm) with context defined in ConfluenceVelocityContext
-   Velocity process content in \*.vm file
-   Within an `#applyDecorator()` directive:
    -   Velocity calls the ApplyDecoratorDirective class with the parameters and body content of the directive
    -   Any `#decoratorParam()` directives are processed by the ParamDirective class, which pushes bits of the current Velocity context into the ApplyDecoratorDirective parameters
    -   ApplyDecoratorDirective matches the name parameter of the directive with a \*.vmd file from decorators.xml
    -   ApplyDecoratorDirective launches Sitemesh on a decorator template (\*.vmd) with context defined in ApplyDecoratorDirective
    -   Sitemesh returns decorated content
-   Velocity template finished processing rest of \*.vm file, returns to Webwork
-   Web.xml servlet filter 'sitemesh' maps to ProfilingPageFilter, a Sitemesh page filter
-   Sitemesh uses the request URL mapping in decorators.xml to launch a decorator template (\*.vmd) with context defined in ProfilingPageFilter
-   Sitemesh returns decorated content as response.

You can find out which beans are in which context by looking in the classes above. A full list would be too long to include here. Note that even though the ApplyDecoratorDirective launches a Sitemesh decorator template, the Sitemesh template doesn't get automatic access to the Velocity context. The only bits that are passed through are done with the `#decoratorParam()` directive.

Wow, pretty complicated. But it lets us do cool stuff like implement custom themes, apply layouts and more.
