---
aliases:
- /server/confluence/package-and-install-a-plugin-39368885.html
- /server/confluence/package-and-install-a-plugin-39368885.md
category: devguide
confluence_id: 39368885
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368885
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368885
date: '2017-12-08'
legacy_title: Package and Install a Plugin
platform: server
product: confluence
subcategory: other
title: Package and Install a Plugin
---
# Package and Install a Plugin

{{% warning %}}

The **pi** command does not support the Transformerless plugin development. Use QuickReload instead.

{{% /warning %}}

 

The **pi** command (Package & Install) in the **atlas-cli** shell will build, package and install the plugin into the running SDK instance of confluence.  This command can be re-run every time a change is made to any of the files in the plugin.  

``` javascript
maven> pi
```

{{% note %}}

The "pi" command (Package & Install) will build the plugin in the directory from which the CLI was launched and push it to the SDK instance of Confluence. The version of the plugin will be the one pulled from the pom.xml file when the CLI was launched. (Changes to the pom.xml file are not picked up until the CLI is restarted.)

When developing multiple plugins, it can be a helpful trick to use one of them as the primary Confluence instance, then lanuch the cli from the other plugins to build and push the code to the primary Confluence instance.  

To push the code to a non-SDK Confluence instance, the pi command builds the plugin as a jar file located in "&lt;plugin-dir&gt;/target". A confluence Administrator can upload that jar file to any instance of Confluence using the Universal Plugin Manager.

{{% /note %}}
