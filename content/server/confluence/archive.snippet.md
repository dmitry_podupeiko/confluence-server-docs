---
aliases:
- /server/confluence/-archive-24805487.html
- /server/confluence/-archive-24805487.md
category: devguide
confluence_id: 24805487
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24805487
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24805487
date: '2017-12-08'
legacy_title: _Archive
platform: server
product: confluence
subcategory: other
title: _Archive
---
# \_Archive

-   [Archived: Creating a Confluence Task Plugin with REST](/server/confluence/archived-creating-a-confluence-task-plugin-with-rest.snippet)
