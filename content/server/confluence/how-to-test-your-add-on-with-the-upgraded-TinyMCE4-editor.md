---
category: devguide
date: '2019-01-04'
platform: server
product: confluence
subcategory: learning
guides: development resources
title: "How to test your add-on with the upgraded TinyMCE 4 editor"
---
# How to test your app with the upgraded TinyMCE 4 editor

We've been working on on upgrading [TinyMCE](https://www.tiny.cloud/docs/) from version 3 to version 4. The dark feature for the upgraded editor will be enabled by default in Confluence 6.14.0 and later.  

If you have an app that interacts with the editor in any way, we strongly recommend you test your app with the upgraded editor as soon as possible.  

## Compatibility

Confluence 6.14 is fully backward compatible with the TinyMCE v3 API, and we don’t plan to break compatibility anytime soon. However we recommend using the TinyMCE 4 API wherever possible when you need to interact directly with the editor. 

You can load editor resources in either the editor-v3 or editor-v4 context if you need something different from either of them. However we recommend you keep using the editor context as you did before, loading the same resources regardless of what version of the editor.  


## Does your app load the editor?

Does your app load the editor in its own views? If so, you will need to do the following.

### Editor context

Load additional web-resource context: editor-v4 along with the existing editor. 

For example, if you're using `requireResourcesForContext('editor')`, you should use `requireResourcesForContext('editor-v4,editor');`.

This only applies when `frontend.editor.v4` is enabled. Technically, in v3 you should 
use `requireResourcesForContext('editor-v3,editor');`, but this will generally not be necessary. You can use the `DarkFeaturesManager` in java or the `confluence/dark-features` AMD module in JS to load either v3 or v4 based on the `frontend.editor.v4` dark feature, to be ready for both. For example:

###### DarkFeaturesManager in Java

``` java
darkFeaturesManager.getDarkFeatures().isFeatureEnabled("frontend.editor.v4") // boolean
```

###### confluence/dark-features AMD module in JavaScript

``` javascript
require('confluence/dark-features').isEnabled("frontend.editor.v4") // boolean
```

### Editor tear down

If you also handle destroying the editor, make sure to use the `mceRemoveEditor` command:

``` javascript
// TinyMCE 4 API
tinymce.execCommand('mceRemoveEditor', true, 'wysiwygTextarea');
// TinyMCE 3 API
tinymce.execCommand('mceRemoveControl', true, 'wysiwygTextarea');
```

### Leverage TinyMCE 4.x API

Although the 3.x API is available using a polyfill TinyMCE plugin, we encourage you to use the newer API once v4 becomes the default. 
See [www.tinymce.com/docs](https://www.tinymce.com/docs/). 

## Testing with earlier Confluence versions

This information is only required for Confluence 6.14.0-m50 and earlier. 

### Manual testing

To test your app, spin up a normal Confluence instance (6.8 to 6.13), go to `<confluence-url>/admin/darkfeatures.action`, and add the dark feature `frontend.editor.v4`. Then, head to the editor and check that your app is working as expected.

### Automated tests

For any automated tests, you can spin up Confluence with the V4 editor enabled by adding the following to the JVM args used to start Confluence: `-Datlassian.darkfeature.frontend.editor.v4=true`. For example:

``` java
<configuration>
    ...
    <jvmArgs>-Datlassian.darkfeature.frontend.editor.v4=true</jvmArgs>
</configuration>
```

With automated tests, we recommend continuing to run without the dark feature enabled, and add a copy of the build with the feature enabled so that you can test both versions until v4 becomes the default.

## Reporting issues

If you encounter issues with your app in TinyMCE v4, please help us by doing some investigation into what is causing your app to break, then raise an [EAP feedback ticket](https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=10470&issuetype=10000&components=43692&summary=Confluence+6.8++Editor+EAP+Feedback). You'll need to be logged in to [jira.atlassian.com](http://jira.atlassian.com) for this link to work. 

