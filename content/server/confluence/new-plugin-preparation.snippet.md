---
aliases:
- /server/confluence/new-plugin-preparation-39368902.html
- /server/confluence/new-plugin-preparation-39368902.md
category: devguide
confluence_id: 39368902
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368902
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368902
date: '2017-12-08'
legacy_title: New Plugin Preparation
platform: server
product: confluence
subcategory: other
title: New Plugin preparation
---
# New Plugin preparation

## Conventions

### Working Directory

For all of the tutorials, the working directory is called *atlas-development *and is located in the users home directory.

### Development Deployment Paradigm

The tutorials use QuickReload and expect to produce transformerless plugins.

### POM.xml

In the tutorials, there is a parent POM.xml file in the working directory.  The POM.xml file in the plugin directory has been configured to inherit from the POM.xml and all redundant entries have been removed.  Typically, only the artifact version, name, description and build instructions are included. 

### Development Environment

In order to keep things simple, the development has been done in a plain text editor, gvim.  Many find the [Eclipse IDE](/server/confluence/conventions.snippet) a more productive environment. 

## Open a Command Shell

The Atlassian SDK provides several tools that are designed to be launched from a command shell.  Using the table below which shows where in the application launcher for the various operating systems the command shell launcher is, launch the command shell

|         |                                                                 |
|---------|-----------------------------------------------------------------|
| Windows | Start -&gt; All Programs -&gt; Accessories -&gt; Command Prompt |
| OS X    | Applications -&gt; Utilities -&gt; Terminal                     |
| Linux   | Applications Menu -&gt; Accessories -&gt; Terminal              |

## Move to the Working Directory

 

1.  Open a terminal window and perform the following steps:
2.  Create the working directory for plugin development unless it already exists

    {{% note %}}

    While a working directory is not strictly necessary, it's a very good practice for reasons described later

    {{% /note %}}

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td>Windows</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:&gt;</span>mkdir <span class="st">&quot;%HOMEDRIVE%%HOMEPATH%</span><span class="sc">\a</span><span class="st">tlas-development&quot;</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    <tr class="even">
    <td>Linux/Mac</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>$ mkdir <span class="op">~</span><span class="ss">/atlas-development</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    </tbody>
    </table>

    \* *

3.  Change into the Directory

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td>Windows</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:&gt;</span>cd <span class="st">&quot;%HOMEDRIVE%%HOMEPATH%</span><span class="sc">\a</span><span class="st">tlas-development&quot;</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    <tr class="even">
    <td>Linux/Mac </td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>$ cd <span class="op">~</span><span class="ss">/atlas-development</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    </tbody>
    </table>
