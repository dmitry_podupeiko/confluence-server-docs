---
aliases:
- /server/confluence/changes-to-the-remote-api-in-confluence-4.0-5669774.html
- /server/confluence/changes-to-the-remote-api-in-confluence-4.0-5669774.md
category: devguide
confluence_id: 5669774
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=5669774
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=5669774
date: '2017-12-08'
legacy_title: Changes to the remote API in Confluence 4.0
platform: server
product: confluence
subcategory: updates
title: Changes to the remote API in Confluence 4.0
---
# Changes to the remote API in Confluence 4.0

{{% note %}}

Please read this page if you have a client that uses the remote API to retrive pages, blogs or comments from Confluence. You will need to update your client work correctly with Confluence 4.0.

{{% /note %}}

### Change to the SOAP and XML-RPC API in Confluence 4.0

Confluence 4.0 introduces changes to the way page content is stored in Confluence. Page content is no longer stored as wiki markup, but is now stored in an XHTML based storage format.

Because of these changes to the way pages are stored Confluence 4.0 also introduces a new version 2 remote API. The new version 2 API implements all the same methods as the version 1 API, however all content is stored and retrieved using the new storage format. This means that you cannot, for example, create a page using wiki markup with the version 2 API, you must instead define the page using the storage format.

The version 1 remote API will continue to work with Confluence 4.0. You will be able to create pages, blogs and comments in wiki markup using the version 1 API. However you will no longer be able to retrive pages, blogs and comments using the version 1 API. Specifically the following methods will no longer work with the version 1 API:

-   `getBlogEntryByDayAndTitle(String token, String spaceKey, int dayOfMonth, String postTitle)`
-   `getBlogEntryByDateAndTitle(String token, String spaceKey, int year, int month, int dayOfMonth, String postTitle)`
-   `getBlogEntry(String token, long entryId)`
-   `getPage(String token, String spaceKey, String pageTitle)`
-   `getPage(String token, long pageId)`
-   `getComments(String token, long pageId)`
-   `getComment(String token, long commentId)`

This means that if you have a client that uses these methods you will need to update it to use the new version 2 API in order to work with Confluence 4.0. You can use both APIs in a client, creating content in wiki markup using the version 1 API and retrieving it in the storage format using the version 2 API. This may be useful stepping stone for clients to move towards the version 2 API. You should note that there is no longer any way to retrive content from Confluence as wiki markup.

To aid the migration from the version 1 API to the version 2 API a new method convertWikiToStorageFormat(String token, String markup) has been added to both versions of the API.

### Using the version 2 remote API

For SOAP the new V2 WSDL can be found at:

`http://<confluence-install>/rpc/soap-axis/confluenceservice-v2?wsdl`

For XML-RPC you can use the V2 API by adding the prefix `confluence2.` to the call instead of the old `confluence1.`
