---
aliases:
- /server/confluence/confluence-internals-2031652.html
- /server/confluence/confluence-internals-2031652.md
category: devguide
confluence_id: 2031652
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031652
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031652
date: '2017-12-08'
guides: guides
legacy_title: Confluence Internals
platform: server
product: confluence
subcategory: learning
title: Confluence internals
---
# Confluence internals

Confluence is a large and complex application. This area documents some of the more complicated aspects of its design. For a complete reference, please refer to the source code which is available for download with all commercial licenses.

-   [Bandana Caching](/server/confluence/bandana-caching)
-   [Confluence Bootstrap Process](/server/confluence/confluence-bootstrap-process)
-   [Confluence Caching Architecture](/server/confluence/confluence-caching-architecture)
-   [Confluence Internals History](/server/confluence/confluence-internals-history)
-   [Confluence Journal Service](/server/confluence/confluence-journal-service)
-   [Confluence Macro Manager](/server/confluence/confluence-macro-manager)
-   [Confluence Permissions Architecture](/server/confluence/confluence-permissions-architecture)
-   [Confluence Services](/server/confluence/confluence-services)
-   [Confluence UI architecture](/server/confluence/confluence-ui-architecture)
-   [Custom User Directories in Confluence](/server/confluence/custom-user-directories-in-confluence)
-   [Date formatting with time zones](/server/confluence/date-formatting-with-time-zones)
-   [HTML to Markup Conversion for the Rich Text Editor](/server/confluence/html-to-markup-conversion-for-the-rich-text-editor)
-   [HTTP authentication with Seraph](/server/confluence/http-authentication-with-seraph)
-   [I18N Architecture](/server/confluence/i18n-architecture)
-   [Page Tree API Documentation](/server/confluence/page-tree-api-documentation)
-   [Password Hash Algorithm](/server/confluence/password-hash-algorithm)
-   [Persistence in Confluence](/server/confluence/persistence-in-confluence)
-   [Spring IoC in Confluence](/server/confluence/spring-ioc-in-confluence)
-   [Velocity Template Overview](/server/confluence/velocity-template-overview)
