---
aliases:
- /server/confluence/how-to-switch-to-non-minified-javascript-for-debugging-2031862.html
- /server/confluence/how-to-switch-to-non-minified-javascript-for-debugging-2031862.md
category: devguide
confluence_id: 2031862
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031862
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031862
date: '2018-03-29'
legacy_title: How to switch to non-minified Javascript for debugging
platform: server
product: confluence
subcategory: faq
title: How to switch to non-minified JavaScript for debugging
---
# How to switch to non-minified JavaScript for debugging

Although you should always serve minified JavaScript, temporarily running Confluence with non-minified JavaScript
can be useful for debugging.

There are a few parameters that can be passed to AMPS plugin's configuration:

* `compressResources` – this parameter enables minification of all resources if set to true, default value is true.
* `compressJs` – this parameter enables minification of only JS files if set to true, default value is true.
* `compressCss` – this parameter enables minification of only CSS files if set to true, default value is true.

In your `pom.xml` file, you can create a `compressJs` property that defaults to `true` and then override it with command line.
For example:

``` xml
    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-confluence-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <compressJs>${compressJs}</compressJs>
                </configuration>
            ...
            </plugin>
        </plugins>
    </build>
    <properties>
        <compressJs>true</compressJs>
    </properties>             
```

``` bash
    atlas-package -DcompressJs=false
```

When you finish debugging, you can simply omit `compressJs` parameter.
