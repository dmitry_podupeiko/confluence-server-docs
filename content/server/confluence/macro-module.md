---
aliases:
- /server/confluence/macro-module-2031840.html
- /server/confluence/macro-module-2031840.md
category: reference
confluence_id: 2031840
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031840
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031840
date: '2018-03-28'
legacy_title: Macro Module
platform: server
product: confluence
subcategory: modules
title: Macro Module
---
# Macro module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 4.0 and later</p></td>
</tbody>
</table>

{{% note %}}
Starting from [Confluence 4.0](/server/confluence/preparing-for-confluence-4-0), macro module was replaced with xhtml-macro module.
To function correctly in Confluence, all macros must contain metadata.
{{% /note %}}

Macros are Confluence code that can be invoked from inside a page by putting the name of the macro in curly brackets.
Users of Confluence are familiar with macros like {color}, {children}, or {rss}. Thanks to the plugin system,
it is easy to write and install new macros into a Confluence server.

## Helpful links to master macro

* Created a new macro or looking for macros?  
Share your macros and find new plugins on the <a href="http://marketplace.atlassian.com" class="external-link">The Atlassian Marketplace</a>.  
* Need a simple macro? Consider a User Macro.
If you want to create a macro that just inserts some boilerplate text or performs simple formatting,
you may only need a [User Macro](/server/confluence/user-macro-module/). User macros can be written entirely from
within the Confluence web interface, and they don't require special installation or programming knowledge.  
* Make your macro work in the Macro Browser.
Make your macro [look good](/server/confluence/including-information-in-your-macro-for-the-macro-browser) in the macro browser.
* Update your macro for Confluence **Mobile web**.
See the tutorial for [Making your macro render in Confluence mobile](/server/confluence/making-your-macro-render-in-confluence-mobile).

## Adding a macro plugin

Macros are a kind of Confluence plugin modules.

*   For more information about plugins in general, read [Confluence plugin guide](/server/confluence/confluence-plugin-guide).
*   For an introduction to writing your own plugins, read [Writing Confluence plugins](/server/confluence/writing-confluence-plugins).

### The Macro plugin module

Each macro is a plugin module of "xhtml-macro" type. It is packaged with whatever Java classes and other resources (for example, Velocity templates)
that the macro requires to run. For easier management, similar macros are usually packaged together into a single plugin.
Here is an example of `atlassian-plugin.xml` file:

``` xml
<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
	<plugin-info>
		<description>${project.description}</description>
		<version>${project.version}</version>
		<vendor name="${project.organization.name}" url="${project.organization.url}" />
		<param name="plugin-icon">images/pluginIcon.png</param>
		<param name="plugin-logo">images/pluginLogo.png</param>
	</plugin-info>

    <xhtml-macro name="example-macro" key="example-macro" class="path/to/your/macro/class"
		   icon="/download/resources/${atlassian.plugin.key}/path/to/your/icon">
		<category name="formatting"/>
		<parameters>
		<!-- your parameters -->
		</parameters>
	</xhtml-macro>

    <!-- more macros... -->
</atlassian-plugin>
```

The **name** of the macro defines how it will be referenced from the page. So, if you define your macro as having `name="tasklist"`,
 the macro will be called from the page as {tasklist}.

The **class** attribute of the macro defines what Java class is used to process that macro.
This is the class you need to write for the macro to function. It must implement the `com.atlassian.confluence.macro.Macro` interface.

The [Including information in your macro for the Macro Browser](/server/confluence/including-information-in-your-macro-for-the-macro-browser/)
page describes available `<xhtml-macro>` elements.

### Creating a very basic plugin

Make sure you have created your first macro plugin using  
[Create a Confluence ‘Hello World’ macro](/server/framework/atlassian-sdk/create-a-confluence-hello-world-macro/) tutorial.
That will save you a lot of time. You can find a more complete guide to writing macros on [Writing macros](/server/confluence/writing-macros) page.

## Using a Velocity template

To use a Velocity template to provide the output of your macro, see page about
[Rendering Velocity templates in a macro](/server/confluence/rendering-velocity-templates-in-a-macro).

## Examples of macro plugins

You can find the source code of a number of macros in the `plugins` directory of your Confluence distribution.
Some of macros are already built and packaged with Confluence. You can modify these macros, but make sure you’re
consistent with the Confluence license.
If you want to write your own macros, the most interesting ones to read are the following:

*   `tasklist` — a simple macro that stores its state in a page `PropertySet`.
*   `userlister` — a macro that works in combination with an [event listener](/server/confluence/event-listener-module) to list logged-in users.
*   `livesearch` — a macro that leverages JavaScript and XMLHttpRequest in combination with an [XWork plugin](/server/confluence/xwork-webwork-module) to handle the server-side interaction.
*   `graphviz` — a macro that interacts with an external, non-Java tool.

### Related topics

*   [Creating a Confluence 'Hello world' macro](/server/framework/atlassian-sdk/create-a-confluence-hello-world-macro/).
*   [Creating a Wikipedia macro](/server/confluence/creating-a-wikipedia-macro)
*   [Plugin tutorial - Writing macros for Confluence pre-4.0](/server/confluence/writing-macros-for-pre-4-0-versions-of-confluence).
*   [Including information in your macro for the Macro Browser](/server/confluence/including-information-in-your-macro-for-the-macro-browser).
*   [Preventing XSS issues with macros in Confluence 4.0](/server/confluence/preventing-xss-issues-with-macros-in-confluence-4-0).
*   [Writing macros](/server/confluence/writing-macros).
