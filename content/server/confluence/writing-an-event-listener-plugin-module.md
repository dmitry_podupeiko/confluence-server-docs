---
aliases:
- /server/confluence/writing-an-event-listener-plugin-module-2031813.html
- /server/confluence/writing-an-event-listener-plugin-module-2031813.md
category: reference
confluence_id: 2031813
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031813
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031813
date: '2018-03-23'
legacy_title: Writing an Event Listener Plugin Module
platform: server
product: confluence
subcategory: modules
title: Writing an Event Listener Plugin module
---
# Writing an Event Listener plugin module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 1.4 and later</p></td>
</tr>
<tr class="even">
<td><p><strong>Legacy:</strong></p></td>
<td><p>In Confluence 3.3 and later, the preferred approach is to use annotation-based events with Atlassian Spring Scanner.  
More information and examples are on <a href="/server/confluence/event-listener-module">Event Listener Module</a> page.</p></td>
</tr>
</tbody>
</table>

## Overview

For an introduction to event listener plugin modules, read [Event Listener module](/server/confluence/event-listener-module).

## Step 1. Identify the events you wish to listen for

The easiest thing here is to consult the <a href="https://docs.atlassian.com/atlassian-confluence/latest-server/" class="external-link">Javadoc</a>,
in the **com.atlassian.confluence.event.events** package. When you implement an `EventListener` you will provide an array of
Class objects which represent the events you wish to handle.

The naming of most events are self explanatory (for example, `GlobalSettingsChangedEvent` or `ReindexStartedEvent`).
However, there are some that need further clarification.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Event Class</p></th>
<th><p>Published</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/event/events/label/LabelCreateEvent.html" class="external-link">LabelCreateEvent</a></p></td>
<td><p>Thrown when a label is created and comes into existence.</p></td>
</tr>
<tr class="even">
<td><p><a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/event/events/label/LabelRemoveEvent.html" class="external-link">LabelRemoveEvent</a></p></td>
<td><p>Thrown when a label is disassociated from a piece of content (as opposed to itself being removed).</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/event/events/label/LabelAddEvent.html" class="external-link">LabelAddEvent</a></p></td>
<td><p>Thrown when an existing label is associated with a piece of content (as opposed to being newly created).</p></td>
</tr>
<tr class="even">
<td><p><a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/event/events/label/LabelDeleteEvent.html" class="external-link">LabelDeleteEvent</a></p></td>
<td><p>Thrown when a label is removed (as opposed to being disassociated from a piece of content).</p></td>
</tr>
</tbody>
</table>

## Step 2. Create the `EventListener`

The `EventListener` interface defines two methods that must be implemented: `getHandledEventClasses()` and `handleEvent()`.

1.  Implement `getHandledEventClasses()`

    The `getHandledEventClasses()` method holds an array of class objects representing the events you wish to listen for.

    *   Your listener will *only* receive events of the types specified in `getHandledEventClasses()`.
    *   You must specify all the event types you need — specifying a superclass will *not* include its subclasses.
    *   Returning an empty array will cause your listener to receive every event Confluence produces.

    So, if you want your listener to receive only `SpaceCreatedEvent` and `SpaceRemovedEvent`, do the following:

    ``` java
        private static final Class[] HANDLED_EVENTS = new Class[] {
            SpaceCreateEvent.class, SpaceRemovedEvent.class
        };

        public Class[] getHandledEventClasses()
        {
            return HANDLED_EVENTS;
        }
    ```

    Alternatively, to receive all possible events, use the following code:

    ``` java
        /**
         * Returns an empty array, thereby handling every ConfluenceEvent
         * @return
         */
        public Class[] getHandledEventClasses()
        {
            return new Class[0];
        }
    ```

1.  Implement `handleEvent()`

    The following implementation simply relies upon the `toString()` implementation of the event and logs it to a log4j appender.

    ``` java
        public void handleEvent(Event event)
        {
            if (!initialized)
               initializeLogger();

            log.info(event);
        }
    ```

    Most often, a `handleEvent(..)` method will type check each event sent through it and execute some conditional logic.

    ``` java
        public void handleEvent(Event event)
        {
            if (event instanceof LoginEvent)
            {
                LoginEvent loginEvent = (LoginEvent) event;
                // ... logic associated with the LoginEvent
            }
            else if (event instanceof LogoutEvent)
            {
                LogoutEvent logoutEvent = (LogoutEvent) event;
                // ... logic associated with the LogoutEvent
            }
        }
    ```

You can find a full example of an `EventListener` class that listens for login and logout events in [EventListener example](/server/confluence/descriptor-based-event-listener).

## Step 3. Add the `EventListener` as a module to your plugin

1.  To add the `EventListener` as a module, create an `atlassian-plugin.xml` file.

The `atlassian-plugin.xml` file has been described [here](/server/framework/atlassian-sdk/atlassian-plugin-xml-element-reference/) in detail.
This is an example of a listener plugin module included in an `atlassian-plugin.xml` file.

``` xml
<atlassian-plugin name='Optional Listeners' key='confluence.extra.auditor'>
    <plugin-info>
        <description>Audit Logging</description>
        <vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
        <version>1.0</version>
    </plugin-info>

    <listener name='Audit Log Listener' class='com.atlassian.confluence.extra.auditer.AuditListener' key='auditListener'>
        <description>Provides an audit log for each event within Confluence.</description>
    </listener>
</atlassian-plugin>
```
