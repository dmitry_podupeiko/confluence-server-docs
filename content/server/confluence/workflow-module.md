---
aliases:
- /server/confluence/workflow-module-2031632.html
- /server/confluence/workflow-module-2031632.md
category: reference
confluence_id: 2031632
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031632
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031632
date: '2017-12-08'
legacy_title: Workflow Module
platform: server
product: confluence
subcategory: modules
title: Workflow module
---
# Workflow module

This set of pages describes the Workflow Plugin. This is a work in progress and is useful as:

-   An example of a reasonably complicated plugin, using macros, events and xwork actions which stores state as page properties and interacts with content entity versions and permissions.
-   A starting point for discussion of what **plugin-based** workflow in Confluence might look like. A workflow implementation which made core Confluence changes might look different.

Here's a [description of the workflow model](/server/confluence/workflow-plugin-prototype) implemented by the plugin.

{{% warning %}}

The workflow plugin as released in 1.4.2 does not have all the features described. It will be updated in the first 1.5DP release.

{{% /warning %}}

We're interested in getting feedback - how useful does the workflow model as described seem to you?

