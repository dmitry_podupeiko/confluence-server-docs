---
aliases:
- /server/confluence/rewrites-for-review-33723224.html
- /server/confluence/rewrites-for-review-33723224.md
category: devguide
confluence_id: 33723224
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33723224
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33723224
date: '2017-12-08'
legacy_title: Rewrites for Review
platform: server
product: confluence
subcategory: other
title: Rewrites for Review
---
# Rewrites for Review

