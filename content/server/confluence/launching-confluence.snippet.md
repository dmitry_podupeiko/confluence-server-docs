---
aliases:
- /server/confluence/launching-confluence-39368878.html
- /server/confluence/launching-confluence-39368878.md
category: devguide
confluence_id: 39368878
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368878
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368878
date: '2017-12-08'
legacy_title: Launching Confluence
platform: server
product: confluence
subcategory: other
title: Launching Confluence
---
# Launching Confluence

{{% note %}}

Confluence may be launched from the plugin's folder or from a sibling plugin's folder if there is a parent POM.xml file. The recommended approach is to have a 'dummy' plugin named by the version of Confluece which gets instantiated.

{{% /note %}}

1.  Open a new command shell (so you can still execute commands in your existing shell)
2.  Change to the root directory of the plugin from which the development instance of Confluence will be launched.  
    *Note, the root directory of the plugin will contain the file "pom.xml" *

    |         |                                                          |
    |---------|----------------------------------------------------------|
    | Windows | c:&gt;cd "%HOMEDIR%%HOMEPATH%\\atlas-development\\5-9-4" |
    | Mac     | $ cd ~/atlas-development/5-9-4                           |

3.  Launch confluence

    ``` javascript
    $ atlas-run
    ```

    By default, the SDK instance of Confluence will be launched with the Developer Mode turned on. While this adds some extra debugging features, it slows Confluence down. If not needed, it can be turned off using the command: `atlas-run -Datlassian.dev.mode=false`

    [Read more about the Confluence Developer Mode](https://developer.atlassian.com/confdev/development-resources/confluence-developer-faq/enabling-developer-mode)

    {{% note %}}

    It can take quite some time for Confluence to get up and running. The very first time Confluence is launched through the SDK, it will download all of the necessary components. The second time, it will reuse what it downloaded early - but it still takes a while for Confluence to get up and running.

    When Confluence is ready, you will see the text:

    ``` javascript
    [INFO] confluence started successfully in XXXs at http://<myserver>:1990/confluence
    [INFO] Type Ctrl-D to shutdown gracefully
    [INFO] Type Ctrl-C to exit
    ```

    You may then open the url printed above (http://&lt;myserveer&gt;:1990/confluence) and login. The default administrator credentials are admin/admin.

    {{% /note %}}
