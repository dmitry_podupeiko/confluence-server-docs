---
category: devguide
date: '2019-01-15'
platform: server
product: confluence
subcategory: learning
guides: development resources
title: Polyfills in Confluence Server
---
# Polyfills in Confluence Server

We bundle a small number of polyfills in order to support particular features. The following matrix lists all polyfills used in each Confluence version.

<table>
    <thead>
  <tr>
    <th>Confluence version</th>
    <th>Polyfills</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td>
        6.1 to 6.6
        </td>
        <td>
        <a href="https://github.com/stefanpenner/es6-promise/tree/v3.0.2">ES6-promise</a>@3.0.2 <br>
        <a href="https://github.com/webcomponents/webcomponentsjs/blob/v0.7.5/MutationObserver.js">webcomponentsjs/MutationObserver.js</a>@0.7.5
        </td>
      </tr>
      <tr>
        <td>6.7 to 6.12</td>
        <td>
        <a href="https://github.com/stefanpenner/es6-promise/tree/v3.0.2">ES6-promise</a>@3.0.2 <br>
        <a href="https://github.com/webcomponents/webcomponentsjs/blob/v0.7.5/MutationObserver.js">webcomponentsjs/MutationObserver.js</a>@0.7.5 <br>
        <a href="https://github.com/babel/babel/tree/v6.26.0/packages/babel-polyfill">babel-polyfill</a>@6.26.0<br>
        </td>
      </tr>
      <tr>
        <td>6.13</td>
        <td>
        <a href="https://github.com/webcomponents/webcomponentsjs/blob/v0.7.5/MutationObserver.js">webcomponentsjs/MutationObserver.js</a>@0.7.5 <br>
        <a href="https://github.com/babel/babel/tree/v6.26.0/packages/babel-polyfill">babel-polyfill</a>@6.26.0<br>
        <a href="https://github.com/whatwg/fetch">whatwg/fetch</a>@2.0.4 <br>
        <a href="https://github.com/mo/abortcontroller-polyfill/tree/v1.1.9">abortcontroller-polyfill</a>@1.1.9 (patch-fetch only)
        </td>
      </tr>
      <tr>
        <td>6.14 and later</td>
        <td>
        <a href="https://github.com/babel/babel/tree/master/packages/babel-polyfill">babel-polyfill</a>@7.2.5<br>
          <a href="https://github.com/whatwg/fetch">whatwg/fetch</a>@2.0.4 <br>
        <a href="https://github.com/mo/abortcontroller-polyfill/tree/v1.1.9">abortcontroller-polyfill</a>@1.1.9 (patch-fetch only)
        </td>
      </tr>
  </tbody>
</table>

Note: there are some known issues with the Babel polyfill in Confluence 6.6 and earlier, see [Babel Polyfill in Confluence Server](/server/confluence/babel-polyfill-in-confluence-server/) for more information.
