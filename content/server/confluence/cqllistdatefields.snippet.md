---
aliases:
- /server/confluence/-cqllistdatefields-29952565.html
- /server/confluence/-cqllistdatefields-29952565.md
category: devguide
confluence_id: 29952565
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29952565
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29952565
date: '2017-12-08'
legacy_title: _CQLListDateFields
platform: server
product: confluence
subcategory: other
title: _CQLListDateFields
---
# \_CQLListDateFields

-   [Created](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-createdCreatedCreated)
-   [Lastmodified](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-lastmodifiedLastModifiedLastModified)
