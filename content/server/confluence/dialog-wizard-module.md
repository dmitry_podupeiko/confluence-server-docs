---
aliases:
- /server/confluence/dialog-wizard-module-24084734.html
- /server/confluence/dialog-wizard-module-24084734.md
category: reference
confluence_id: 24084734
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24084734
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24084734
date: '2017-12-08'
legacy_title: Dialog Wizard Module
platform: server
product: confluence
subcategory: modules
title: Dialog Wizard module
---
# Dialog Wizard module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>Confluence 5.2 and later</p></td>
</tr>
</tbody>
</table>

## Purpose of this module

Currently you can only use this module with a [Space Blueprint](/server/confluence/space-blueprint-module) or [Blueprint](/server/confluence/blueprint-module) definition. It allows you to easily create a wizard for users to input data in for your blueprint. Note that you must provide your wizard in the form of soy templates. 

## Configuration

The root element for the Dialog Wizard module is `dialog-wizard`. 

#### Element: `dialog-wizard`

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
Attributes
</div></th>
<th><div class="tablesorter-header-inner">
Description
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>key</code></p></td>
<td><p>A unique identifier of the dialog wizard.</p>
<p><strong>Required.</strong></p></td>
</tr>
</tbody>
</table>

The `dialog-wizard` element must have at least one `dialog-page` element.

#### Element:` dialog-page`

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
Attributes*
</div></th>
<th><div class="tablesorter-header-inner">
Description
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>id</code></p></td>
<td><p>A unique identifier of the dialog page</p>
<p><strong>Default:</strong> None</p></td>
</tr>
<tr class="even">
<td><code>template-key</code></td>
<td>A reference to the soy template for the dialog page to be rendered</td>
</tr>
<tr class="odd">
<td><p><code>title-key</code></p></td>
<td>An i18n key for the dialog page title</td>
</tr>
<tr class="even">
<td><p><code>description-header-key</code></p></td>
<td>An i18n key for the title of the description section on the dialog page</td>
</tr>
<tr class="odd">
<td><p><code>description-content-key</code></p></td>
<td>An i18n key for the text of the description section on the dialog page</td>
</tr>
<tr class="even">
<td><p><code>last</code></p></td>
<td>A Boolean that should have the value 'true' if it is the last dialog wizard page</td>
</tr>
</tbody>
</table>

**\*id, template-key, and title-key are required**

Note the description section refers to the right side panel, which should explain more about the blueprint. 

## Example

The following example shows a Space Blueprint with a one page wizard defined. It will render a soy template provided already by Confluence for a create space form.

``` xml
<space-blueprint key="my-space-blueprint" i18n-name-key="confluence.hello.space.blueprint.name">       
     <dialog-wizard key="my-space-blueprint-wizard">
            <dialog-page id="spaceBasicDetailsId"
                         template-key="Confluence.Templates.Blueprints.CreateSpace.createSpaceForm"
                         title-key="confluence.hello.blueprint.dialog.choose.title"
                         description-header-key="confluence.hello.blueprint.dialog.choose.heading"
                         description-content-key="confluence.hello.blueprint.dialog.choose.description"/>
        </dialog-wizard>
...
</space-blueprint>
```
