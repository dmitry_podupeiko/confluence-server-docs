---
aliases:
- /server/confluence/http-authentication-with-seraph-2031684.html
- /server/confluence/http-authentication-with-seraph-2031684.md
category: devguide
confluence_id: 2031684
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031684
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031684
date: '2017-12-08'
guides: guides
legacy_title: HTTP authentication with Seraph
platform: server
product: confluence
subcategory: learning
title: HTTP authentication with Seraph
---
# HTTP authentication with Seraph

### Introduction

This document describes how the default security system in Confluence works, using the <a href="http://opensource.atlassian.com/seraph/" class="external-link">Seraph</a> library for HTTP authentication.

Extending the security system by subclassing Seraph's authenticator and configuring the `seraph-config.xml` file is outside the scope of this document. <a href="https://confluence.atlassian.com/display/DEV/Single+Sign-on+Integration+with+JIRA+and+Confluence" class="external-link">Single Sign-on Integration with JIRA and Confluence</a> explains one way to integrate Seraph with Atlassian products.

### Flowchart diagrams

The easiest way to understand Confluence's authentication process is with the following diagrams.

<img src="/server/confluence/images/confluenceauthentication.png" class="confluence-thumbnail" />  
*Authentication flowchart*

Because the `Authenticator.login(request, response, username, password, rememberMe)` method occurs three times, and is slightly complex, it has been broken into its own sub-flowchart.

<img src="/server/confluence/images/confluencelogin.png" class="confluence-thumbnail" />  
*Login method flowchart*

### Supported authentication methods

The default Seraph authenticator supports four methods of authentication, as can be seen in the flowchart:

-   request parameters: os\_username and os\_password
-   session attribute storing the logged-in user
-   cookie storing username and password ('remember me' login)
-   HTTP basic authentication via standard headers.

Each method is tried in the order above. A successful login at an earlier method continues without checking the later methods. Failure at one method means continuing with the later methods until all are exhausted. At this point, the user is considered an anonymous user, and treated according to the permissions of an anonymous user in Confluence.

Looking through the source code will show that Seraph supports role-based authentication, but this is only used in Confluence for the `/admin/` URL restriction.

### Related pages

[Confluence Internals](/server/confluence/confluence-internals) <br>
[Confluence Internals](/server/confluence/confluence-internals) <br>
<a href="https://confluence.atlassian.com/display/AOD/Managing+Confluence+Users" class="external-link">Managing Confluence Users</a>
