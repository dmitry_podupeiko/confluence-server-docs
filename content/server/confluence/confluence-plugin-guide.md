---
aliases:
- /server/confluence/confluence-plugin-guide-2031748.html
- /server/confluence/confluence-plugin-guide-2031748.md
category: devguide
confluence_id: 2031748
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031748
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031748
date: '2018-10-02'
legacy_title: Confluence Plugin Guide
platform: server
product: confluence
subcategory: intro
title: Confluence plugin guide
---
# Confluence plugin guide

Confluence's plugin system allows you to build apps that  customize and extend Confluence. An app (also known as a plugin or add-on) is a bundle of code, resources and a special configuration file that can be installed on a Confluence site to add new functionality, or change the behavior of existing features.

There are two main types of apps:

- **system apps** which are bundled with Confluence and provide core Confluence functions.
- **custom and third-party apps** which are developed specifically for your Confluence site or available from the [Atlassian Marketplace](https://marketplace.atlassian.com/). These can be free or paid. [Learn how to make your app available on Marketplace](/platform/marketplace/)

The terms app, add-on, and plugin are often used interchangeably. Throughout this guide, we'll mostly use the term 'plugin'. 

In this guide:

- [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins/)
- [Confluence Plugin Module Types](/server/confluence/confluence-plugin-module-types/)
- [Confluence Blueprints](/server/confluence/confluence-blueprints/)

{{% note %}}

Building for a cloud site?

Head over to our [Confluence Cloud docs](/cloud/confluence/) to build for Confluence instances hosted by Atlassian in the cloud.

{{% /note %}}

## Plugins and plugin modules

<img src="/server/confluence/images/plugin-structure.png" class="image-right" />

Every plugin is made up of one or more *plugin modules*. A single plugin may do many things, while a plugin module represents a single function of the plugin.

For example, a theme plugin will consist of a colour-scheme module to define the theme's colors, a number of layout modules to define the site's page layouts, and a theme module to combine those pieces together into a single theme.

Some plugins, such as the macro packs that come with Confluence, are just a collection of unrelated modules that just happen to be packaged together. Other plugins, such as theme plugins, have modules that work together to provide some orchestrated functionality.

## Where plugins are stored

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Category</p></th>
<th><p>Storage</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Manually installed</p></td>
<td><p>database</p></td>
</tr>
<tr class="even">
<td><p>Installed via repository</p></td>
<td><p>database</p></td>
</tr>
<tr class="odd">
<td><p>Bundled plugins</p></td>
<td><p><code>conf-home</code></p></td>
</tr>
<tr class="even">
<td><p>System plugins</p></td>
<td><p><code>WEB-INF/lib</code></p></td>
</tr>
</tbody>
</table>

For example, the System plugins `chart` plugin or the `Widget Connector` plugin will store data in `WEB-INF/lib`. Similarly for advanced-formatting macros.

## Where plugin run-time data is stored

There is no distinct requirement where a Confluence plugin's run-time data is stored. It dependeds on the particular implementation of each plugin. The most common storage location would be: database, BANDANA, `conf-home` or other.

## Developing a plugin

If you're interested in developing an app for Confluence, our [plugin SDK](https://developer.atlassian.com/server/framework/atlassian-sdk/) is a great place to start. 