---
aliases:
- /server/confluence/adding-new-confluence-to-hipchat-notifications-33737846.html
- /server/confluence/adding-new-confluence-to-hipchat-notifications-33737846.md
category: devguide
confluence_id: 33737846
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33737846
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33737846
date: '2017-12-08'
legacy_title: Adding new Confluence to HipChat notifications
platform: server
product: confluence
subcategory: learning
title: Adding new Confluence to HipChat notifications
---
# Adding new Confluence to HipChat notifications

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to <strong>Confluence 5.8 or higher</strong></p>
<p>(or <strong>Confluence 5.5 and higher</strong>, if the <strong>HipChat for Confluence</strong> plugin (<strong>6.14.0 or greater</strong>) is installed)</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>This is an intermediate tutorial. You should have completed at least one beginner tutorial before working through this tutorial.</p></td>
</tr>
<tr class="odd">
<td><p>Time estimate:</p></td>
<td><p>It should take you approximately 1 hour to complete this tutorial.</p></td>
</tr>
</tbody>
</table>

## Overview

This tutorial shows you how to add support for a new HipChat space to room notification in Confluence.  In this tutorial we'll add a new notification type, which sends a notification to a HipChat room when a comment is created. 

{{% tip %}}

If you're already familiar with the Atlassian SDK and just want to learn how to use the add new HipChat space to room notifications, go straight to [Step 3. Create a new HipChat notification type](#step-3-create-a-new-hipchat-notification-type).

{{% /tip %}}

### Prerequisite knowledge

To complete this tutorial, you must already understand:

-   The basics of Java development: classes, interfaces, methods, how to use the compiler, and so on. 
-   How to create an Atlassian plugin project using the [Atlassian Plugin SDK](/server/confluence/adding-new-confluence-to-hipchat-notifications).

### Plugin source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you are done, you can find the plugin source code on Atlassian Bitbucket. To clone the repository, use the following command:

``` bash
https://bitbucket.org/atlassian/tutorial-confluence-hipchat-notification.git 
```

Alternatively, you can download the source using the **get source** option here: https://bitbucket.org/atlassian/tutorial-confluence-hipchat-notification/downloads

{{% note %}}

About these Instructions

You can use any supported combination of OS and IDE to construct this plugin. These instructions were written using IntelliJ IDEA Ultimate 14.0 on a MacBook Pro running Mac OS X. If you are using another combination, you should use the equivalent operations for your specific environment.

{{% /note %}}

## Step 1. Create the plugin project

In this step, you'll use the `atlas-create` command to generate stub code for your plugin. The `atlas-` commands are part of the Atlassian Plugin SDK, and automate much of the work of plugin development for you.

1.  Open a terminal and navigate to your plugin directory.
2.  Enter the following command to create a Confluence plugin skeleton:

    ``` bash
    atlas-create-confluence-plugin
    ```

    When prompted, enter the following information to identify your plugin:

    <table>
    <colgroup>
    <col style="width: 20%" />
    <col style="width: 80%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence</code></p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>tutorial-confluence-hipchat-notification</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence.hipchat.notification</code></p></td>
    </tr>
    </tbody>
    </table>

3.  Confirm your entries when prompted.
4.  Change to the `tutorial-confluence-hipchat-notification` directory created by the previous step.

## Step 2. Review and tweak the generated stub code

It is a good idea to familiarise yourself with the stub plugin code. In this section, we'll check a version value and tweak a generated stub class. Open your plugin project in your IDE and follow those steps.

### Add plugin metadata to the POM

Add some metadata about your plugin and your company or organization.

1.  Edit the `pom.xml` file in the root folder of your plugin.
2.  Add your company or organisation name and your website to the `<organization> `element:

    ``` xml
    <organization>
        <name>Example Company</name>
        <url>http://www.example.com</url>
    </organization>
    ```

3.  Update the `<description> `element:

    ``` xml
    <description>Allow space administrators to send notifications to HipChat rooms.</description>
    ```

4.  Save the file.

### Verify your Confluence version

When you generated the stub files, the Confluence version you chose was added to your `pom.xml` file (Project Object Model definition file). This file is located at the root of your project and declares the project dependencies. Take a moment and examine the Confluence dependency:

1.  Open the `pom.xml` file.
2.  Scroll to the bottom of the file.
3.  Find the `<properties>` element.  
    This section lists the version of the Confluence version you selected in **Step 1** and also the version of the `atlas-` commands you are running.
4.  Verify that the Confluence version is 5.8 or higher.

    ``` xml
    <properties>
        <confluence.version>5.8.1</confluence.version>
        <confluence.data.version>5.8.1</confluence.data.version>
        <amps.version>5.0.13</amps.version>
    </properties>
    ```

5.  Save the `pom.xml` file.

### Review the generated plugin descriptor

Your stub code contains a plugin descriptor file `atlassian-plugin.xml`. This is an XML file that identifies the plugin to the host application (Confluence) and defines the required plugin functionality. In your IDE, open the descriptor file which is located in your project under `src/main/resources` and you should see something like this:

``` xml
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.artifactId}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>
</atlassian-plugin>
```

## Step 3. Create a new HipChat notification type

First, let's add some dependencies to the `pom.xml` file that are needed to implement a new notification type.

``` xml
<!-- HipChat notification support -->
<dependency>
    <groupId>com.atlassian.hipchat</groupId>
    <artifactId>hipchat-java-api</artifactId>
    <version>${hipchat.notifications.version}</version>
    <scope>provided</scope>
</dependency>
<dependency>
    <groupId>com.atlassian.plugins</groupId>
    <artifactId>base-hipchat-integration-plugin-api</artifactId>
    <version>${hipchat.notifications.version}</version>
    <scope>provided</scope>
</dependency>
<dependency>
    <groupId>com.atlassian.confluence.plugins</groupId>
    <artifactId>confluence-hipchat-integration-plugin</artifactId>
    <version>${hipchat.notifications.version}</version>
    <scope>provided</scope>
</dependency>
```

You will also need to add the `hipchat.notifications.version` property in the properties section of the `pom.xml`.

``` xml
<hipchat.notifications.version>6.23.8</hipchat.notifications.version>
```

Now to define the notification type that we are adding, we'll need to add it to the `atlassian-plugin.xml`:

``` xml
<component-import key="notificationMessageBuilder" interface="com.atlassian.confluence.plugins.hipchat.spacetoroom.api.notifications.NotificationMessageBuilder"/>
<hipchat-notification-type key="test-notification"
                           class="com.example.plugins.tutorial.confluence.hipchat.notification.ExampleNotification"
                           value="tutorial-example"
                           name="Example HipChat notification"
                           i18n-name-key="hipchat.notification.type.comment.added"
                           context="space"/>
```

Note the `component-import `declaration. This is required if you're implementing the default message rendering provided by the API, which we'll be doing in this tutorial.

The `hipchat-notification-type `is what defines a new notification type:

| Attribute    | Purpose                                                                                                                                                                     |
|------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **key**      | Unique id within the plugin.                                                                                                                                                |
| **class**    | The implementation of the notification type, this should extend the `com.atlassian.confluence.plugins.hipchat.spacetoroom .api.notifications.SpaceToRoomNotification` class. |
| **.value**   | This is the unique value representing this notification type.                                                                                                               |
| **name**     | A name for this type.                                                                                                                                                       |
| **i18n-key** | The i18n key in the i18n properties file for the text that will be displayed on the configuration page.                                                                     |
| **context**  | The context where this notification type is used. Currently only **space**is available.                                                                                     |

Here's the code to create a notification type for when a comment is created:

``` java
package com.example.plugins.tutorial.confluence.hipchat.notification;

import com.atlassian.confluence.event.events.content.comment.CommentCreateEvent;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.plugins.hipchat.spacetoroom.api.notifications.SpaceToRoomNotification;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.fugue.Option;
import com.atlassian.hipchat.api.icons.ADGIcon;
import com.atlassian.hipchat.api.icons.Icon;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.user.UserKey;

import static java.lang.String.format;

/**
 * The is an example HipChat space to room notification the notifies a room
 * when a space is updated.
 */
public class ExampleNotification extends SpaceToRoomNotification<CommentCreateEvent> {
    private ApplicationProperties applicationProperties;
    private I18NBeanFactory i18NBeanFactory;

    public ExampleNotification() {
    }

    @Override
    public Icon getIcon(CommentCreateEvent event) {
        return ADGIcon.PAGE;
    }

    @Override
    public Option<UserKey> getUser(CommentCreateEvent event) {
        ConfluenceUser creator = event.getComment().getCreator();

        if (creator != null) {
            return Option.some(creator.getKey());
        }
        return Option.none();
    }

    @Override
    public String getLink(CommentCreateEvent event) {
        Comment comment = event.getComment();
        String text = i18NBeanFactory.getI18NBean().getText("hipchat.notification.comment.link.text");
        return format("<a href=\"%s\"><b>%s</b></a>",
            GeneralUtil.escapeForHtmlAttribute(linkUrl(comment)),
            GeneralUtil.escapeXMLCharacters(text));
    }

    private String linkUrl(Comment comment) {
        return applicationProperties.getBaseUrl(UrlMode.ABSOLUTE) + comment.getUrlPath();
    }

    @Override
    public String getMessageKey(CommentCreateEvent event) {
        return "hipchat.notification.message.comment.added";
    }

    @Override
    public Option<Space> getSpace(CommentCreateEvent event) {
        return Option.some(event.getComment().getSpace());
    }

    @Override
    public Class<CommentCreateEvent> getEventClass() {
        return CommentCreateEvent.class;
    }

    @Override
    public boolean shouldSend(CommentCreateEvent event) {
        return true;
    }

    public void setApplicationProperties(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory) {
        this.i18NBeanFactory = i18NBeanFactory;
    }
}
```

Finally, we need to add the i18n messages to our i18n properties file, not the parameterised format for the `hipchat.notification.message.comment.added` message that is sent to HipChat:

``` java
# The checkbox text in the HipChat configuration
hipchat.notification.type.comment.added=Comment is added

# {0} is the icon ; {1} is the space key, and {2} is the user
hipchat.notification.message.comment.added={0}&nbsp;{1} added by {2}

hipchat.notification.comment.link.text=Comment
```

And that is all there is to it ! Let's run our plugin and create some notifications.

## Step 4. Build, install and run the plugin

Follow these steps to build and install your plugin, so that you can test your code.  

1.  Make sure you have saved all your code changes to this point.
2.  Open a terminal window and navigate to the plugin root folder (where the `pom.xml` file is).
3.  Run the following command:

    ``` bash
    atlas-run
    ```

    This command builds your plugin code, starts a Confluence instance, and installs your plugin. This may take several seconds. When the process is complete, you'll see many status lines on your screen concluding with something like:

    ``` bash
    [INFO] Confluence started successfully in 71s at http://localhost:1990/confluence
    [INFO] Type CTRL-D to shutdown gracefully
    [INFO] Type CTRL-C to exit
    ```

4.  Open your browser and navigate to the local Confluence instance started by `atlas-run`.  
    If you used the settings in the instructions, the default port is 1990. 
5.  Navigate to your local Confluence, enter http://localhost:1990/confluence/ in your browser.
6.  At the Confluence login, enter the username `admin` and password `admin`.
7.  Integrate Confluence with HipChat by navigating to http://localhost:1990/confluence/plugins/servlet/hipchat/configure and following the prompts. Create a test group in HipChat, if required.
8.  Configure space to room notifications and enable your new notification:  
      
     <img src="/server/confluence/images/image2015-5-26-20:5:23.png" width="851" height="368" />
9.  Comment on a page, and you should see your notification appear in HipChat:  
      
     <img src="/server/confluence/images/image2015-5-26-20:16:21.png" width="700" />

## Step 5. Expand the plugin by using a custom message format and icon

You can introduce custom messages by overriding `getMessage`in your implementation class. The message is expected to be in HTML format, so be sure to escape any parameterised content. The <a href="https://www.hipchat.com/docs/apiv2/method/send_room_notification" class="external-link">HipChat API documentation</a> describes the restrictions on the supported HTML element.

``` java
@Override
public String getMessage(CommentCreateEvent event) {
    return "A <b>custom</b> message";
}
```

Similarly you can provide custom icons for display in messages by implementing your own version of the `com.atlassian.hipchat.api.icons.Icon` interface, or creating an instance of `com.atlassian.hipchat.api.icons.DefaultIcon`.

The icon instance can be returned from the `getIcon `method, if you're using the default message rendering, or you can use it within your own implementation of `getMessage`.
