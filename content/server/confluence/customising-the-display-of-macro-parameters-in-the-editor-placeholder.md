---
aliases:
- /server/confluence/customising-the-display-of-macro-parameters-in-the-editor-placeholder-17563810.html
- /server/confluence/customising-the-display-of-macro-parameters-in-the-editor-placeholder-17563810.md
category: devguide
confluence_id: 17563810
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=17563810
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=17563810
date: '2017-12-08'
legacy_title: Customising the Display of Macro Parameters in the Editor Placeholder
platform: server
product: confluence
subcategory: learning
title: Customising the display of macro parameters in the editor placeholder
---
# Customising the display of macro parameters in the editor placeholder

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This document applies to <strong>Confluence 4.0 and later</strong><br />
</p></td>
</tr>
</tbody>
</table>

It is possible to customise the way the editor displays macro parameters in the macro placeholder.

By default as many parameters as can fit will be displayed in the placeholder, as shown here:

![](/server/confluence/images/placeholder-parameters.png)

You can control which parameters you want to display here, to ensure the most relevant information is visible to the author.

For example, the Confluence Warning macro has two parameters, *title* and *icon*. We consider *title* to be the most interesting parameter, so the macro is configured as follows:

``` xml
<xhtml-macro name='warning' class='com.atlassian.confluence.extra.information.xhtml.WarningMacro' key='xhtml-warning'>
    <description key="com.atlassian.confluence.extra.information.warning.desc"/>
    <device-type>mobile</device-type>
    <category name="formatting"/>
    <parameters>
        <parameter name="title" type="string">
            <option key="showNameInPlaceholder" value="false" />
            <option key="showValueInPlaceholder" value="true" />
        </parameter>
        <parameter name="icon" type="boolean" default="true"/>
    </parameters>
</xhtml-macro> 
```

The 'showNameInPlaceholder' option is set to false which means the name of the parameter (title) will not be displayed. In addition the presence of these options on any of the macro parameters means that any other parameters without explicit options set will not be displayed.

Let's assume an author adds the Warning macro to a page, and gives it a title of 'The title of the warning'. The above configuration leads to a placeholder as shown here:

![](/server/confluence/images/warning-placeholder.png)

## Parameter details

| Option                 | Value       | Meaning                                                                       |
|------------------------|-------------|-------------------------------------------------------------------------------|
| showNameInPlaceholder  | true\|false | If true then the parameter name will be displayed in the Editor placeholder.  |
| showValueInPlaceholder | true\|false | If then then the parameter value will be displayed in the Editor placeholder. |

If none of the parameters in a macro include any of the above options, then the default behaviour is to show all the parameters that fit in the placeholder: full title and value.

If one or more parameters has either option set, then all parameters that do not include the options will default to false (that is, they will not be shown).
