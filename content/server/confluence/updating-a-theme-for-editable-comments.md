---
aliases:
- /server/confluence/updating-a-theme-for-editable-comments-2031659.html
- /server/confluence/updating-a-theme-for-editable-comments-2031659.md
category: reference
confluence_id: 2031659
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031659
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031659
date: '2017-12-08'
legacy_title: Updating a theme for editable comments
platform: server
product: confluence
subcategory: modules
title: Updating a theme for editable comments
---
# Updating a theme for editable comments

This is a simple how-to that shows the steps to upgrade your plugin for editable comments.

#### Modify `sharedcomments.vmd`

Making your themes compatible with editable comment only requires modifying `sharedcomments.vmd`. There are 3 parts to update. A good example of this is the <a href="#clickr-theme" class="unresolved">Clickr Theme</a>.

##### Adding the edit link

First to enable editable comment you will need to give access to the edit function.  
Adding the link is as simple as adding the following piece of code near your existing 'Permalink' and 'Remove Comment' links:

``` java
#if ($permissionHelper.canEdit($remoteUser, $comment ))
  | <a id="edit-$comment.id" href="$req.contextPath$generalUtil.customGetPageUrl($page)showComments=true&amp;editComment=true&amp;focusedCommentId=$comment.id#comment-$comment.id">$action.getText('edit.name')</a>
#end
```

##### Enable inline editing

Editing a comment happens inline. Therefore the editor must be added when rendering the comment being edited as follow:

``` java
#if ($focusedCommentId == $comment.id && $action.editComment && $permissionHelper.canEdit($remoteUser, $comment))
  <form name="editcommentform" method="POST" action="$req.contextPath/pages/doeditcomment.action?pageId=$page.id&amp;commentId=$comment.id">
    #bodytag (Component "name='content'" "theme='notable'" "template='wiki-textarea.vm'")
      #param ("formname" "editcommentform")
      #param ("spaceKey" "$generalUtil.urlEncode($spaceKey)")
      #param ("rows" 15)
      #param ("cols" 70)
      #param ("width" "100%")
      #param ("tabindex" "4")
      #param ("tdcolor" "f0f0f0")
      #param ("toolbarExpanded" "false")
      #param ("initialFocus" "false")
      #param ("edit" "true")
      #param ("heartbeat" "false")
      #param ("wikiContent" "$comment.content")
      #param ("wysiwygContent" "$action.helper.wikiStyleRenderer.convertWikiToXHtml($comment.toPageContext(), $comment.content)")
    #end
    #commentSubmission()
  </form>
#else
  ## your current comment rendering...
#end
```

##### Add update information

This step is optional but it always nice for user to knwo when a comment has been updated and by who. The following piece of code gets the necessary information.

``` java
#if ( $action.helper.shouldRenderCommentAsUpdated($comment) )
  #if ( $comment.creatorName == $comment.lastModifierName )
    $action.getText("comment.updated.by.author", ["#usernameLink ($comment.lastModifierName)", $action.dateFormatter.formatDateTime( $comment.lastModificationDate )])
  #else
    $action.getText("comment.updated.by.non.author", ["#usernameLink ($comment.lastModifierName)", $action.dateFormatter.formatDateTime( $comment.lastModificationDate )])
  #end
#end
```

The `shouldRenderCommentAsUpdated` method is a convenience method that checks whether the comment has been updated by its creator more than 10 minutes after being created. It exists so that comments will not get cluttered with useless information because of a quick fix made shortly after the comment is posted. One can adjust the time frame by passing a number of seconds as the second argument to this method.

Finally, if the updater of the comment is different to the original author of the comment, their name is displayed.

