---
aliases:
- /server/confluence/plugin-development-deployment-paradigm-39368852.html
- /server/confluence/plugin-development-deployment-paradigm-39368852.md
category: devguide
confluence_id: 39368852
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368852
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368852
date: '2017-12-08'
legacy_title: Plugin Development Deployment Paradigm
platform: server
product: confluence
subcategory: other
title: Plugin Development Deployment Paradigm
---
# Plugin Development Deployment Paradigm

Confluence Plugins are deployed as OSGi packages. This is a Java structure that allows code to be deployed into a running application.  It also bundles the code so that each plugin to either share a common library or have it's own copy to avoid collisions.  The general process is to build the java code in the plugin, package everything as an OSGi bundle and install into Confluence.  For production, a user can manually upload a plugin or download it from the Atlassian Marketplace.  This process is cumbersome for development.  Over time, Confluence has adopted several more efficient patterns for stitching together the process from building to deploying the plugin.

### QuickReload

The general pattern for this process is to have Confluence watch directories for changes using QuickReload and use the shell command **atlas-package** to compile the java code and bundle everything together.  

{{% note %}}

Transformerless & Transformation Required Plugins

 The traditional design is the "Transformation Required" pattern.  A plugin that requires transformation is inspected when uploaded for dependencies and repackaged with an OSGi manifest when pushed to Confluence.

The term transformerless refers to the fact that plugins do not need to be "transformed" into OSGi bundles when uploaded into Confluence.  The transformerless plugin uses the SpringScanner to build the manifests needed to make the packaged plugin a viable OSGi bundle.

{{% /note %}}

### FastDev (deprecated by QuickReload)

The FastDev would also watch a directory for changes.  It had some implementation issues and has be deprecated by QuickReload.  It is unclear if the FastDev approach supports the transformerless design.

### atlas-cli + pi

{{% warning %}}

Transformerless plugin development not supported.

{{% /warning %}}

The shell command **atlas-cli** brings up a maven command line interface.  Within the atlas-cli shell, the command **pi** ( package & install ) will build the plugin and push it to the runnning SDK instance of Confluence.  This seems to be the tried and true approach; however, it does not support the transformerless plugin architecture as the **pi** command does not call the spring scanner that is necessary for making the dependency manifest need by the OSGi bundle.
